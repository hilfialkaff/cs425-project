#ifndef UTILS_H
#define UTILS_H

#include "network.h"
#include "constants.h"

#include <netinet/in.h>
#include <sys/stat.h>
#include <dirent.h>
#include <arpa/inet.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <math.h>

using namespace std;

inline bool fileExists(string filename)
{
    struct stat buf;
    if (stat(filename.c_str(), &buf) != -1) {
        return true;
    }
    return false;
}

/* Return the physical location of sdfs file */
inline string get_real_fname(string num, string fname)
{
    return string("machines/" + num + "/" + fname);
}

inline vector<string> get_outfile_with_prefix(string dir, string prefix)
{
    DIR *pdir = NULL;
    pdir = opendir(dir.c_str());
    struct dirent *pent = NULL;
    vector<string> ret;
 
    if (pdir == NULL) {
        cout << __func__ << " opendir(): " << errno << endl;
        return ret;
    }
 
    while (pent = readdir (pdir)) {
        if (pent == NULL) {
            cout << __func__ << " readdir(): " << errno << endl;
            return ret;
        }

        string fname = string(pent->d_name);
        if (fname.find(prefix) == 0) {
            ret.push_back(fname);
        }
    }
 
    closedir (pdir);
    return ret;
}

/* Convert sockaddr struct into string */
inline string tostring(struct sockaddr_in s)
{
    char id[100];

    sprintf(id, "%s:%d", inet_ntoa(s.sin_addr), ntohs(s.sin_port));
    return string(id);  
}

/* Print ip & port of sockaddr_in */
inline void print_ip_port(struct sockaddr_in s)
{
#ifdef DEBUG
    cout << "ip: " << inet_ntoa(s.sin_addr) << " port: " << ntohs(s.sin_port) << endl;
#endif /* DEBUG */
}

/* Return true if port and ip equal */
inline bool sockaddr_cmp(struct sockaddr_in* s1, struct sockaddr_in* s2)
{
    return (s1->sin_port == s2->sin_port) &&
        !strcmp(inet_ntoa(s1->sin_addr), inet_ntoa(s2->sin_addr));
}

/* Setup the sockaddr struct */
inline void setup_sockaddr(struct sockaddr_in* sockaddr, int port, string ip)
{
    memset(sockaddr, 0, sizeof(sockaddr));

    sockaddr->sin_family = AF_INET;
    sockaddr->sin_port = htons(port);
    inet_aton(ip.c_str(), &sockaddr->sin_addr);
}

/* Read the port/ip information of the contact point */
inline pair<int, string> read_contact_info()
{
    ifstream fp;
    string ip;
    int port;
    string str;
    int index;

    fp.open(CONF_NAME);
    if(!fp.is_open()) {
        cerr << "! Open: " << errno << endl;
        exit(1);
    }

    getline(fp, str);
    index = str.find(CONF_DELIMITER);
    port = atoi(str.substr(index + 1, str.length()).c_str());
    ip = str.substr(0, index);

    return pair<int, string>(port, ip);
}

inline unsigned int hashfile(map<unsigned int, Contact> contacts, const char* s, bool isnode)
{
	string input(s);
    long h = 0;
    while (*s)
        h += (unsigned char) *s++;
	unsigned int maxNodes = pow(2,NUM_BIT);
    unsigned int proposedid = h % maxNodes;
    return proposedid;
}

#endif /* UTILS_H */
