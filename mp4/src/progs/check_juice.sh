#!/bin/bash

OUT=`ls output/`

rm output/* -f
cp juice ..
cd ..
juice_files=`find machines/ -name local_result_*`
./juice $juice_files progs/output/final_result
rm juice
cd progs/

f=`find ../machines/ -name local_final_result`
# cat output/final_result | wc -l
# cat $f | wc -l

cat output/final_result | sort > manual_result
cat $f | sort > sdfs_result
colordiff manual_result sdfs_result
