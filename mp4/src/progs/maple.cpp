#include "maple.h"



void maple::read_phase(char* filename, char* intermediate_prefix)
{
	ifstream infile;
	fstream outfile;
	infile.open (filename);
	if (!infile.is_open()) {
	    perror("infile open()");
		return;
	}

//	string word = "";
    char c;
    int totalcount=0;
	string word="";
    while(infile.get(c))
    {
        if( (c > 64 && c < 91) ||(c>96&&c<123) )
        {
            if((isupper(c)))
            {
                c = (tolower(c));
            }
            word = word + c;
        }
        else
        {
        	if(word!="")
        	{
        		totalcount++;
        		if( hash_table.find(word)==hash_table.end())
				{
					hash_table[word]=1;
				}
				else
				{
					hash_table[word]++;
				}
        	}
        	word="";

        }
    }
	unordered_map<string, int>::iterator itr;
	for(itr=hash_table.begin(); itr!=hash_table.end(); itr++)
	{
		string tempname=string(intermediate_prefix) +"_"+itr->first;
		outfile.open (tempname.c_str(), fstream::out | fstream::app);
		if (!outfile) {
            perror("outfile()");
		}

		outfile<<filename <<" "<<itr->first<<" "<<itr->second<<" "<<totalcount<<endl;
		outfile.close();
	}

	hash_table.clear();
	infile.close();
	return;
}

//prefix, file1, file2...

int main (int argc, char* argv[] )
{
	maple read;
	for(int i=1; i< argc - 1; i++)
	{
		read.read_phase(argv[i],argv[argc - 1]);
	}

}
