#include "juice.h"

void juice::final_phase(char* filename, char* dst_filename)
{
	ifstream infile;
	fstream outfile;
	outfile.open (dst_filename, fstream::in | fstream::out | fstream::app);

	infile.open (filename);
	if(!infile)
		return;
	string doc, key;
	string word_count, total_count;
	int count_file=0;
	while (infile>>doc)
	{
		count_file++;
		infile>>key;
		infile>>word_count;
		infile>>total_count;
		Key element(doc, key);
		int temp_count1= atoi( word_count.c_str() );
		int temp_count2= atoi( total_count.c_str() );
		double freq= (temp_count1*1.0)/temp_count2;

		if( hash_table.find(element)==hash_table.end())
			hash_table[element]= freq;
		else
		{
			continue;
		}
	}
	map<Key, double>::iterator itr;
	for (itr= hash_table.begin(); itr!= hash_table.end(); itr++)
	{
		double final_value = -1* log(count_file)*itr->second; 
		outfile<<itr->first.first <<" " <<itr->first.second<<" "<<final_value<<endl;
	}

	infile.close();
	outfile.close();
	hash_table.clear();
}
 //output, file1, file2, ....
 int main(int argc, char* argv[] )
 {
 	juice outputfile;
	for(int i=1; i< argc - 1; i++)
	{
 		outputfile.final_phase(argv[i] , argv[argc - 1]);
	}

 }
