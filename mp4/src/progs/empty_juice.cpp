#include <fstream>
#include <iostream>
#include <sys/stat.h>

using namespace std;

int main(int argc, char* argv[])
{
    fstream out;
    ifstream in;

    in.open(argv[1]);
    out.open(argv[2], fstream::out | fstream::app);
    out << "Test Juice" << endl;

    in.close();
    out.close();
}
