#ifndef JUICE_H
#define JUICE_H 1
#endif

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <tr1/unordered_map>
#include <utility>
#include <map>
#include <stdio.h>
#include <math.h>

using namespace std;
using namespace std::tr1;
typedef pair<string, string> Key;

class juice{
public: 
	void final_phase(char* filename, char* dst_filename);
private:
	map<Key, double> hash_table;
};
