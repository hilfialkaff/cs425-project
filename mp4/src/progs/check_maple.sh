#!/bin/bash

OUT=`ls output/`

rm output/* -f
cp maple ..
cd ..
maple_files=`find machines/ -name local_* | grep -v local_result | grep -v local_final_result`
./maple $maple_files progs/output/result
rm maple
cd progs/

for file in $OUT
do
    f=`find ../machines/ -name local_$file | grep -v local_final_result`
    echo $f
    colordiff output/$file $f
done
