#include <fstream>
#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{
    fstream out;
    ifstream prefix;

    prefix.open(argv[1]);
    out.open(argv[2], fstream::out | fstream::app);
    out << "Test Maple" << endl;

    prefix.close();
    out.close();
}
