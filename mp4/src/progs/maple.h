#ifndef MAPLE_H
#define MAPLE_H 1
#endif

#include <iostream>
#include <fstream>
#include <string>
#include <tr1/unordered_map>
using namespace std;
using namespace std::tr1;
class maple
{
public:
	void read_phase(char* filename, char* intermediate_prefix);
private:
	unordered_map<string, int> hash_table;
};
