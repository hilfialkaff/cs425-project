#include "contact.h"
#include "utils.h"

/* Constructor */
Contact::Contact() :
    ctr(0), penalty(0), sockaddr(), timestamp()
{
    cfd = -1;
    return;
}

/* Constructor */
Contact::Contact(struct sockaddr_in addr, struct timespec time) :
    ctr(0), penalty(0), sockaddr(addr), timestamp(time)
{
    cfd = -1;
    return;
}

/* Destructor */
Contact::~Contact()
{
    return;
}

/* Compare two neighbor through its port/ip information */
bool Contact::operator==(Contact other)
{
    return sockaddr_cmp(&sockaddr, other.get_sockaddr());
}
