#include "node.h"

void Node::send_join(void* arg)
{
    struct timespec cur;
    char tmp[100];
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cur);
    
    send_udp(JOIN, udp_fd, &contact_sockaddr, &cur, sizeof(struct timespec));
    sprintf(tmp, "Sending JOIN msg at %ld", cur.tv_sec * 1000000 + cur.tv_nsec / 1000);
    logger.log(tmp, &contact_sockaddr);
}

void Node::send_leave(void* arg)
{
    logger.log("Sending LEAVE msg", clkwise_node.second.get_sockaddr());
    send_udp(LEAVE, udp_fd, clkwise_node.second.get_sockaddr(), NULL, 0);
}

void Node::send_heartbeat(void* arg)
{
    logger.log("Sending HEARTBEAT msg", aclkwise_node.second.get_sockaddr());

    uint64_t ctr;
    
    if (aclkwise_node.second == Contact()) {
        return;
    }

    ctr = aclkwise_node.second.get_ctr();
    send_udp(HEARTBEAT, udp_fd, aclkwise_node.second.get_sockaddr(), &ctr, sizeof(uint64_t));
    aclkwise_node.second.inc_ctr();
}

void Node::send_new_member(void* arg)
{
    map<unsigned int, Contact>::iterator it;

    for (it = contacts.begin(); it != contacts.end(); it++) {
		if (!sockaddr_cmp(it->second.get_sockaddr(), &contact_sockaddr)) {
		    send_tcp(NEW_MEMBER, it->second.get_cfd(), arg, sizeof(new_member_msg_t));
		
		    logger.log("Sending NEW MEMBER msg", it->second.get_sockaddr());
		}
    }
}

void Node::send_member_list(void* arg)
{
    map<unsigned int, Contact>::iterator it;
    new_member_msg_t members[contacts.size()];
    int i;
    struct sockaddr_in* s = (struct sockaddr_in*)arg;

    logger.log("Sending MEMBER LIST msg", s);

    for (i = 0, it = contacts.begin(); it != contacts.end(); i++, it++) {
        members[i].addr = *(it->second.get_sockaddr());
		members[i].hash = it->first;
    }

    send_udp(MEMBER_LIST, udp_fd, s, (void*)members, sizeof(members));
}

void Node::send_member_die(void* arg)
{
    map<unsigned int, Contact>::iterator it;
    struct sockaddr_in* s = (struct sockaddr_in*)arg;

    for (it = contacts.begin(); it != contacts.end(); it++) {
        logger.log("Sending MEMBER DIE msg", it->second.get_sockaddr());
        send_udp(MEMBER_DIE, udp_fd, it->second.get_sockaddr(), (void*)s, sizeof(struct sockaddr_in));
    }
}

void Node::send_election_req(void* arg)
{
    Contact c = clkwise_node.second;
    logger.log("Sending ELECTION REQ msg", c.get_sockaddr());

    election_msg_t* msg = (election_msg_t*)arg;
    send_udp(ELECTION_REQ, udp_fd, c.get_sockaddr(), msg, sizeof(election_msg_t));
}

void Node::send_new_leader(void* arg)
{
    Contact c = clkwise_node.second;
    logger.log("Sending NEW LEADER msg", c.get_sockaddr());

    new_leader_msg_t* msg = (new_leader_msg_t*)arg;
    send_udp(NEW_LEADER, udp_fd, c.get_sockaddr(), msg, sizeof(new_leader_msg_t));
}

void Node::send_juice_start(void* arg)
{
    cout << "fd: " << contact_fd << endl;
    logger.log("Sending JUICE START msg", &contact_sockaddr);

    send_tcp(JUICE_START, contact_fd, arg, sizeof(juice_msg_t));
}

void Node::send_maple_start(void* arg)
{
    logger.log("Sending MAPLE START msg", &contact_sockaddr);
    send_tcp(MAPLE_START, contact_fd, arg, sizeof(maple_msg_t));
}

void Node::send_maple_execute(int fd, void* arg)
{
    logger.log("Sending MAPLE EXECUTE msg", contacts.find(hashid)->second.get_sockaddr());
    send_tcp(MAPLE_EXECUTE, fd, arg, sizeof(maple_msg_t));
}

void Node::send_juice_execute(int fd, void* arg)
{
    cout << __func__ << " fd: " << fd << endl;
    logger.log("Sending JUICE EXECUTE msg", contacts.find(hashid)->second.get_sockaddr());

    send_tcp(JUICE_EXECUTE, fd, arg, sizeof(juice_msg_t));
}
