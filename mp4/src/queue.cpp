/*
 * Generic queue data type.
 */

#include <pthread.h>
#include <stdlib.h>
#include <assert.h>
#include "queue.h"

struct queue_node {
	void *item;
	struct queue_node *next;
};

void queue_init(queue_t *q)
{
	q->head = q->tail = NULL;
}

int queue_is_empty(queue_t *q)
{
	return q->head == NULL;
}

void queue_enqueue(queue_t *q, void *item)
{
	queue_node_t *node;

	node = (queue_node_t *)malloc(sizeof(queue_node_t));
	node->item = item;
	node->next = NULL;

	if (q->tail != NULL) {
		q->tail->next = node;
		q->tail = node;
	} else {
		q->head = q->tail = node;
	}
}

void *queue_dequeue(queue_t *q)
{
	void *item;
	queue_node_t *old_head;

	assert(!queue_is_empty(q));

	item = q->head->item;
	old_head = q->head;
	q->head = q->head->next;
	free(old_head);
	if (q->head == NULL) {
		q->tail = NULL;
	}

	return item;
}
