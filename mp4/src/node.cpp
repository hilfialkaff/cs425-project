#include <unistd.h>

#include "node.h"
#include "math.h"
#include "tester.h"

Node node;

/* Constructor */
Node::Node()
    : clkwise_node(), aclkwise_node(), is_candidate(false), job_exist(false)
{
    pthread_mutex_init(&mutex, NULL);
    pthread_mutex_init(&file_mutex, NULL);
    return;
}

/* Populate the node's information */
void Node::bootstrap(string ip, int port, string num)
{
    pair<int, string> port_ip = read_contact_info();

    if (port_ip.first == port && port_ip.second == ip) {
        is_contact = true;
    }

    setup_sockaddr(&sockaddr, port, ip);

    if (this->is_contact)
    {
        char id[100];
        struct timespec cur;
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cur);

        sprintf(id,"%s:%d", inet_ntoa(sockaddr.sin_addr), sockaddr.sin_port);
        Contact new_member = Contact(sockaddr, cur);
        hashid = hash(id, true);
		char status[100];
		sprintf(status, "Contact Server Hash: %d\n", hashid);
        logger.log(string(status), &sockaddr);

        contacts.insert(pair<unsigned int, Contact>(hashid, new_member));
    }

    this->udp_fd = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0);
    if (udp_fd < 0) {
        cerr << "! Socket: " << errno << endl;
        exit(1);
    }

    this->datafd = socket(AF_INET, SOCK_STREAM | O_NONBLOCK, 0);
    if(datafd < 0) {
        cerr << "! Socket: " << errno << endl;
        exit(1);
    }

    if (bind(udp_fd, (struct sockaddr*)(&(this->sockaddr)), sizeof(struct sockaddr))) {
        cerr << "! Bind: " << errno << "port: " << ntohs(this->sockaddr.sin_port) << endl;
        exit(1);
    }

    if (bind(datafd, (struct sockaddr*)(&(this->sockaddr)), sizeof(struct sockaddr))) {
        cerr << "! Bind: " << errno << "port: " << ntohs(this->sockaddr.sin_port) << endl;
        exit(1);
    }

    /* Start listening for clients */
    listen(datafd, 100);

    machine_num = num;
    logger.prep_file(num);
    this->setup();
}

unsigned int Node::hash(const char* s, bool isnode)
{
	string input(s);
    long h = 0;
    while (*s)
        h += (unsigned char) *s++;
	unsigned int maxNodes = pow(2,NUM_BIT);
    unsigned int proposedid = h % maxNodes;
	char status[100];
	sprintf(status, "Hash Output for %s is %d\n", input.c_str(), proposedid);
	logger.log(string(status), &sockaddr);
    if (isnode)
    {
        bool proposedidnotunique;
        unsigned int originalid = proposedid;
        do
        {
            proposedidnotunique = false;
            for (map<unsigned int, Contact>::iterator it = contacts.begin(); it != contacts.end(); it++)
            {
                if (proposedid == (*it).first)
                    proposedidnotunique = true;
            }

            if (proposedidnotunique)
                proposedid = (proposedid + 1) % maxNodes;
        } while (proposedidnotunique && proposedid != originalid);

        if (proposedidnotunique && proposedid == originalid)
            cerr << "We have hit an upper limit on the number of ids. Should increase NUM_BIT";
    }

    return proposedid;
}

/* Socket setup procedures and try to join the system (if it is not the contact point) */
void Node::setup()
{
    pair<int, string> port_ip = read_contact_info();
    setup_sockaddr(&contact_sockaddr, port_ip.first, port_ip.second);

    if (sockaddr_cmp(&contact_sockaddr, &sockaddr)) {
        is_contact = true;
    }

    if (!is_contact) {
        do {
            this->send(JOIN, NULL);
            //sleep(1);
        } while (this->handle_udp() <= 0);
    }

    this->is_connected = true;

    tester.execute();
}

/* Clean up the resources associated with the connections */
void Node::cleanup()
{
    // close(this->fd);
    // close(this->datafd);
    // vector<int>::iterator it;
    // for (it = cfds.begin(); it != cfds.end(); it++) {
    //     close(*it);
    // }

    // this->fd = -1;
    // this->datafd = -1;
    this->is_connected = false;
    //clkwise_node.second.~Contact();
    //aclkwise_node.second.~Contact();

    if (!this->is_contact) {
        contacts.clear();
    }
}

/* Send a msg corresponding to type */
void Node::send(int type, void* arg)
{
    switch(type) {
    case JOIN:
        send_join(arg);
        break;
    case LEAVE:
        send_leave(arg);
        break;
    case HEARTBEAT:
        send_heartbeat(arg);
        break;
    case NEW_MEMBER:
        send_new_member(arg);
        break;
    case MEMBER_LIST:
        send_member_list(arg);
        break;
    case MEMBER_DIE:
        send_member_die(arg);
        break;
    case ELECTION_REQ:
        send_election_req(arg);
        break;
    case NEW_LEADER:
        send_new_leader(arg);
        break;
    case MAPLE_START:
        send_maple_start(arg);
        break;
    case JUICE_START:
        send_juice_start(arg);
        break;
    default:
        cerr << __func__ << "Invalid msg type: " << type << endl;
    }
}

void Node::send_fileop(int type, string sdfs, string local)
{
    switch(type) {
    case GET:
        getfile(sdfs, local);
        break;
    case PUT:
        putfile(sdfs, local);
        break;
    case DEL:
        delfile(sdfs);
        break;
    }
}

//Checks whether file exists which is owned by the node
bool Node::fileexists(string sdfs, int* fd)
{
    unsigned int fileid = hash(sdfs.c_str(),false);

    if ((fileid >= hashid && fileid < clkwise_node.first) || (hashid > clkwise_node.first && (fileid >= hashid || fileid < clkwise_node.first)))
    {
        *fd = -1;
		// File may already be present locally
		for (vector<FileEntry>::iterator it = clockwisefilelist.begin(); it != clockwisefilelist.end(); it++)
		    if (sdfs == it->sdfsfilename)
				return true;

        return false;
    }
    else
    {
#ifdef DEBUG
        cout << __func__ << " File in different node " << endl;
#endif
        //If file is present then it needs to be fetched from the node whose hash id is <= fileid
        map<unsigned int, Contact>::iterator it = contacts.begin();
        if ((*it).first > fileid)
            *fd = contacts.rbegin()->second.get_cfd();
        else
        {
            for (; it != contacts.end(); it++)
            {
                if ((*it).first > fileid)
                {
                    it--;
                    *fd = it->second.get_cfd();
                    break;
                }
            }
            if (it == contacts.end())
                *fd = contacts.rbegin()->second.get_cfd();
        }
#ifdef DEBUG
        cout << __func__ << " Dest Node" << *fd << endl;
#endif
        send_tcp(FILE_EXISTS, *fd, const_cast<char *>(sdfs.c_str()), sdfs.size());

        uint8_t bytes[MAX_MSG];
        memset(bytes, '\0', MAX_MSG);
        msg_t* msg = (msg_t*)bytes;
        struct sockaddr_in src;

        bool ret = _recv_tcp(*fd, msg);
        if (!ret) {
            return false;
        }


		if (msg->type == FILE_MISSING)
		    return false;
		else
		    return true;
    }
}

string Node::getfile(string sdfs, string local)
{
    //Check if file exists
    int fd;
    bool exists = fileexists(sdfs, &fd);
    string kInvalidString = "";

    // File may be already cached. Check the cache list
    for (vector<FileEntry>::iterator it = cachedfilelist.begin(); it != cachedfilelist.end(); it++)
    {
        if (sdfs == it->sdfsfilename)
        {
            if (!exists)
            {
				if (fd != -1)
				{
				    string fname = get_real_fname(machine_num, it->localfilename);
		            remove(fname.c_str());
				}
				else
				{
					cerr << "File's hash should owned by node and should not be kept in cache" << endl;
				    /*FileEntry f;
					rename(it->localfilename, local);
				    f.sdfsfilename = sdfs;
				    f.localfilename = local;
				    clockwisefilelist.push_back(f);
			        cout << "File has been locally saved as" << local << endl;*/
				}
                cachedfilelist.erase(it);
                return kInvalidString;
            }
            else
                cout << "File is already present and is locally saved as " << it->localfilename << endl;
            return it->localfilename;
        }
    }

    if (!exists)
    {
        cout << "File does not exist" << endl;
        return kInvalidString;
    }

    if (fd == -1)
    {
        //File is present and resides locally.
        for (vector<FileEntry>::iterator it = clockwisefilelist.begin(); it != clockwisefilelist.end(); it++)
            if (sdfs == it->sdfsfilename)
            {
                local = it->localfilename;
                break;
            }

        cout << "File is already present and is locally saved as" << local << endl;
    }
    else
    {
        //File is present it needs to be fetched from the node whose hash id is <= fileid
        bool success = fetchfile(fd, sdfs, local);
        if (success) {
            cout << "File read successfully and is locally saved as " << local << endl;
            FileEntry f;
            f.sdfsfilename = sdfs;
            f.localfilename = local;
            cachedfilelist.push_back(f);
        }
        else
            cout << "File read failed for some unknown reason errno: " << errno << endl;
    }
    return local;
}

bool Node::fetchfile(int fd, string sdfs, string local)
{
    send_tcp(GET, fd, const_cast<char *>(sdfs.c_str()), sdfs.size());
    return receivefile(fd, local);
}

bool Node::receivefile(int fd, string local, bool append)
{
    uint8_t bytes[MAX_MSG];
    struct sockaddr_in src;
    string fname = get_real_fname(machine_num, local);

	char status[100];
	sprintf(status, "Function: receivefile: %s\n", fname.c_str());
	logger.log(string(status), &sockaddr);
	vector<string> s;
    ofstream out;

    int ret;
    msg_t* msg = (msg_t*)bytes;

    do
    {
        memset(bytes, '\0', MAX_MSG);
        bool ret= _recv_tcp(fd, msg);
        if (!ret) {
            return false;
        }

        if (msg->type != END_OF_FILE) {
            s.push_back(string((char*)msg->payload));
	        send_tcp(DATA_RECV, fd, NULL, 0);
		}
    } while (msg->type != END_OF_FILE);

    pthread_mutex_lock(&file_mutex);

    if (append) {
        out.open(fname.c_str(), fstream::out | fstream::app);
    } else {
        out.open(fname.c_str(), fstream::out | fstream::app);
    }

    if (!out) {
        cout << __func__ << " error: " << errno << " file: " << fname.c_str() << endl;
        return false;
    }

    vector<string>::iterator it;
    for (it = s.begin(); it != s.end(); it++) {
	    out << *it;
    }

    out.close();

    pthread_mutex_unlock(&file_mutex);

	sprintf(status, "Function: receivefile: Recv END_OF_FILE");
	logger.log(string(status), &sockaddr);

#ifdef DEBUG
    cout << __func__ << ": after end_of_file ret " << ret << " errno: " << errno << " msg: " << msg->payload << endl;
#endif

    return true;
}

bool Node::copylocally(string srcfile, string localfile)
{
    int filefd, ret;

    size_t len = 0;

    pthread_mutex_lock(&file_mutex);
    fstream out(localfile.c_str(), fstream::out | fstream::app);

    if (!out) {
        cout << __func__ << " error: " << errno << " file: " << localfile.c_str() << endl;
        return false;
    }

	char status[100];
	sprintf(status, "Function: copylocally: Copying file %s\n", localfile.c_str());
	logger.log(string(status), &sockaddr);

    fstream in(srcfile.c_str(), fstream::in);
    if (!in) {
        cout << __func__ << " error: " << errno << " file: " << srcfile.c_str() << endl;
        return false;
    }

    string line;
    while(in.good()) {
        getline(in, line);
        out << line << endl;
    }

	out.close();
	in.close();
    pthread_mutex_unlock(&file_mutex);

    return true;
}

bool Node::appendlocally(string srcfile, string localfile)
{
    int filefd, ret;

    size_t len = 0;

    pthread_mutex_lock(&file_mutex);

    fstream out(localfile.c_str(), fstream::out | fstream::app);

    if (!out) {
        cout << __func__ << " error: " << errno << " file: " << localfile.c_str() << endl;
        return false;
    }

	char status[100];
	sprintf(status, "Function: appendlocally: Appending file %s\n", localfile.c_str());
	logger.log(string(status), &sockaddr);

    fstream in(srcfile.c_str(), fstream::in);
    if (!in) {
        cout << __func__ << " error: " << errno << " file: " << srcfile.c_str() << endl;
        return false;
    }

    string line;
    while(in.good()) { 
        getline(in, line);
        out << line << endl;
    }

	out.close();
	in.close();

    pthread_mutex_unlock(&file_mutex);

    return true;
}

void Node::putfile(string sdfs, string local)
{
    int fd;
    cout << __func__ << " file: " << local << endl;
    bool exists = fileexists(sdfs, &fd);

    bool success = true;
    if (fd == -1)
    {
		string localfile = "local_" + sdfs;
	    string fname = get_real_fname(machine_num, localfile);

		char status[100];
		sprintf(status, "Function: putfile: Filename %s\n", fname.c_str());
		logger.log(string(status), &sockaddr);

        pthread_mutex_lock(&file_mutex);
        bool append;
        append = fileExists(fname) ? true : false;
        fstream out(fname.c_str(), fstream::out | fstream::app);
        out.close();
        cout << __func__ << " append: " << append << " fname: " << fname << endl;
        pthread_mutex_unlock(&file_mutex);

        if (append) {
        if (appendlocally(local, fname))
		{
		    cout << "If: appending locally" << endl;

#ifdef REPL
            pthread_mutex_lock(&mutex);
		    // Send the data to the clockwise neighbor as a replica
		    // TODO: Replica should automatically append
			send_tcp(REPL_DEL, clkwise_node.second.get_cfd(), const_cast<char *>(sdfs.c_str()), sdfs.size());
		    send_tcp(REPL_PUT, clkwise_node.second.get_cfd(), const_cast<char *>(sdfs.c_str()), sdfs.size());
		    sleep(1);
		    success = pushfile(clkwise_node.second.get_cfd(), fname);
            pthread_mutex_unlock(&mutex);
#endif /* REPL */
		}
        }
        else {
            if (copylocally(local, fname))
		    {
		    	FileEntry f;
		    	f.sdfsfilename = sdfs;
		    	f.localfilename = localfile;
		    	clockwisefilelist.push_back(f);

		    	cout << "File is locally saved as: " << localfile << endl;

                char* tmp = new char [sdfs.size() + 1];
                copy(sdfs.begin(), sdfs.end(), tmp);
                tmp[sdfs.size()] = '\0';

		        // Send the data to the clockwise neighbor as a replica
#ifdef REPL
                pthread_mutex_lock(&mutex);
		        send_tcp(REPL_PUT, clkwise_node.second.get_cfd(), (void*)tmp, sdfs.size() + 1);
		        sleep(1);
		        success = pushfile(clkwise_node.second.get_cfd(), fname);
                pthread_mutex_unlock(&mutex);
#endif /* REPL */
		    }
		}
    }
    else
    {
		char status[100];
		sprintf(status, "Function: putfile: Sending Data %s ", sdfs.c_str());
		logger.log(string(status), &sockaddr);

        char* tmp = new char [sdfs.size() + 1];
        copy(sdfs.begin(), sdfs.end(), tmp);
        tmp[sdfs.size()] = '\0';
        pthread_mutex_lock(&mutex);
        send_tcp(PUT, fd, (void*)tmp, sdfs.size() + 1);
        success = pushfile(fd, local);
        pthread_mutex_unlock(&mutex);
    }

    if (success) {
        // cout << "File inserted successfully" << endl;
    }

    // cout << __func__ << " finish: " << local << " src: self" << endl; 
}

void Node::putintermediatefile(string sdfs, string local)
{
    int fd;
    // cout << __func__ << " file: " << local << endl;
    bool exists = fileexists(sdfs, &fd);

    bool success = true;
    if (fd == -1)
    {
		string localfile = "local_" + sdfs;
	    string fname = get_real_fname(machine_num, localfile);

		char status[100];
		sprintf(status, "Function: putfile: Filename %s\n", fname.c_str());
		logger.log(string(status), &sockaddr);

        pthread_mutex_lock(&file_mutex);
        bool append;
        append = fileExists(fname) ? true : false;
        fstream out(fname.c_str(), fstream::out | fstream::app);
        out.close();
        cout << __func__ << " append: " << append << " fname: " << fname << endl;
        pthread_mutex_unlock(&file_mutex);

        if (append) {
        if (appendlocally(local, fname))
		{
		    cout << "If: appending locally" << endl;
		}
        }
        else {
            if (copylocally(local, fname))
		    {
		    	FileEntry f;
		    	f.sdfsfilename = sdfs;
		    	f.localfilename = localfile;
		    	clockwisefilelist.push_back(f);

		    	cout << "File is locally saved as: " << localfile << endl;
		    }
		}
    }
    else
    {
		char status[100];
		sprintf(status, "Function: putfile: Sending Data %s ", sdfs.c_str());
		logger.log(string(status), &sockaddr);

        char* tmp = new char [sdfs.size() + 1];
        copy(sdfs.begin(), sdfs.end(), tmp);
        tmp[sdfs.size()] = '\0';
        pthread_mutex_lock(&mutex);
        send_tcp(INT_PUT, fd, (void*)tmp, sdfs.size() + 1);
        success = pushfile(fd, local);
        pthread_mutex_unlock(&mutex);
    }

    if (success) {
        // cout << "File inserted successfully" << endl;
    }
}

bool Node::pushfile(int fd, string local)
{
    int filefd, ret;

    char line[MAX_MSG];
    size_t len = 0;
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*)bytes;

    ifstream in;
    in.open(local.c_str(), fstream::in);

    memset(line, '\0', MAX_MSG);
    while(in.good()) {
        in.read(line, MAX_MSG - sizeof(msg_t) - 2);
        send_tcp(PUT, fd, line, strlen(line));
        memset(bytes, '\0', MAX_MSG);
        memset(line, '\0', MAX_MSG);
        bool ret = _recv_tcp(fd, msg);
        if (!ret) {
            return false;
        }

        if (msg->type != DATA_RECV)
			cerr << __func__ << ": invalid msg type: " << msg->type << endl;
    }

	char status[100];
	sprintf(status, "Send Finished");
	logger.log(string(status), &sockaddr);

    send_tcp(END_OF_FILE, fd, NULL, 0);

    in.close();
    return true;
}

void Node::delfile(string sdfs)
{
    //Check if file exists
    int fd;
    bool exists = fileexists(sdfs, &fd);

    // File may be already cached. Check the cache list
    for (vector<FileEntry>::iterator it = cachedfilelist.begin(); it != cachedfilelist.end(); it++)
    {
        if (sdfs == it->sdfsfilename)
        {
            if (!exists)
            {
				if (fd != -1)
				{
				    string fname = get_real_fname(machine_num, it->localfilename);
		            remove(fname.c_str());
		            cachedfilelist.erase(it);
	                cout << "File does not exist" << endl;
				}
				else
					cerr << "File's hash should owned by node and should not be kept in cache" << endl;
                return;
            }
        }
    }

    if (!exists)
    {
        cout << "File does not exist" << endl;
        return;
    }

    if (fd == -1)
    {
        //File is present and resides locally.
        for (vector<FileEntry>::iterator it = clockwisefilelist.begin(); it != clockwisefilelist.end(); it++)
            if (sdfs == it->sdfsfilename)
            {
#ifdef REPL
				send_tcp(REPL_DEL, clkwise_node.second.get_cfd(), const_cast<char *>(sdfs.c_str()), sdfs.size());
#endif /* REPL */
			    string fname = get_real_fname(machine_num, it->localfilename);
                if (!verify_delete(clkwise_node.second.get_cfd()) ||  remove(fname.c_str()) != 0) {
                    cout << "Delete Failed for some unknown reason error: " << errno << endl;
                } else {
                    cout << "Successfully deleted file" << endl;
                }
                clockwisefilelist.erase(it);

                break;
            }
    }
    else
    {
        //File is present it needs to be fetched from the node whose hash id is <= fileid
        bool success = removefile(fd, sdfs);
        if (success)
            cout << "Successfully deleted file" << endl;
        else
            cout << "Delete Failed for some unknown reason error: " << errno << endl;
    }
}

bool Node::removefile(int fd, string sdfs)
{
    send_tcp(DEL, fd, const_cast<char *>(sdfs.c_str()), sdfs.size());

    return verify_delete(fd);
}

bool Node::verify_delete(int fd)
{
    uint8_t bytes[MAX_MSG];
    memset(bytes, '\0', MAX_MSG);
    msg_t* msg = (msg_t*)bytes;
    bool ret;

    ret = _recv_tcp(fd, msg);
    if (!ret) {
        return false;
    }

	if (msg->type != DELETE_FAILED) {
	    return true;
	}
	else {
	    return false;
	}
}

/* Receive and handle the msg */
bool Node::handle_udp()
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*)bytes;
    int ret;
    struct sockaddr_in src;

    ret = recv_udp(this->udp_fd, msg, &src);

    if (ret <= 0) {
        return false;
    }

    switch(msg->type) {
    case JOIN:
        ret = handle_join(src, msg);
        break;
    case LEAVE:
        ret = handle_leave(src, msg);
        break;
    case HEARTBEAT:
        ret = handle_heartbeat(src, msg);
        break;
    case MEMBER_LIST:
        ret = handle_member_list(src, msg);
        break;
    case MEMBER_DIE:
        ret = handle_member_die(src, msg);
        break;
    case ELECTION_REQ:
        ret = handle_election_req(src, msg);
        break;
    case NEW_LEADER:
        ret = handle_new_leader(src, msg);
        break;
    default:
        cerr << __func__ << ": Invalid type " << msg->type << endl;
    }

    return ret;
}

bool Node::handle_tcp(int fd)
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*)bytes;
    bool ret;

    memset(msg, '\0', sizeof(char) * MAX_MSG);

    ret = _recv_tcp(fd, msg);
    if (!ret) {
        return ret;
    }

    switch(msg->type) {
    case NEW_MEMBER:
        ret = handle_new_member(fd, msg);
        break;
    case GET:
        ret = handle_get(fd, msg);
        break;
    case PUT:
        ret = handle_put(fd, msg);
        break;
    case INT_PUT:
        ret = handle_intermediate_put(fd, msg);
        break;
    case SPL_PUT:
        ret = handle_spl_put(fd, msg);
        break;
#ifdef REPL
    case REPL_PUT:
        ret = handle_repl_put(fd, msg);
        break;
    case REPL_DEL:
        ret = handle_repl_del(fd, msg);
        break;
    case NEW_REPL_GET:
        ret = handle_new_repl_get(fd, msg);
        break;
#endif /* REPL */
    case DEL:
        ret = handle_del(fd, msg);
        break;
    case FILE_EXISTS:
        ret = handle_file_exists(fd, msg);
        break;
    case NEW_FILE_GET:
        ret = handle_new_file_get(fd, msg);
        break;
    case MJ_REJECT:
        ret = handle_mj_reject(fd, msg);
        break;
    case JUICE_START:
        ret = handle_juice_start(fd, msg);
        break;
    case JUICE_EXECUTE:
        ret = handle_juice_execute(fd, msg);
        break;
    case JUICE_FINISH:
        ret = handle_juice_finish(fd, msg);
        break;
    case JUICE_AVAILABLE:
        ret = handle_juice_available(fd, msg);
        break;
    case JUICE_STARTED:
        ret = handle_juice_started(fd, msg);
        break;
    case MAPLE_START:
        ret = handle_maple_start(fd, msg);
        break;
    case MAPLE_EXECUTE:
        ret = handle_maple_execute(fd, msg);
        break;
    case MAPLE_AVAILABLE:
        ret = handle_maple_available(fd, msg);
        break;
    case MAPLE_STARTED:
        ret = handle_maple_started(fd, msg);
        break;
    case MAPLE_FINISH:
        ret = handle_maple_finish(fd, msg);
        break;
    case TASK_FINISHED:
        ret = handle_task_finished(fd, msg);
        break;
    case LIST_FILE:
        ret = handle_list_files(fd, msg);
        break;
    case FILENAME_LIST:
        ret = handle_filename_list(fd, msg);
        break;
    default:
        cerr << __func__ << ": Invalid type " << msg->type << endl;
    }

    return ret;
}

/* Check the status of all the contacts */
void Node::check_heartbeat()
{
    map<unsigned int, Contact>::iterator it;

    if (clkwise_node.second == Contact()) {
        return;
    }

    clkwise_node.second.inc_penalty();
    if (clkwise_node.second.get_penalty() == MAX_PENALTY) {
        logger.log("No heartbeat. Removing.", clkwise_node.second.get_sockaddr());

        for (it = contacts.begin(); it != contacts.end(); it++) {
            if (clkwise_node.second == (*it).second) {
                contacts.erase(it++);
                if (!contacts.size()) {
                    clkwise_node.second = Contact();
                    aclkwise_node.second = Contact();
                    break;
                }

                struct sockaddr_in member_s = *(clkwise_node.second.get_sockaddr());
                update_contacts_onrem();

                send_member_die(&member_s);

                if (sockaddr_cmp(&member_s, &contact_sockaddr)) {
                    election_msg_t msg;
                    msg.best_attr = clockwisefilelist.size();
                    msg.node_id = hashid;
                    is_candidate = true;

                    send(ELECTION_REQ, &msg);
                }

                break;
            }
        }
    }
}

bool Node::copyfile(int dst, string sdfs, string local)
{
    pthread_mutex_lock(&mutex);
    send_tcp(SPL_PUT, dst, const_cast<char *>(sdfs.c_str()), sdfs.size());
    string fname = get_real_fname(machine_num, local);
    pushfile(dst, fname);
    pthread_mutex_unlock(&mutex);
}

#ifdef REPL
bool Node::copyreplica(int dst, string sdfs, string local)
{
    pthread_mutex_lock(&mutex);
    send_tcp(REPL_PUT, dst, const_cast<char *>(sdfs.c_str()), sdfs.size());
    string fname = get_real_fname(machine_num, local);
    usleep(100);
    pushfile(dst, fname);
    usleep(100);
    pthread_mutex_unlock(&mutex);
}

void Node::recv_replicas(int fd)
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*)bytes;

    do {
        _recv_tcp(fd, msg);
        if (msg->type != TRANS_FINISH) {
            string sdfs((char*)msg->payload);

            string local = "replica_" + sdfs;
#ifdef DEBUG
            cout << __func__ << ": Replica name:" << local << endl;
#endif
            bool success = receivefile(fd, local);
            if (success) {
                FileEntry f;
                f.sdfsfilename = sdfs;
                f.localfilename = local;
                anticlockwisereplicafilelist.push_back(f);
            }
        }
    } while (msg->type != TRANS_FINISH);
}
#endif /* REPL */

void Node::recv_files(int fd)
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*)bytes;

    do {
        _recv_tcp(fd, msg);
        if (msg->type != TRANS_FINISH) {
            string sdfs((char*)msg->payload);

            string local = "local_" + sdfs;
#ifdef DEBUG
            cout << __func__ << " File name:" << local << endl;
#endif

            bool success = receivefile(fd, local);
            if (success) {
                FileEntry f;
                f.sdfsfilename = sdfs;
                f.localfilename = local;
                clockwisefilelist.push_back(f);
            }
        }
    } while (msg->type != TRANS_FINISH);
}

bool Node::update_contacts_onadd()
{
    update_contacts();

    //logger.begin_benchmark();

#ifdef REPL
    send_tcp(NEW_REPL_GET, clkwise_node.second.get_cfd(), NULL, 0);
    recv_replicas(clkwise_node.second.get_cfd());
#endif /* REPL */
    usleep(500);

    send_tcp(NEW_FILE_GET, aclkwise_node.second.get_cfd(), NULL, 0);
    recv_files(aclkwise_node.second.get_cfd());
    // logger.end_benchmark("Node rejoining");
}

bool Node::update_contacts_onrem()
{
    // logger.begin_benchmark();

    unsigned int oldclkwisehash = clkwise_node.first;
    unsigned int oldaclkwisehash = aclkwise_node.first;

    update_contacts();

    //Old clockwise neighbor removed: Need to send some files from my original set to new clockwise node as replicas
    if (clkwise_node.first != oldclkwisehash)
    {
        unsigned int fileid;
        vector<FileEntry>::iterator it = clockwisefilelist.begin();
        while (it != clockwisefilelist.end())
        {
            fileid = hash(it->sdfsfilename.c_str(), false);
#ifdef DEBUG
            cout << "replica if sdfds: " << it->sdfsfilename << endl;
#endif
            if ((fileid >= hashid && fileid < oldclkwisehash) // Takes care of normal case and wrap around case when NACH > OACH
                || (hashid > oldclkwisehash && (fileid >= hashid || fileid < oldclkwisehash))) { // NACH > OACH
		        logger.log("Moving File " + it->sdfsfilename , &sockaddr);
#ifdef REPL
                copyreplica(clkwise_node.second.get_cfd(), it->sdfsfilename, it->localfilename);
#endif /* REPL */
            }
            it++;
        }
    }

    //Old anticlockwise neighbor removed: Need to send some files from my replica set to new anticlockwise node
    if (aclkwise_node.first != oldaclkwisehash)
    {
        unsigned int fileid;
        vector<FileEntry>::iterator it = anticlockwisereplicafilelist.begin();
        while (it != anticlockwisereplicafilelist.end())
        {
#ifdef DEBUG
            cout << "file if sdfds: " << it->sdfsfilename << endl;
#endif
            fileid = hash(it->sdfsfilename.c_str(), false);
            if ((fileid < hashid && fileid >= oldaclkwisehash) // Takes care of normal case and wrap around case when NACH > OACH
                || (hashid < oldaclkwisehash && (fileid < hashid || fileid >= oldaclkwisehash))) { // NACH > OACH
		        logger.log("Moving File " + it->sdfsfilename, &sockaddr);
                copyfile(aclkwise_node.second.get_cfd(), it->sdfsfilename, it->localfilename);
            }
            it++;
        }
    }

    // logger.end_benchmark("Replication upon member failure");
}

void Node::enqueue_job(job_type type, void* msg, int len)
{
    wanted_job.type = type;
    wanted_job.is_executed = false;
    wanted_job.payload = new char[len]; // TODO: Clean mem

    memcpy(wanted_job.payload, msg, len);

    job_exist = true;

    issue_job();
}

void Node::issue_job()
{
    int job_type;

    if (wanted_job.type == JUICE) {
        job_type = JUICE_START;
    } else {
        job_type = MAPLE_START;
    }

    cout << __func__ << ": " << sizeof(wanted_job.payload) << endl;
    send(job_type, wanted_job.payload);
}

void Node::execute_task()
{
    int fd;
    vector<string> files = neighbor_task.get_src_files();
    vector<string>::iterator it;
    string local;

    cur_task.execute();
    // cur_task.set_is_done(true);

    // XXX: Very hardcoded. Should be a vector
    if (neighbor_task.get_exe().empty() && !neighbor_task.get_is_done()) {
        cout << __func__ << ": executing neighbor task\n" << endl;
        cur_task = neighbor_task;
        cur_task.reset_src_files();
        for (it = files.begin(); it != files.end(); it++) {
            cur_task.set_src_file(getfile(*it,local));
        }
        neighbor_task = Task();
        node.execute_task();
        // cur_task.set_is_done(true);
    }

    // sleep(1);

    string dir = get_real_fname(machine_num, "");
    vector<string> outfiles = get_outfile_with_prefix(dir, cur_task.get_outfile());

    cout << __func__ << " outfile size: " << outfiles.size() << endl;

    for (vector<string>::iterator it = outfiles.begin();it != outfiles.end(); it++) {
        // cout << __func__ << " file:" << *it << endl;

        // putfile(*it, dir + *it);
        putintermediatefile(*it, dir + *it);
        // sleep(1);
    }
    cout << __func__ << ": finish executing job" << endl;

    Contact c = find_aclkwise_contact().second;
    send_tcp(TASK_FINISHED, c.get_cfd(), NULL, 0);
    if (!is_contact)
    {
        if (cur_task.get_job_type() == MAPLE) {
            send_tcp(MAPLE_FINISH, contact_fd, NULL, 0);
        } else {
            send_tcp(JUICE_FINISH, contact_fd, NULL, 0);
        }
    }
}

/* Destructor */
Node::~Node()
{
    close(udp_fd);
    contacts.clear();
}

/*********************************************/
/* Utility functions */
/*********************************************/

int Node::find_contact_fd(struct sockaddr_in addr)
{
    map<unsigned int, Contact>::iterator it;

    for (it = contacts.begin(); it != contacts.end(); it++) {
        Contact c = (*it).second;
        if (sockaddr_cmp(c.get_sockaddr(), &addr)) {
            return c.get_cfd();
        }
    }
}

void Node::set_cfds(fd_set* fds)
{
    vector<int>::iterator it;
    for (it = cfds.begin(); it!= cfds.end(); it++) {
        FD_SET((*it), fds);
    }
}

void Node::check_cfds(fd_set* fds)
{
    vector<int>::iterator it;
    int fd;
    int ret;
    pthread_t th[cfds.size()];
    int i = 0;
    int j;

    for (it = cfds.begin(); it!= cfds.end(); it++) {
        fd = (*it);
        if (FD_ISSET(fd, fds)) {
            if (!handle_tcp(fd) && errno != EAGAIN) {
                cout << __func__ << " handle_tcp error: " << errno << endl;
                cfds.erase(it);
                break;
            }
        }
    }
}

void Node::remove_cfd(int cfd)
{
    vector<int>::iterator it;
    for (it = cfds.begin(); it != cfds.end(); it++) {
        if (cfd == (*it)) {
            cfds.erase(it);
            break;
        }
    }
}

bool Node::_recv_tcp(int fd, msg_t* msg)
{
    bool ret = recv_tcp(fd, msg);
    if (!ret) {
        remove_cfd(fd);
    }
    return ret;
}

void Node::setup_contact()
{
    is_contact = true;
    contact_sockaddr = sockaddr;

    fstream fp;
    fp.open(CONF_NAME, fstream::out);
    fp << tostring(sockaddr);
    fp.close();
}

/* Print current contact list */
void Node::print_contacts_list()
{
    map<unsigned int, Contact>::iterator it;

    cout << "PRINT CONTACTS" << endl;
    for (it = contacts.begin(); it != contacts.end(); it++) {
        Contact c = (*it).second;
        cout << "sockaddr: " << tostring(*(c.get_sockaddr())) << " fd: " << c.get_cfd() << endl;
    }
}

/* Find logical clockwise contact */
pair<unsigned int, Contact> Node::find_clkwise_contact()
{
    map<unsigned int, Contact>::iterator it = contacts.find(hashid);

    if (it == contacts.end())
        cerr << __func__ << "Hash of the node does not exist" << endl;
    else
        it++;

    if (it == contacts.end())
        it = contacts.begin();

#ifdef DEBUG
    // cout << " hashid: " << hashid << " clkwise: " << ntohs((*it).second.get_sockaddr()->sin_port) << endl;
#endif

    return *it;
}

/* Find logical anticlockwise contact */
pair<unsigned int, Contact> Node::find_aclkwise_contact()
{
    map<unsigned int, Contact>::iterator it = contacts.find(hashid);
    pair<unsigned int, Contact> anti = *it;
    if (it == contacts.end())
        cerr << __func__ << "Hash of the node does not exist" << endl;
    else if (it == contacts.begin())
        anti = *(contacts.rbegin());
    else
    {
        it--;
        anti = *it;
    }

#ifdef DEBUG
    // cout << " hashid: " << hashid << " aclkwise: " << ntohs((anti).second.get_sockaddr()->sin_port) << endl;
#endif

    return anti;
}

vector<string> Node::get_file_with_prefix(string prefix)
{
    vector<FileEntry>::iterator it;
    vector<string> ret;

    for (it = clockwisefilelist.begin(); it != clockwisefilelist.end(); it++) {
        if (it->sdfsfilename.find(prefix) == 0) {
            ret.push_back(it->sdfsfilename);
        }
    }

    return ret;
}
