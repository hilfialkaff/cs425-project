#include <utility>
#include <iostream>
#include <fstream>
#include <string>
#include <assert.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>

#include "constants.h"
#include "network.h"
#include "node.h"
#include "parser.h"
#include "prodcons.h"

using namespace std;

string ip;
int port;
string num;

/*void signal_cb_handler(int signum)
{
    cout << __func__ << ": HERE" << endl;
    close(node.get_datafd());
    close(node.get_udp_fd());
}
void signal_cb_handler2(int signum)
{
    cout << __func__ << ": HERE" << endl;
    close(node.get_datafd());
    close(node.get_udp_fd());
}
void signal_cb_handler3(int signum)
{
    cout << __func__ << ": HERE" << endl;
    close(node.get_datafd());
    close(node.get_udp_fd());
}
void setup_signal_handler()
{
    signal(SIGINT, signal_cb_handler);
    signal(SIGSEGV, signal_cb_handler2);
    signal(SIGABRT, signal_cb_handler3);
}*/

// consumer is slow
void *consumer(void *arg)
{
	while (1) {
	    thread_msg_t *item = (thread_msg_t *)boundedqueue_dequeue(bq);
	    if (!item) {
	        cout << __func__ << ": stuck" << endl;
	        continue;
	    }
        send_tcp(item->type, item->fd, item->payload, item->size);
	}
	return NULL;

}

/* Parse command line arguments */
void parse_args(int argc, char** argv)
{
    int opt;
    pair<string, int> ret;

    while ((opt = getopt(argc, argv, "h:p:n:")) != -1) {
        switch (opt) {
        case 'h':
            ip = optarg;
            break;
        case 'p':
            port = atoi(optarg);
            break;
        case 'n':
            num = optarg;
            break;
        default:
            cout << "Usage: " << argv[0] << " [-h server address] [-p port] [-n number]." << endl;
        }
    }
}

/* Setting up the initial connection */
void setup()
{
    bq = (BoundedQueue *)malloc(sizeof(BoundedQueue));
	boundedqueue_init(bq, 1000);

    node.bootstrap(ip, port, num);

    cout << "Done setting up" << endl;
}

/* Send heartbeat message to all of its contacts */
void* send_heartbeat(void* ptr)
{
    while(1) {
        sleep(HB_INTERVAL);

        if (node.get_is_connected()) {
            node.send(HEARTBEAT, NULL);
        }
    }

    return NULL;
}

/* Detect failed nodes from the absence of heartbeat msg */
void* check_heartbeat(void* ptr)
{
    while(1) {
        sleep(HB_INTERVAL);

        if (node.get_is_connected()) {
            node.check_heartbeat();
        }
    }

    return NULL;
}

/* Check if there is new connection. If there is, handle it */
void check_new_conn(int data_fd)
{
    struct sockaddr_in s;
    size_t size = sizeof(struct sockaddr_in);
    int new_fd;

    new_fd = accept(data_fd, (struct sockaddr*)&s, (socklen_t*)&size);
    if (new_fd > 0) {
        node.insert_cfd(new_fd);
    }
}

/* Actual main function */
void _main()
{
    pthread_t th1, th2, thread1, thread2;
    fd_set fds;
    string cmd;
    int cfd;
    int data_fd;
    int i;
	
	//pthread_create(&thread1, NULL, &producer, bq);
	// pthread_create(&thread2, NULL, &consumer, NULL);
    pthread_create(&th1, NULL, send_heartbeat, NULL);
    pthread_create(&th2, NULL, check_heartbeat, NULL);

    cout << "cmd> ";
    fflush(stdout);

    /* Keep an eye on the stdin (in case we receive user input)
     * the socket descriptor (in case we receive any data). */
    while(1) {
        data_fd = node.get_datafd();
        cfd = node.get_udp_fd();

        check_new_conn(data_fd);

        FD_ZERO(&fds);
        FD_SET(0, &fds);

        if (node.get_is_connected()) {
            FD_SET(cfd, &fds);
            node.set_cfds(&fds);
        }

        if(select(FD_SETSIZE, &fds, NULL, NULL, NULL) < 0) {
            continue;
        }

        if(FD_ISSET(0, &fds)) {
            getline(cin,cmd);		
            Parser::parse_cmd(node, cmd.c_str());

            cout << "cmd> ";
            fflush(stdout);
        }

        if (FD_ISSET(cfd, &fds)) {
            node.handle_udp();
        }

        node.check_cfds(&fds);
    }	
}

int main(int argc, char** argv)
{
    //setup_signal_handler();
    parse_args(argc, argv);
    setup();
    _main();
}
