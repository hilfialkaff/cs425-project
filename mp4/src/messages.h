#ifndef MESSAGES_H
#define MESSAGES_H

/* Generic message header */
typedef struct msg {
    uint32_t type;
    uint32_t len;
    struct sockaddr_in src; /* For TCP msg */
    char payload[0];
} __attribute__((packed)) msg_t;

/* New member message payload */
typedef struct new_member_msg {
    struct sockaddr_in addr;
    unsigned int hash;
} __attribute__((packed)) new_member_msg_t;

/* Election message payload */
typedef struct election_msg {
    int best_attr;
    unsigned int node_id;
} __attribute__((packed)) election_msg_t;

/* New leader message payload */
typedef struct new_leader_msg {
    struct sockaddr_in s;
} __attribute__((packed)) new_leader_msg_t;

/* Map start message payload */
typedef struct maple_msg {
    char m_exe[MAX_NAME];
    int num_m;
    int num_src_files;
    char src_filename[100][MAX_NAME];
    char out_file_prefix[MAX_NAME];
} __attribute__((packed)) maple_msg_t;

/* Juice message payload */
typedef struct juice_msg {
    char j_exe[MAX_NAME];
    int num_j;
    int num_src_files;
    char src_filename[100][MAX_NAME];
    char outfile[MAX_NAME];
} __attribute__((packed)) juice_msg_t;

/* File List Name Message */
typedef struct file_list {
    char filelist[100][MAX_NAME];
    int count;
} __attribute__((packed)) file_list_msg_t;

/* Different type of messages in our system */
enum {
/********************************************************/
/* Ring management messages */
/********************************************************/
    JOIN = 0x1, /* Sent when a new node wants join */
    LEAVE, /* Sent when a node tries to leave */
    HEARTBEAT, /* Heartbeat to neighbor */
    NEW_MEMBER, /* Inform current nodes that a new node is joining */
    MEMBER_LIST, /* Inform new joining node the members in the system */
    MEMBER_DIE, /* Sent when a member just died */
    ELECTION_REQ,
    NEW_LEADER,
/********************************************************/
/* File management messages */
/********************************************************/
    GET,
    PUT, /* 10 */
    APPEND,
    SPL_PUT,
    DEL,
    FILE_MISSING,
    FILE_DELETED, /* 15 */
    DELETE_FAILED,
    FILE_EXISTS,
    FILE_FOUND,
    END_OF_FILE,
    TRANS_FINISH, /* 20 */
    NEW_FILE_GET,
    DATA_RECV,
#ifdef REPL
    REPL_PUT,
    REPL_DEL,
    NEW_REPL_GET, /* 25 */
#endif /* REPL */
    LIST_FILE,
    FILENAME_LIST,
    TASK_FINISHED,
    INT_PUT,
/********************************************************/
/* MapleJuice messages */
/********************************************************/
    MJ_REJECT,  /* 30 */
    MAPLE_START,
    JUICE_START,
    MAPLE_AVAILABLE,
    JUICE_AVAILABLE,
    MAPLE_EXECUTE, /* 35 */
    JUICE_EXECUTE,
    MAPLE_FINISH,
    JUICE_FINISH,
    MAPLE_STARTED,
    JUICE_STARTED,
    FILENAME_FINISH,
};

/* Marshall a msg by converting it into network byte order */
inline void marshall(msg_t* msg)
{
    uint32_t i;
    uint16_t tmp;

    /* Fancy pointer trick since htons takes in 2 bytes
     * while our payload is a char array (1 byte each) */
    for (i = 0;i < msg->len;i += 2) {
        tmp = htons(*(uint16_t*)(msg->payload + i));
        memcpy(msg->payload + i, (void*)&tmp, sizeof(uint16_t));
    }

    msg->type = htonl(msg->type);
    msg->len = htonl(msg->len);
}

/* Unmarshall a msg by converting it into host byte order */
inline void unmarshall(msg_t* msg)
{
    uint32_t i;
    uint16_t tmp;

    msg->type = ntohl(msg->type);
    msg->len = ntohl(msg->len);

    /* Fancy pointer trick since ntohs takes in 2 bytes
     * while our payload is a char array (1 byte each) */
    for (i = 0;i < msg->len; i+=2) {
        tmp = ntohs(*(uint16_t*)(msg->payload + i));
        memcpy(msg->payload + i, (void*)&tmp, sizeof(uint16_t));
    }
}

/* Find the total length of a message */
inline int msg_size(msg_t* msg)
{
    return sizeof(msg_t) + msg->len;
}

#endif /* MESSAGES_H */
