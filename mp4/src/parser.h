#ifndef PARSE_CMD_H
#define PARSE_CMD_H

#include <cstring>
#include <string>
#include <vector>

#include "node.h"

using namespace std;
 
/* The different available commands */
#define JOIN_CMD "join"
#define LEAVE_CMD "leave"
#define CRASH_CMD "crash"
#define GET_CMD "get"
#define PUT_CMD "put"
#define DEL_CMD "delete"
#define MAPLE_CMD "maple"
#define JUICE_CMD "juice"

class Parser {

public:
    /* Wrapper function to parse command from user input */
    static void parse_cmd(Node& node, const char* cmd);

private:
    /* Parse the different user commands */
    static void parse_join_cmd(Node& node);
    static void parse_leave_cmd(Node& node);
    static void parse_crash_cmd(Node& node);
    static void parse_get_cmd(Node& node, const char *cmd);
    static void parse_put_cmd(Node& node, const char* cmd);
    static void parse_del_cmd(Node& node, const char* cmd);
    static void parse_maple_cmd(Node& node, const char* cmd);
    static void parse_juice_cmd(Node& node, const char* cmd);

    /* Parse string into tokens */
    static vector<string> tokenize(string str);
};

#endif /* PARSE_H */
