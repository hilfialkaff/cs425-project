#include "node.h"

bool Node::handle_join(struct sockaddr_in src, msg_t* msg)
{
    char id[100];
    map<unsigned int, Contact>::iterator it;
    struct timespec* join_time;
    char tmp[100];

    /* Contact point shouldn't receive join */
    if (!is_contact) {
        cerr << "Shouldn't receive JOIN" << endl;
        return false;
    }

    /* Return when src already exist in neighbor list */
    for (it = contacts.begin(); it != contacts.end(); it++) {
        if (sockaddr_cmp(&src, it->second.get_sockaddr())) {
            return false;
        }
    }

    join_time = (struct timespec*)msg->payload;
    sprintf(tmp, "Recv JOIN msg at %ld", join_time->tv_sec * 1000000 + join_time->tv_nsec / 1000);
    logger.log(tmp, &src);

    sprintf(id,"%s:%d", inet_ntoa(src.sin_addr), src.sin_port);
    Contact new_member = Contact(src, *join_time);
    unsigned int new_member_hash = hash(id, true);

    int fd = socket(AF_INET, SOCK_STREAM | O_NONBLOCK, 0);
    int ret;

    do {
        ret = connect(fd, (struct sockaddr*)&src, sizeof(struct sockaddr));

        if (!ret) {
    		char status[100];
    		sprintf(status, "Msg: JOIN: Connected to: %d\n", ntohs(src.sin_port));
    		logger.log(string(status), &sockaddr);

            new_member.set_cfd(fd);
            break;
        }
    } while (ret != 0);

    new_member_msg_t arg;
    arg.addr = src;
    arg.hash = new_member_hash;

    send(NEW_MEMBER, (void*)&arg);

    sleep(1);

    contacts.insert(pair<unsigned int, Contact>(new_member_hash, new_member));
    update_contacts();
    send(MEMBER_LIST, (void*)&src);

    return true;
}

bool Node::handle_leave(struct sockaddr_in src, msg_t* msg)
{
    map<unsigned int, Contact>::iterator it;
    logger.log("Recv LEAVE msg", &src);
    bool ret = false;

    for (it = contacts.begin(); it != contacts.end(); it++) {
        if (aclkwise_node.second == (*it).second) {
            contacts.erase(it++);

            if (!contacts.size()) {
                clkwise_node.second = Contact();
                aclkwise_node.second = Contact();
                break;
            }

            struct sockaddr_in member_s = *(aclkwise_node.second.get_sockaddr());
            update_contacts_onrem();

            send(MEMBER_DIE, (void*)&member_s);

            if (sockaddr_cmp(&member_s, &contact_sockaddr)) {
                election_msg_t msg;
                msg.best_attr = clockwisefilelist.size();
                msg.node_id = hashid;
                is_candidate = true;

                send(ELECTION_REQ, (void*)&msg);
            }
            ret = true;
        }
    }

    return ret;
}

bool Node::handle_heartbeat(struct sockaddr_in src, msg_t* msg)
{
    clkwise_node.second.reset_penalty();
    logger.log("Recv HEARTBEAT msg", &src);
    return true;
}

bool Node::handle_member_list(struct sockaddr_in src, msg_t* msg)
{
    if (this->is_contact) {
        cerr << "Contact shouldn't recv MEMBER_LIST" << endl;
        return false;
    }

    new_member_msg_t s;
    int ret;
    int fd;
    for(uint32_t i = 0; i < (msg->len)/sizeof(new_member_msg_t); i++) {
        s = ((new_member_msg_t*)msg->payload)[i];

        pair<unsigned int, Contact> c (s.hash, Contact(s.addr, zero));
        if (sockaddr_cmp(&sockaddr, &s.addr)) {
            hashid = s.hash;
        }
        else {
            fd = socket(AF_INET, SOCK_STREAM | O_NONBLOCK, 0);
            do {
                ret = connect(fd, (struct sockaddr*)&s.addr, sizeof(struct sockaddr));

                if (!ret) {
    				char status[100];
    				sprintf(status, "Msg: MEMBER_LIST: Connected to: %d\n", ntohs(src.sin_port));
    				logger.log(string(status), &sockaddr);
                    c.second.set_cfd(fd);
                    
                    if (sockaddr_cmp(&s.addr, &contact_sockaddr)) {
                        contact_fd = fd;
                    }
                }
            } while (ret != 0);
        }
        contacts.insert(c);
    }

    update_contacts_onadd();

    logger.log("Recv MEMBER LIST msg", &src);
    return true;
}

void* _execute_task(void* arg)
{
    node.execute_task();

    return NULL;
}

bool Node::handle_member_die(struct sockaddr_in src, msg_t* msg)
{
    map<unsigned int, Contact>::iterator it;
    struct sockaddr_in* s = (struct sockaddr_in*)msg->payload;
    int fd;
    bool exec = false;

    cout << __func__ << "E:" << endl;
    print_ip_port(*s);

    /* There is a task to be executed in behalf of the dead neighbor */
    Contact c = find_clkwise_contact().second;
    if (sockaddr_cmp(s, c.get_sockaddr()) && neighbor_task.get_exe().empty()) {
        int cfd;
        vector<string> files = neighbor_task.get_src_files();
        vector<string>::iterator it;
        string local;

#if 0
        if (cur_task.get_is_done()) {
            cout << __func__ << ": neighbor fail\n" << endl;
            cur_task = neighbor_task;
            cur_task.reset_src_files();
            for (it = files.begin(); it != files.end(); it++) {
                cur_task.set_src_file(getfile(*it,local));
            }

            neighbor_task = Task();

            pthread_t th;
            pthread_create(&th, NULL, _execute_task, NULL);

            exec = true;
        }
#endif
    }

    bool has_pending_task = false;
    map<unsigned int, Contact>::iterator previousit;
    for (it = contacts.begin(); it != contacts.end(); it++) {
        if (sockaddr_cmp(s, it->second.get_sockaddr())) {
#if 0
            if (is_contact) {
                if (!cur_job.is_task_done(it->second.get_cfd())) {
                    cout << __func__ << ": setting job done" << endl;
                    print_ip_port(*s);
                    cur_job.set_done(it->second.get_cfd());
                    has_pending_task = true;
                }
            }
#endif
            contacts.erase(it);
            break;
        }
        previousit = it;
    }

#if 0
    if (it != contacts.end() && has_pending_task) {
        if (it == contacts.begin()) {
            cout << __func__ << ": If allocating job to: " << endl;
            print_ip_port(*(contacts.rbegin()->second.get_sockaddr()));
            cur_job.addcfdcount(contacts.rbegin()->second.get_cfd());
        } else {
            cout << __func__ << ": Else allocating job to: " << endl;
            print_ip_port(*(previousit->second.get_sockaddr()));
            cur_job.addcfdcount(previousit->second.get_cfd());
        }
    }
#endif

    update_contacts_onrem();

    // XXX: Need to update the handle_maple_available to record the new job
    if (exec) {
        int i;
        vector<string> files = cur_task.get_src_files();
        vector<string>::iterator it;
        c = find_aclkwise_contact().second;

        if (cur_task.get_job_type() == MAPLE) {
            maple_msg_t m;

            strcpy(m.m_exe, cur_task.get_exe().c_str());
            m.num_m = cur_task.get_num();
            m.num_src_files = files.size();
            for (i = 0; i < m.num_src_files; i++) {
                strcpy(m.src_filename[i], files.at(i).c_str());
            }
            strcpy(m.out_file_prefix, cur_task.get_outfile().c_str());

            send_tcp(MAPLE_AVAILABLE, c.get_cfd(), (void*)&m, sizeof(maple_msg_t));
        } else {
            juice_msg_t m;

            strcpy(m.j_exe, cur_task.get_exe().c_str());
            m.num_j = cur_task.get_num();
            for (i = 0; i < m.num_src_files; i++) {
                strcpy(m.src_filename[i], files.at(i).c_str());
            }
            strcpy(m.outfile, cur_task.get_outfile().c_str());

            send_tcp(JUICE_AVAILABLE, c.get_cfd(), (void*)&m, sizeof(maple_msg_t));
        }
    }

    logger.log("Recv MEMBER DIE msg", &src);

    return true;
}

bool Node::handle_election_req(struct sockaddr_in src, msg_t* msg)
{
    election_msg_t* m = (election_msg_t*)msg->payload;

    /* This node has the best attribute */
    if (is_candidate && m->node_id == hashid) {
    	char status[100];
    	sprintf(status, "Msg: ELECTION_REQ: This node will be leader");
    	logger.log(string(status), &sockaddr);
        new_leader_msg_t ldr_msg;
        ldr_msg.s = sockaddr;
        send(NEW_LEADER, (void*)&ldr_msg);

        is_candidate = false;
        is_leader = true;
        setup_contact();
    } else {
        if (clockwisefilelist.size() < m->best_attr) {
            m->best_attr = clockwisefilelist.size();
            m->node_id = hashid;
            is_candidate = true;
        }

    	char status[100];
    	sprintf(status, "best_attr: %d node: %d\n", m->best_attr, m->node_id);
    	logger.log(string(status), &sockaddr);

        send(ELECTION_REQ, (void*)m);
    }

    logger.log("Recv ELECTION_REQ msg", &src);
    return true;
}

bool Node::handle_new_leader(struct sockaddr_in src, msg_t* msg)
{
    new_leader_msg_t* m = (new_leader_msg_t*)msg->payload;

    if (is_leader) {
        is_leader = false;
    } else {
        is_candidate = false;
        contact_sockaddr = m->s;
        contact_fd = find_contact_fd(contact_sockaddr);

        issue_job();
    }

    logger.log("Recv NEW_LEADER msg", &src);
    return true;
}

bool Node::handle_new_member(int src_fd, msg_t* msg)
{
    if (this->is_contact) {
        cerr << "Contact shouldn't recv NEW_MEMBER" << endl;
        return false;
    }

    new_member_msg_t* m = (new_member_msg_t*)msg->payload;
    Contact new_member = Contact(m->addr, zero);

    int fd = socket(AF_INET, SOCK_STREAM | O_NONBLOCK, 0);
    int ret;

    do {
        ret = connect(fd, (struct sockaddr*)&m->addr, sizeof(struct sockaddr));

        if (!ret) {
    		char status[100];
    		sprintf(status, "Msg: NEW_MEMBER: Connected\n");
            new_member.set_cfd(fd);
            break;
        }
    } while (ret != 0);

    contacts.insert(pair<unsigned int, Contact>(m->hash, new_member));

    update_contacts();

    logger.log("Recv NEW MEMBER msg for: ", &sockaddr);
    return true;
}

bool Node::handle_get(int src_fd, msg_t* msg)
{
    char *sdfsfile;

    sdfsfile = (char *)msg->payload;
    string sdfs(sdfsfile), local;

    for (vector<FileEntry>::iterator it = clockwisefilelist.begin(); it != clockwisefilelist.end(); it++) {
        if (sdfs == it->sdfsfilename) {
            local = it->localfilename;
            break;
        }
    }

    string fname = get_real_fname(machine_num, local);

    pushfile(src_fd, fname);
    logger.log("Recv GET msg for" + sdfs, &sockaddr);
    return true;
}

bool Node::handle_put(int src_fd, msg_t* msg)
{
    char* sdfsfile;

    sdfsfile = (char *)msg->payload;
    string sdfs(sdfsfile);
    string local = "local_" + sdfs;

#ifdef DEBUG
    cout << __func__ << " File name:" << local << endl;
#endif

    pthread_mutex_lock(&file_mutex);
    bool append;
    string fname = get_real_fname(machine_num, local);
    append = fileExists(fname) ? true : false;
    fstream out(fname.c_str(), fstream::out | fstream::app);
    out.close();
    cout << __func__ << ": append:" << append << " file: " << fname << endl;
    pthread_mutex_unlock(&file_mutex);

    bool success = receivefile(src_fd, local, append);

    if (success) {
        if (!append) {
            FileEntry f;
            f.sdfsfilename = sdfs;
            f.localfilename = local;
            clockwisefilelist.push_back(f);
            string fname = get_real_fname(machine_num, local);

#ifdef REPL
            cout << "Replicating to fd: " << clkwise_node.second.get_cfd();
            pthread_mutex_lock(&mutex);
            send_tcp(REPL_PUT, clkwise_node.second.get_cfd(), const_cast<char *>(sdfs.c_str()), sdfs.size());
    	    sleep(1);
            success = pushfile(clkwise_node.second.get_cfd(), fname);
            pthread_mutex_unlock(&mutex);
#endif /* REPL */
        } else {
            string fname = get_real_fname(machine_num, local);

#ifdef REPL
            cout << "Replicating to fd: " << clkwise_node.second.get_cfd();
            pthread_mutex_lock(&mutex);
		    send_tcp(REPL_DEL, clkwise_node.second.get_cfd(), const_cast<char *>(sdfs.c_str()), sdfs.size());
            send_tcp(REPL_PUT, clkwise_node.second.get_cfd(), const_cast<char *>(sdfs.c_str()), sdfs.size());
    	    sleep(1);
            success = pushfile(clkwise_node.second.get_cfd(), fname);
            pthread_mutex_unlock(&mutex);
#endif /* REPL */
        }

    }
    cout << __func__ << " finish: " << local << " src: " << endl;
    print_ip_port (msg->src);

    logger.log("Recv PUT msg for" + sdfs, &sockaddr);
    return true;
}

bool Node::handle_intermediate_put(int src_fd, msg_t* msg)
{
    char* sdfsfile;

    sdfsfile = (char *)msg->payload;
    string sdfs(sdfsfile);
    string local = "local_" + sdfs;

#ifdef DEBUG
    cout << __func__ << " File name:" << local << endl;
#endif

    pthread_mutex_lock(&file_mutex);
    bool append;
    string fname = get_real_fname(machine_num, local);
    append = fileExists(fname) ? true : false;
    fstream out(fname.c_str(), fstream::out | fstream::app);
    out.close();
#ifdef DEBUG
    cout << __func__ << ": append:" << append << " file: " << fname << endl;
#endif
    pthread_mutex_unlock(&file_mutex);

    bool success = receivefile(src_fd, local, append);

    if (success) {
        if (!append) {
            FileEntry f;
            f.sdfsfilename = sdfs;
            f.localfilename = local;
            clockwisefilelist.push_back(f);
            string fname = get_real_fname(machine_num, local);
        }
    }

    print_ip_port (msg->src);

    logger.log("Recv PUT msg for" + sdfs, &sockaddr);
    return true;
}

bool Node::handle_spl_put(int src_fd, msg_t* msg)
{
    char* sdfsfile;

    // PUT without replication
    sdfsfile = (char *)msg->payload;
    string sdfs(sdfsfile);
    string local = "local_" + sdfs;

#ifdef DEBUG
    cout << __func__ << " File name:" << local << endl;
#endif

    bool success = receivefile(src_fd, local);
    if (success) {
        FileEntry f;
        f.sdfsfilename = sdfs;
        f.localfilename = local;
        clockwisefilelist.push_back(f);
        cout << __func__ << ": SPL_PUT size " << clockwisefilelist.size() << endl;
    }

    logger.log("Recv SPL_PUT msg for" + sdfs, &sockaddr);
    return true;
}

#ifdef REPL
bool Node::handle_repl_put(int src_fd, msg_t* msg)
{
    char* sdfsfile;

    sdfsfile = (char *)msg->payload;
    string sdfs(sdfsfile);

    string local = "replica_" + sdfs;

#ifdef DEBUG
    cout << __func__ << " File name:" << local << endl;
#endif

    bool success = receivefile(src_fd, local);
    if (success)
    {
        FileEntry f;
        f.sdfsfilename = sdfs;
        f.localfilename = local;
        anticlockwisereplicafilelist.push_back(f);
        cout << __func__ << ": REPL_PUT size " << anticlockwisereplicafilelist.size() << endl;
    }

    logger.log("Recv REPL_PUT msg for" + sdfs, &sockaddr);
    return true;
}
#endif /* REPL */

bool Node::handle_del(int src_fd, msg_t* msg)
{
    char* sdfsfile;
    bool success = false;
    sdfsfile = (char *)msg->payload;
    string sdfs(sdfsfile);

    vector<FileEntry>::iterator it = clockwisefilelist.begin();
    for (; it != clockwisefilelist.end(); it++) {
        if (sdfs == it->sdfsfilename) {
    	    string fname = get_real_fname(machine_num, it->localfilename);
#ifdef REPL
    		send_tcp(REPL_DEL, clkwise_node.second.get_cfd(), const_cast<char *>(sdfs.c_str()), sdfs.size());
#endif /* REPL */
    		if (!verify_delete(clkwise_node.second.get_cfd()) || (remove(fname.c_str()) == -1)) {
    			cout << "File Delete Failed for some unknown reason errno: " << errno << endl;
            } else {
    		    cout << "File Deleted successfully" << endl;
            }
    		success = true;
            clockwisefilelist.erase(it);
            send_tcp(FILE_DELETED, src_fd, NULL, 0);

            break;
        }
    }

    if (!success && it == clockwisefilelist.end()) {
    	cout << "File is not present" << endl;
        send_tcp(DELETE_FAILED, src_fd, NULL, 0);
    }

    logger.log("Recv DEL msg for" + sdfs, &sockaddr);
    return true;
}

#ifdef REPL
bool Node::handle_repl_del(int src_fd, msg_t* msg)
{
    char* sdfsfile;
    bool success = false;
    sdfsfile = (char *)msg->payload;
    string sdfs(sdfsfile);

    vector<FileEntry>::iterator it = anticlockwisereplicafilelist.begin();
    for (; it != anticlockwisereplicafilelist.end(); it++) {
        if (sdfs == it->sdfsfilename) {
    	    string fname = get_real_fname(machine_num, it->localfilename);
    		if (remove(fname.c_str()) != 0) {
    			cout << "Replica Delete Failed for some unknown reason error: " << errno << endl;
                send_tcp(DELETE_FAILED, src_fd, NULL, 0);
                break;
            }
    		success = true;
            anticlockwisereplicafilelist.erase(it);
    		cout << "Replica Deleted successfully" << endl;
            send_tcp(FILE_DELETED, src_fd, NULL, 0);

            break;
        }
    }

    if (!success && it == anticlockwisereplicafilelist.end()) {
    	cout << "Replica is not present" << endl;
        send_tcp(DELETE_FAILED, src_fd, NULL, 0);
    }
    logger.log("Recv REPL_DEL msg for" + sdfs, &sockaddr);
}
#endif /* REPL */

bool Node::handle_file_exists(int src_fd, msg_t* msg)
{
    cout << __func__ << " file: " << (char*)msg->payload << endl;
    char* sdfsfile;

    sdfsfile = (char *)msg->payload;
    string sdfs(sdfsfile);
    bool success = false;

    struct sockaddr_in dst;
    for (vector<FileEntry>::iterator it = clockwisefilelist.begin(); it != clockwisefilelist.end(); it++) {
        if (sdfs == it->sdfsfilename) {
            logger.log("Sending File Found\n", &sockaddr);
            send_tcp(FILE_FOUND, src_fd, NULL, 0);
    		success = true;
    		break;
        }
    }

    if (!success) {
        logger.log("Sending File Missing\n", &sockaddr);
        send_tcp(FILE_MISSING, src_fd, NULL, 0);
    }

    logger.log("Recv FILE_EXISTS msg for" + sdfs, &sockaddr);
    return true;
}

#ifdef REPL
bool Node::handle_new_repl_get(int src_fd, msg_t* msg)
{
    // New anticlockwise neighbor added: Need to move some replicas from my list to its list.
    unsigned int fileid;
    vector<FileEntry>::iterator it = anticlockwisereplicafilelist.begin();
    int i = 0;
    while ((i++) < anticlockwisereplicafilelist.size()) {
#ifdef DEBUG
        cout << "replica if sfds: " << it->sdfsfilename << " size: " << anticlockwisereplicafilelist.size() << " aclkwise id: " << aclkwise_node.first << endl; 
#endif
        fileid = hash(it->sdfsfilename.c_str(), false);
        if ((aclkwise_node.first < hashid && (fileid < aclkwise_node.first || fileid >= hashid)) || (aclkwise_node.first > hashid && (fileid < aclkwise_node.first && fileid >= hashid))) {
            logger.log("Moving Replica" + it->sdfsfilename , &sockaddr);
            // Test for crash
#ifdef REPL
            copyreplica(src_fd, it->sdfsfilename, it->localfilename);
#endif /* REPL */
    	    string fname = get_real_fname(machine_num, it->localfilename);
            if (remove(fname.c_str()) != 0)
                cout << "Delete Failed for some unknown reason errno" << errno << endl;
            anticlockwisereplicafilelist.erase(it);
            it = anticlockwisereplicafilelist.begin();
            i = 0;
        }
        else {
            it++;
        }
        usleep(100);
    }
    send_tcp(TRANS_FINISH, src_fd, NULL, 0);
    logger.log("Recv NEW_REPL_GET msg", &sockaddr);
    return true;
}
#endif /* REPL */

bool Node::handle_new_file_get(int src_fd, msg_t* msg)
{
    // New Clockwise Neighbor added: Need to move some file from my list to its list. Replica will still stay with the original node
    unsigned int fileid;
    vector<FileEntry>::iterator it = clockwisefilelist.begin();
    int i = 0;
    while ((i++) < clockwisefilelist.size()) {
        fileid = hash(it->sdfsfilename.c_str(), false);

#ifdef DEBUG
        cout << "file if sfds: " << it->sdfsfilename << endl;
#endif

        if ((clkwise_node.first > hashid && (fileid >= clkwise_node.first || fileid < hashid)) || (clkwise_node.first < hashid && fileid >= clkwise_node.first && fileid < hashid)) {
            logger.log("Moving File" + it->sdfsfilename , &sockaddr);
            // Test for crash
            copyfile(src_fd, it->sdfsfilename, it->localfilename);
    	    string fname = get_real_fname(machine_num, it->localfilename);
            if (remove(fname.c_str()) == -1) {
                cout << "Delete Failed for some unknown reason errno: " << errno << endl;
            }
            clockwisefilelist.erase(it);
            it = clockwisefilelist.begin();
            i = 0;
        }
        else {
            it++;
        }
        usleep(100);
    }

    send_tcp(TRANS_FINISH, src_fd, NULL, 0);
    logger.log("Recv NEW_FILE_GET msg", &sockaddr);
    return true;
}

bool Node::handle_mj_reject(int src_fd, msg_t* msg)
{
    cout << "Another job is currently running." << endl;
    logger.log("Recv MJ_REJECT msg", &sockaddr);
    return true;
}

bool Node::handle_juice_start(int src_fd, msg_t* msg)
{
    /* Only contact point receive JUICE_START */
    if (!is_contact) {
        cerr << "Shouldn't receive JUICE_START" << endl;
        return false;
    }

    int cfd = find_contact_fd(msg->src);
    juice_msg* m = (juice_msg*)msg->payload;

    if (!cur_job.is_done()) {
        send_tcp(MJ_REJECT, cfd, NULL, 0);
        return true;
    } else {
        logger.begin_benchmark();
        cout << "Previous job is done." << endl;
        cur_job = Job(m->j_exe, m->num_j, JUICE, m->outfile);

        // Get all input src files
        vector<string> filenames;
        char bytes[MAX_MSG];
        msg_t *ack = (msg_t*)bytes;
        
        int fd;
        vector<string> outfiles = get_file_with_prefix(m->src_filename[0]);
        cout << __func__ << ": size: " << outfiles.size() << endl;
        for (int i = 0; i < outfiles.size(); i++) {
            fileexists(outfiles[i], &fd);
            cur_job.set_src_file(fd, outfiles[i]);
        }
        
        for (map<unsigned int, Contact>::iterator it = contacts.begin(); it != contacts.end(); it++)
        {
            if (it->first == hashid) {
                continue;
            }

            send_tcp(LIST_FILE, it->second.get_cfd(), m->src_filename[0], strlen(m->src_filename[0]));

            while(1) {
                memset(bytes, '\0', MAX_MSG);
                bool ret = _recv_tcp(it->second.get_cfd(), ack);
                if (!ret) {
                    cout << __func__ << "Shouldn't go here" << endl;
                    return false;
                }
                cout << __func__ << " type: " << ack->type << endl;
                
                msg_t* m = (msg_t*)ack->payload;
                if (ack->type == FILENAME_FINISH) {
                    break;
                }
                if (ack->type == FILENAME_LIST) {
                    handle_filename_list(it->second.get_cfd(), m);
                }
            }
        }

        cur_job.allocate_jobs(contacts);
        map<int, void *> messagelist = cur_job.getSendMessageList();
        map<int, void *>::iterator it = messagelist.begin();
        bool needToProcessLocally = false;
        juice_msg_t* sameNodeMsg = (juice_msg_t *)it->second;
        if (it->first == -1)
        {
            needToProcessLocally = true;
            it++;
        }

        for (; it != messagelist.end(); it++) {
            send_juice_execute(it->first, it->second);
        }
        
        if (needToProcessLocally) {
            juice_msg_t* m = sameNodeMsg;

            /* Notifying the neighbor */
            Contact c = find_aclkwise_contact().second;
            cout << __func__ << ": outfile: " << m->outfile << endl;
            send_tcp(JUICE_AVAILABLE, c.get_cfd(), m, sizeof(juice_msg_t));

            string exe = m->j_exe;
            cout << __func__ << ": exe: " << exe << endl;

            cur_task = Task(exe, m->num_j, JUICE, m->outfile, get_real_fname(machine_num, ""));
            string local;
            for (int i = 0; i < m->num_src_files; i++)
                cur_task.set_src_file(getfile(m->src_filename[i],local));

            pthread_t th;
            pthread_create(&th, NULL, _execute_task, NULL);
            cur_job.set_done(-1);
        }
    }

    send_tcp(JUICE_STARTED, cfd, NULL, 0);

    logger.log("Recv JUICE_START msg", &sockaddr);
    return true;
}

bool Node::handle_list_files(int src_fd, msg_t *msg)
{
    int cfd = find_contact_fd(msg->src);
    string prefix = string((char*)msg->payload);
    vector<string> outfiles = get_file_with_prefix(prefix);

    cout << __func__ << ": size: " << outfiles.size() << endl;

    char bytes[MAX_MSG];
    file_list_msg_t *list = (file_list_msg_t*)bytes;
    int count = 0;
    for (vector<string>::iterator it = outfiles.begin(); it != outfiles.end(); it++) {
        // cout << __func__ << " file: " << it->c_str() << endl;
        
        strcpy(list->filelist[count++],it->c_str()); 

        // if (!(count % 100)) {
        //     list->count = count;
        //     send_tcp(FILENAME_LIST, src_fd, list, sizeof(file_list_msg_t));
        //     count = 0;
        //     memset(bytes, '\0', MAX_MSG);
        // }
    }

    if (count) {
        list->count = count;
        send_tcp(FILENAME_LIST, src_fd, list, sizeof(file_list_msg_t));
    }

    send_tcp(FILENAME_FINISH, src_fd, NULL, 0);
    logger.log("Recv LIST_FILES msg", &sockaddr);
    return true;
}

bool Node::handle_filename_list(int src_fd, msg_t *msg)
{
    file_list_msg_t *list = (file_list_msg_t *)msg;
    // int fd;
    cout << __func__ << " size: " << list->count << endl;
    for (int i = 0; i < list->count; i++) {
        // fileexists(list->filelist[i], &fd);
        // cout << __func__ << " file: " << list->filelist[i] << endl;
        cur_job.set_src_file(src_fd, list->filelist[i]);
    }

    logger.log("Recv FILENAME_LIST msg", &sockaddr);
    return true;
}

bool Node::handle_juice_execute(int src_fd, msg_t* msg)
{
    int fd = find_contact_fd(msg->src);
    int cfd;
    juice_msg_t* m = (juice_msg_t*)msg->payload;

    /* Notifying the neighbor */
    Contact c = find_aclkwise_contact().second;
    send_tcp(JUICE_AVAILABLE, c.get_cfd(), m, sizeof(juice_msg_t));

    string exe = m->j_exe;

    cur_task = Task(exe, m->num_j, JUICE, m->outfile, get_real_fname(machine_num, ""));
    string local;
    for (int i = 0; i < m->num_src_files; i++) {
        cur_task.set_src_file(getfile(m->src_filename[i],local));
    }

    pthread_t th;
    pthread_create(&th, NULL, _execute_task, NULL);

    // send_tcp(JUICE_FINISH, fd, NULL, 0);
    logger.log("Recv JUICE_EXECUTE msg", &sockaddr);
    return true;
}

bool Node::handle_juice_finish(int src_fd, msg_t* msg)
{
    int fd = find_contact_fd(msg->src);

    cur_job.set_done(fd);

    logger.log("Recv JUICE_FINISH msg", &sockaddr);
    return true;
}

bool Node::handle_juice_available(int src_fd, msg_t* msg)
{
    juice_msg_t* m = (juice_msg_t*)msg->payload;
    string exe = m->j_exe;

    cout << __func__ << ": outfile: " << m->outfile << endl;

    neighbor_task = Task(exe, m->num_j, JUICE, m->outfile, get_real_fname(machine_num, ""));
    for (int i = 0; i < m->num_src_files; i++)
        neighbor_task.set_src_file(m->src_filename[i]);

    logger.log("Recv JUICE_AVAILABLE msg", &sockaddr);
}

bool Node::handle_juice_started(int src_fd, msg_t* msg)
{
    wanted_job.is_executed = true;

    logger.log("Recv JUICE_STARTED msg", &sockaddr);
}

bool Node::handle_maple_finish(int src_fd, msg_t* msg)
{
    logger.log("Recv MAPLE_FINISH msg", &sockaddr);

    int fd = find_contact_fd(msg->src);

    cur_job.set_done(fd);

    return true;
}

bool Node::handle_maple_execute(int src_fd, msg_t* msg)
{
    logger.log("Recv MAPLE_EXECUTE msg", &sockaddr);

    int fd = find_contact_fd(msg->src);
    maple_msg_t* m = (maple_msg_t*)msg->payload;

    /* Notifying the neighbor */
    Contact c = find_aclkwise_contact().second;
    send_tcp(MAPLE_AVAILABLE, c.get_cfd(), m, sizeof(maple_msg_t));

    string exe = m->m_exe;

    cur_task = Task(exe, m->num_m, MAPLE, m->out_file_prefix, get_real_fname(machine_num, ""));
    string local;
    for (int i = 0; i < m->num_src_files; i++) {
        cur_task.set_src_file(getfile(m->src_filename[i],local));
    }

    pthread_t th;
    pthread_create(&th, NULL, _execute_task, NULL);
       
    // send_tcp(MAPLE_FINISH, fd, NULL, 0);
    return true;
}

bool Node::handle_maple_start(int src_fd, msg_t* msg)
{
    /* Only contact point receive MAPLE_START */
    if (!is_contact) {
        cerr << "Shouldn't receive MAPLE_START" << endl;
        return false;
    }

    int cfd = find_contact_fd(msg->src);
    maple_msg_t* m = (maple_msg_t*)msg->payload;

    if (!cur_job.is_done())
    {
        send_tcp(MJ_REJECT, cfd, NULL, 0);
        return true;
    } else {
        logger.begin_benchmark();
        cout << "Previous job is done." << endl;
        cur_job = Job(m->m_exe, m->num_m, MAPLE, m->out_file_prefix);

        int fd;
        for (int i = 0; i < m->num_src_files; i++)
        {
            fileexists(m->src_filename[i], &fd);
            cout << __func__ << "filename: " << m->src_filename[i] << " fd: " << fd << endl;
            cur_job.set_src_file(fd, m->src_filename[i]);
        }
        cur_job.allocate_jobs(contacts);
        map<int, void *> messagelist = cur_job.getSendMessageList();
        map<int, void *>::iterator it = messagelist.begin();
        bool needToProcessLocally = false;
        maple_msg_t* sameNodeMsg = (maple_msg_t *)it->second;
        if (it->first == -1)
        {
            needToProcessLocally = true;
            it++;
        }

        for (;it != messagelist.end(); it++) {
            send_maple_execute(it->first, it->second);
        }

        if (needToProcessLocally) {
            maple_msg_t* m = sameNodeMsg;

            /* Notifying the neighbor */
            Contact c = find_aclkwise_contact().second;
            cout << __func__ << ": outfile: " << m->out_file_prefix << endl;
            cout << __func__ << " num src files: " << m->num_src_files << endl;
            send_tcp(MAPLE_AVAILABLE, c.get_cfd(), m, sizeof(maple_msg_t));

            string exe = m->m_exe;
            cout << __func__ << ": exe: " << exe << endl;

            cur_task = Task(exe, m->num_m, MAPLE, m->out_file_prefix, get_real_fname(machine_num, ""));
            string local;
            for (int i = 0; i < m->num_src_files; i++) {
                cur_task.set_src_file(getfile(m->src_filename[i],local));
            }

            pthread_t th;
            pthread_create(&th, NULL, _execute_task, NULL);

            cur_job.set_done(-1);
        }
    }
   
    send_tcp(MAPLE_STARTED, cfd, NULL, 0);

    logger.log("Recv MAPLE_START msg", &sockaddr);
    return true;
}

bool Node::handle_maple_available(int src_fd, msg_t* msg)
{
    logger.log("Recv MAPLE_AVAILABLE msg", &sockaddr);

    int i;
    maple_msg_t* m = (maple_msg_t*)msg->payload;
    string exe = m->m_exe;

    neighbor_task = Task(exe, m->num_m, MAPLE, m->out_file_prefix, get_real_fname(machine_num, ""));
    for (i = 0; i < m->num_src_files; i++)
        neighbor_task.set_src_file(m->src_filename[i]);
}

bool Node::handle_task_finished(int src_fd, msg_t* msg)
{
    neighbor_task = Task();

    logger.log("Recv TASK_FINISHED msg", &sockaddr);
}

bool Node::handle_maple_started(int src_fd, msg_t* msg)
{
    wanted_job.is_executed = true;

    logger.log("Recv MAPLE_STARTED msg", &sockaddr);
}
