#ifndef PRODCONS_H
#define PRODCONS_H

#include "queue.h"
#include <pthread.h>

typedef struct {
	queue_t q;
	int max;   // max number of items
	int count; // current number of items

	pthread_mutex_t lock;
	pthread_cond_t cond;
} BoundedQueue;

extern BoundedQueue *bq;

void boundedqueue_init(BoundedQueue* bq, int max);
void boundedqueue_enqueue(BoundedQueue* bq, void *item);
void* boundedqueue_dequeue(BoundedQueue* bq);

#endif /* PRODCONS_H */
