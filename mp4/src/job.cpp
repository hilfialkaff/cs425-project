#include "job.h"
#include "log.h"
#include "utils.h"

Job::Job()
{
    return;
}

Job::Job(string exe, int num, job_type type, string outfile) :
    exe(exe), num(num), type(type), outfile(outfile)
{
    return;
}

void Job::set_src_file(int fd, string src_file)
{
    this->src_files.insert(pair<int, string>(fd, src_file));
}

void Job::set_done(int cfd)
{
    cout << __func__ << " set_done: " << cfd << endl;

    free(sendMessageList.find(cfd)->second);

    map<int, int>::iterator it;
    for (it = cfds.begin(); it != cfds.end(); it++) {
        if ((*it).first == cfd) {
            (*it).second--;
            break;
        }
    }
    
    if (is_done()) {
        if (type == MAPLE)
            logger.end_benchmark("Finish MAPLE Task");
        else
            logger.end_benchmark("Finish JUICE Task");
        cout << "Current Job is done." << endl;
    }
}

void Job::allocate_jobs(map<unsigned int, Contact> contacts)
{
    if (type == MAPLE)
        allocate_maple_jobs(contacts);
    else
        allocate_juice_jobs(contacts);
}

void Job::allocate_juice_jobs(map<unsigned int, Contact> contacts)
{
    vector<string> sendlist;
    int orig;
    
    for (multimap<int, string>::iterator it = src_files.begin(); it != src_files.end(); it++)
    {
        if (it == src_files.begin())
        {
            sendlist.push_back(it->second);
            orig = it->first;
        }
        else if (orig == it->first)
            sendlist.push_back(it->second);
        else
        {
            juice_msg_t* exec_msg = new juice_msg_t;
            memset(exec_msg, '\0', sizeof(exec_msg));
            strncpy(exec_msg->j_exe, exe.c_str(), MAX_NAME);
            exec_msg->num_j = num;
            strncpy(exec_msg->outfile, outfile.c_str(), MAX_NAME);

            // Send the maple execute message out to the node with the hash id
            exec_msg->num_src_files = sendlist.size();
            for (int i = 0; i < sendlist.size(); i++)
                strncpy(exec_msg->src_filename[i], sendlist[i].c_str(), MAX_NAME);

            sendMessageList.insert(pair<int, void *>(orig, (void*)exec_msg));
            // TODO
            cfds.insert(pair<int, int>(orig, 1));    
            // Reset the file list collection
            orig = it->first;
            sendlist.clear();
            sendlist.push_back(it->second);
        }
    }

    juice_msg_t* exec_msg = new juice_msg_t;
    memset(exec_msg, '\0', sizeof(exec_msg));
    strncpy(exec_msg->j_exe, exe.c_str(), MAX_NAME);
    exec_msg->num_j = num;
    strncpy(exec_msg->outfile, outfile.c_str(), MAX_NAME);

    exec_msg->num_src_files = sendlist.size();
    for (int i = 0; i < sendlist.size(); i++)
        strncpy(exec_msg->src_filename[i], sendlist[i].c_str(), MAX_NAME);

    sendMessageList.insert(pair<int, void *>(orig, (void*)exec_msg));
    cfds.insert(pair<int, int>(orig, 1));    
}

void Job::allocate_maple_jobs(map<unsigned int, Contact> contacts)
{
    vector<string> sendlist;
    int orig;
    
    for (multimap<int, string>::iterator it = src_files.begin(); it != src_files.end(); it++)
    {
        cout << __func__ << " 1st: " << it->first << " 2nd: " << it->second << endl;
        if (it == src_files.begin())
        {
            sendlist.push_back(it->second);
            orig = it->first;
        }
        else if (orig == it->first)
            sendlist.push_back(it->second);
        else
        {
            maple_msg_t* exec_msg = new maple_msg_t;
            memset(exec_msg, '\0', sizeof(exec_msg));
            strncpy(exec_msg->m_exe, exe.c_str(), MAX_NAME);
            exec_msg->num_m = num;
            strncpy(exec_msg->out_file_prefix, outfile.c_str(), MAX_NAME);

            // Send the maple execute message out to the node with the hash id
            exec_msg->num_src_files = sendlist.size();
            for (int i = 0; i < sendlist.size(); i++)
                strncpy(exec_msg->src_filename[i], sendlist[i].c_str(), MAX_NAME);

            sendMessageList.insert(pair<int, void *>(orig, (void*)exec_msg)); 
            cfds.insert(pair<int, int>(orig, 1));

            // Reset the file list collection
            orig = it->first;
            sendlist.clear();
            sendlist.push_back(it->second);
        }
    }

    maple_msg_t* exec_msg = new maple_msg_t;
    memset(exec_msg, '\0', sizeof(exec_msg));
    strncpy(exec_msg->m_exe, exe.c_str(), MAX_NAME);
    exec_msg->num_m = num;
    strncpy(exec_msg->out_file_prefix, outfile.c_str(), MAX_NAME);

    // Send the maple execute message out to the node with the hash id
    exec_msg->num_src_files = sendlist.size();
    cout << __func__ << " 2 size: " << sendlist.size() << " str: " << sendlist[0] << endl;
    for (int i = 0; i < sendlist.size(); i++)
        strncpy(exec_msg->src_filename[i], sendlist[i].c_str(), MAX_NAME);

    sendMessageList.insert(pair<int, void *>(orig, (void*)exec_msg));
    cfds.insert(pair<int, int>(orig, 1));    
}

bool Job::is_done()
{
    bool ret = true;
    map<int, int>::iterator it;

    for (it = cfds.begin(); it != cfds.end(); it++) {
        if ((*it).second) {
            ret = false;
            break;
        }
    }

    return ret;
}

bool Job::is_task_done(int fd)
{
    return !(cfds.find(fd)->second);
}

void Job::addcfdcount(int fd)
{
    cfds.find(fd)->second++;
}

vector<int> Job::get_cfds()
{
    vector<int> ret;
    map<int, int>::iterator it;

    for (it = cfds.begin(); it != cfds.end(); it++) {
        ret.push_back((*it).first);
    }

    return ret;
}
