#ifndef TASK_H
#define TASK_H

#include "constants.h"
#include "contact.h"
#include "utils.h"

#include <vector>
#include <string>
#include <sys/stat.h>
#include <fcntl.h>
#include <fstream>

using namespace std;

typedef struct task_exec {
    int start;
    int end;
    vector<string> src_files;
    string outfile;
    string exe;
    string dir;
    int num;
} task_exec_t;

class Task
{
public:
    Task();
    Task(string exe, int num, job_type type, string outfile);
    Task(string exe, int num, job_type type, string outfile, string dir);

    /* Set the src files */
    void set_src_file(string src_file);

    void set_is_done(bool is_done);

    /* TODO: Check when files are being cleaned */
    void execute();

    bool get_is_done() { return this->is_done; }
    string get_exe() { return this->exe; }
    int get_num() { return this->num; }
    job_type get_job_type() { return this->type; }
    string get_outfile() { return this->outfile; }
    vector<string> get_src_files() { return this->src_files; }
    void reset_src_files() { this->src_files.clear(); }

private:
    string exe; /* Name of executable */
    int num; /* Number of maple/juice tasks */
    job_type type; /* Job type: maple or juice */
    string outfile; /* Output file */
    vector<string> src_files; /* Input file */
    bool is_done; /* Is the task done? */
    string dir;
};

#endif /* TASK_H */
