#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <time.h>

/* Max msg length to be transmitted */ 
#define MAX_MSG 8192

/* File which stores the information about the contact point */
#define CONF_NAME "contact_point.conf"

/* Delimiter in conf file */
#define CONF_DELIMITER ':'

/* Base directory for log files */
#define BASE_DIR "logs/"

/* The interval in which heartbeat message is sent and checked */
#define HB_INTERVAL 10

/* Chord number of bit*/
#define NUM_BIT 3

/* Max number of clients in the sdfs */
#define MAX_CLIENTS 128

const struct timespec zero = {0, 0};

/* Max number of chars for any name */
#define MAX_NAME 50

/* Number of concurrent maple tasks */
#define NUM_MAPLE 3

/* Job type that exists in the system */
enum job_type { MAPLE, JUICE };

#endif /* CONSTANTS_H */
