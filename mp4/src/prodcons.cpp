
#include "prodcons.h"
#include <stdio.h>

    BoundedQueue *bq;
    
    void boundedqueue_init(BoundedQueue* bq, int max)
    {
    	queue_init(&bq->q);
    	bq->max = max;
    	bq->count = 0;
    	pthread_mutex_init(&bq->lock, NULL);
    	pthread_cond_init(&bq->cond, NULL);
    }
    
    void boundedqueue_enqueue(BoundedQueue* bq, void *item)
    {
        pthread_mutex_lock(&bq->lock);
    
        if (bq->count == bq->max) {
            pthread_mutex_unlock(&bq->lock);
            return;
        }

        // while (bq->count >= bq->max) {
        //     pthread_cond_wait(&bq->cond, &bq->lock);
        // }
    
        queue_enqueue(&bq->q, item);
        bq->count++;
        // pthread_cond_broadcast(&bq->cond); // wake up consumer
    
        pthread_mutex_unlock(&bq->lock);
    }
    
    void* boundedqueue_dequeue(BoundedQueue* bq)
    {
        printf("%s: stuck\n", __func__);

        pthread_mutex_lock(&bq->lock);
    
        // while (bq->count == 0) {
        //     pthread_cond_wait(&bq->cond, &bq->lock);
        // }
    
        if (bq->count == 0) {
            pthread_mutex_unlock(&bq->lock);
            return NULL;
        }

        void *result = queue_dequeue(&bq->q);
        bq->count--;
        // pthread_cond_broadcast(&bq->cond); // wake up consumer
    
        pthread_mutex_unlock(&bq->lock);
    
        return result;
    }
