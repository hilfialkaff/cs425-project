#ifndef CLIENT_H
#define CLIENT_H

#include <netinet/in.h>
#include <string>
#include <cstring>
#include <errno.h>
#include <iostream>
#include <vector>
#include <time.h>
#include <fcntl.h>

#include "constants.h"
#include "network.h"
#include "contact.h"
#include "log.h"
#include "messages.h"
#include "job.h"
#include "task.h"
#include "utils.h"
#include <pthread.h>
using namespace std;

typedef struct
{
    string sdfsfilename;
    string localfilename;
}FileEntry;

/**
 * Storing the state of the current program
 */
class Node
{
public:

    /* Constructor */
    Node();

    /* Populate the node's information */
    void bootstrap(string ip, int port, string num);

    /* Socket setup procedures and try to join the system (if it is not the contact point) */
    void setup();

    /* Clean up the resources associated with the connections */
    void cleanup();

    /* Wrapper function to send messages */
    void send(int type, void* arg);

    /* Send different types of msg */
    void send_join(void* arg);
    void send_leave(void* arg);
    void send_heartbeat(void* arg);
    void send_new_member(void* arg);
    void send_member_list(void* arg);
    void send_contact_rejoin(void* arg);
    void send_member_die(void* arg);
    void send_election_req(void* arg);
    void send_new_leader(void* arg);
    void send_juice_start(void* arg);
    void send_maple_start(void* arg);
    void send_maple_execute(int fd, void* arg);
    void send_juice_execute(int fd, void* arg);

    void send_fileop(int type, string sdfsfilename=string(), string localfilename=string());

    /* Wrapper function to handle TCP/UDP messages */
    bool handle_udp();
    bool handle_tcp(int fd);

    /* Handle different types of UDP messages */
    bool handle_join(struct sockaddr_in src, msg_t* msg);
    bool handle_leave(struct sockaddr_in src, msg_t* msg);
    bool handle_heartbeat(struct sockaddr_in src, msg_t* msg);
    bool handle_member_list(struct sockaddr_in src, msg_t* msg);
    bool handle_member_die(struct sockaddr_in src, msg_t* msg);
    bool handle_election_req(struct sockaddr_in src, msg_t* msg);
    bool handle_new_leader(struct sockaddr_in src, msg_t* msg);

    /* Handle different types of TCP messages */
    bool handle_new_member(int src_fd, msg_t* msg);
    bool handle_get(int src_fd, msg_t* msg);
    bool handle_put(int src_fd, msg_t* msg);
    bool handle_append(int src_fd, msg_t* msg);
    bool handle_spl_put(int src_fd, msg_t* msg);
    bool handle_del(int src_fd, msg_t* msg);
    bool handle_file_exists(int src_fd, msg_t* msg);
    bool handle_intermediate_put(int src_fd, msg_t* msg);
#ifdef REPL
    bool handle_repl_del(int src_fd, msg_t* msg);
    bool handle_repl_put(int src_fd, msg_t* msg);
    bool handle_new_repl_get(int src_fd, msg_t* msg);
#endif /* REPL */
    bool handle_new_file_get(int src_fd, msg_t* msg);
    bool handle_mj_reject(int src_fd, msg_t* msg);
    bool handle_juice_start(int src_fd, msg_t* msg);
    bool handle_juice_execute(int src_fd, msg_t* msg);
    bool handle_juice_finish(int src_fd, msg_t* msg);
    bool handle_juice_available(int src_fd, msg_t* msg);
    bool handle_juice_started(int src_fd, msg_t* msg);
    bool handle_maple_start(int src_fd, msg_t* msg);
    bool handle_maple_execute(int src_fd, msg_t* msg);
    bool handle_maple_finish(int src_fd, msg_t* msg);
    bool handle_maple_available(int src_fd, msg_t* msg);
    bool handle_maple_started(int src_fd, msg_t* msg);
    bool handle_task_finished(int src_fd, msg_t* msg);
    bool handle_list_files(int src_fd, msg_t* msg);
    bool handle_filename_list(int src_fd, msg_t* msg);

    /* Check the status of all the contacts */
    void check_heartbeat();

    unsigned int hash(const char* s, bool isnode);

    string getfile(string sdfs, string local);
    //void appendfile(string sdfs, int fd);
    void putfile(string sdfs, string local);
    void putintermediatefile(string sdfs, string local);
    void delfile(string sdfs);
    bool fetchfile(int fd, string sdfs, string local);
    bool receivefile(int fd, string local, bool append = false);
    bool pushfile(int fd, string sdfs);
    bool removefile(int fd, string sdfs);

    bool verify_delete(int fd);

    bool fileexists(string sdfs, int* fd);

#ifdef REPL
    void recv_replicas(int fd);
    bool copyreplica(int fd, string sdfs, string local);
#endif /* REPL */

    void recv_files(int fd);

    bool copyfile(int fd, string sdfs, string local);

    bool copylocally(string sdfs, string local);

    bool appendlocally(string sdfs, string local);

    void enqueue_job(job_type type, void* msg, int len);

    void issue_job();

    void set_cfds(fd_set* fds);
    void check_cfds(fd_set* fds);
    void remove_cfd(int cfd);

    bool _recv_tcp(int fd, msg_t* msg);

    void setup_contact();

    /* Find logical (anti)clockwise contact */
    pair<unsigned int, Contact> find_clkwise_contact();
    pair<unsigned int, Contact> find_aclkwise_contact();

    /* Print current contact list */
    void print_contacts_list();

    /* Find sockaddr based on the cfd */
    int find_contact_fd(struct sockaddr_in addr);

    /* Destructor */
    ~Node();

    bool insert_cfd(int fd)
    {
        cfds.push_back(fd);
    }

    int get_udp_fd()
    {
        return this->udp_fd;
    }

    int get_datafd()
    {
        return this->datafd;
    }

    bool get_is_contact()
    {
        return this->is_contact;
    }

    void set_is_contact(bool is_contact)
    {
        this->is_contact = is_contact;
    }

    pair<unsigned int, Contact> get_clkwise_node()
    {
        return clkwise_node;
    }

    pair<unsigned int, Contact> get_aclkwise_node()
    {
        return aclkwise_node;
    }

    struct sockaddr_in* get_sockaddr()
    {
        return &(this->sockaddr);
    }

    struct sockaddr_in* get_contact_sockaddr()
    {
        return &(this->contact_sockaddr);
    }

    string get_machine_num() { return this->machine_num; };

    bool get_is_connected()
    {
        return this->is_connected;
    }

    map<unsigned int, Contact> get_contacts()
    {
        return this->contacts;
    }

    void execute_task();

private:

    bool update_contacts()
    {
        aclkwise_node = find_aclkwise_contact();
        cout << __func__ << " hashid: " << hashid << " clkwise: " << ntohs(aclkwise_node.second.get_sockaddr()->sin_port) << endl;
        clkwise_node = find_clkwise_contact();
        cout << __func__ << " hashid: " << hashid << " clkwise: " << ntohs(clkwise_node.second.get_sockaddr()->sin_port) << endl;
    }

    bool update_contacts_onadd();
    bool update_contacts_onrem();

    bool is_contact; /* Is this the contact point? */
    bool is_connected; /* Is this node alive? */
    int udp_fd; /* UDP fd to listen on */
    int datafd; /* TCP fd  to listen on */
    struct sockaddr_in sockaddr; /* Port/IP */
    struct sockaddr_in contact_sockaddr; /* Port/IP of contact contact */
    int contact_fd; /* TCP socket of contact point */
    map<unsigned int, Contact> contacts; /* <key, connected contacts> */

    unsigned int hashid; /* Node Hash ID */
    vector<FileEntry> clockwisefilelist; /* Files with hash >= hashid and < clockwise node's hashid */
    vector<FileEntry> anticlockwisereplicafilelist; /* File replicas with hash < hashid and >= anticlockwise node's hashid*/
    vector<FileEntry> cachedfilelist;
    pair<unsigned int, Contact> clkwise_node;
    pair<unsigned int, Contact> aclkwise_node;

    vector<int> cfds;

    string machine_num;

    /* For election purposes */
    bool is_candidate;
    bool is_leader;

    /***************************************************/
    /* For maple/juice functionalities */
    /***************************************************/

    /* For master: the job that's currently running in the system */
    Job cur_job;
    Job neighbor_job;

    /* For slave: the task that it needs to execute */
    Task cur_task;
    Task neighbor_task;

    /* For client issuing the job */
    typedef struct wanted_job {
        job_type type;
        bool is_executed;
        char* payload;
    } wanted_job_t;

    wanted_job_t wanted_job;
    bool job_exist;

    vector<string> get_file_with_prefix(string prefix);
    pthread_mutex_t mutex;
    pthread_mutex_t file_mutex;
};

extern Node node;

#endif /* CLIENT_H */
