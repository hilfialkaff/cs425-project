#ifndef NETWORK_H
#define NETWORK_H

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <errno.h>
#include <string>

#include "constants.h"
#include "messages.h"

using namespace std;

typedef struct thread_msg
{
    int type;
    int fd;
    void *payload;
    size_t size;
}thread_msg_t;

/* Wrapper function to send UDP message */
void send_udp(int type, int fd, struct sockaddr_in* dest_addr, void* payload, int len);

/* Wrapper function to send TCP message */
void enqueue_tcp(int type, int fd, void* payload, int len);
void send_tcp(int type, int fd, void* payload, int len);

/* Wrapper function to receive UDP message */
int recv_udp(int fd, msg_t* buf, struct sockaddr_in* src);

/* Wrapper function to receive TCP message */
bool recv_tcp(int fd, msg_t* msg);

#endif /* NETWORK_H */
