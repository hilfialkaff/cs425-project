#include "parser.h"

vector<string> Parser::tokenize(string str)
{
     vector<string> tokens;
     size_t p0 = 0, p1 = string::npos;
    
     while(p0 != string::npos) {
        p1 = str.find_first_of(' ', p0);
        if(p1 != p0) {
            string token = str.substr(p0, p1 - p0);
            tokens.push_back(token);
        }
        p0 = str.find_first_not_of(' ', p1);
    }
    return tokens;
}

void Parser::parse_join_cmd(Node& node)
{
    if (node.get_is_connected()) {
        cerr << "Already connected" << endl;
        return;
    }
    
    node.setup();
}

void Parser::parse_leave_cmd(Node& node)
{
    if (!node.get_is_connected()) {
        cerr << "Not yet connected" << endl;
        return;
    }

    node.send(LEAVE, NULL);
    node.set_is_contact(false);
    node.cleanup();
}

void Parser::parse_crash_cmd(Node& node)
{
    if (!node.get_is_connected()) {
        cerr << "Not yet connected" << endl;
        return;
    }
    node.cleanup();
}

void Parser::parse_get_cmd(Node& node, const char *cmd)
{
    // logger.begin_benchmark();
    if (!node.get_is_connected()) {
        cerr << "Not yet connected" << endl;
        return;
    }

    string command(cmd);
    size_t found = command.find_last_of(' ');            
    node.send_fileop(GET, command.substr(4,found - 4), command.substr(found + 1));
    // logger.end_benchmark("GETFILE");
}

void Parser::parse_put_cmd(Node& node, const char* cmd)
{
    // logger.begin_benchmark();
    if (!node.get_is_connected()) {
        cerr << "Not yet connected" << endl;
        return;
    }

    string command(cmd);
    size_t found = command.find_last_of(' ');

    node.send_fileop(PUT, command.substr(found + 1), command.substr(4,found - 4));
    // logger.end_benchmark("PUTFILE");
}

void Parser::parse_del_cmd(Node& node, const char* cmd)
{
    if (!node.get_is_connected()) {
        cerr << "Not yet connected" << endl;
        return;
    }

    string command(cmd);
    size_t found = command.find_last_of(' ');            
    node.send_fileop(DEL, command.substr(found + 1));
}

void Parser::parse_juice_cmd(Node& node, const char* cmd)
{
    if (!node.get_is_connected()) {
        cerr << "Not yet connected" << endl;
        return;
    }

    string c(cmd);
    juice_msg_t msg;
    memset(&msg, '\0', sizeof(msg));

    vector<string> tokens = tokenize(cmd);
    strncpy(msg.j_exe, tokens.at(1).c_str(), MAX_NAME);
    msg.num_j  = atoi(tokens.at(2).c_str());
    msg.num_src_files = 1;
    strncpy(msg.src_filename[0], tokens.at(3).c_str(), MAX_NAME);
    strncpy(msg.outfile, tokens.at(4).c_str(), MAX_NAME);

    node.enqueue_job(JUICE, (void*)&msg, sizeof(msg));
}
    
void Parser::parse_maple_cmd(Node& node, const char* cmd)
{
    if (!node.get_is_connected()) {
        cerr << "Not yet connected" << endl;
        return;
    }

    string c(cmd);
    maple_msg_t msg;
    memset(&msg, '\0', sizeof(msg));

    vector<string> tokens = tokenize(cmd);
    strncpy(msg.m_exe, tokens.at(1).c_str(), MAX_NAME);
    msg.num_m  = NUM_MAPLE;
    msg.num_src_files = tokens.size() - 3;

    strncpy(msg.out_file_prefix, tokens.at(2).c_str(), MAX_NAME);

    for (int i = 3; i < tokens.size(); i++)
    {
        strncpy(msg.src_filename[i-3], tokens.at(i).c_str(), MAX_NAME);
    }

    node.enqueue_job(MAPLE, (void*)&msg, sizeof(msg));
}

void Parser::parse_cmd(Node& node, const char* cmd)
{
    if (!strncmp(cmd, JOIN_CMD, strlen(JOIN_CMD))) {
        parse_join_cmd(node);
    } else if (!strncmp(cmd, LEAVE_CMD, strlen(LEAVE_CMD))) {
        parse_leave_cmd(node);
    } else if (!strncmp(cmd, CRASH_CMD, strlen(CRASH_CMD))) {
        parse_crash_cmd(node);
    } else if (!strncmp(cmd, GET_CMD, strlen(GET_CMD))) {
        parse_get_cmd(node, cmd);
    } else if (!strncmp(cmd, PUT_CMD, strlen(PUT_CMD))) {
        parse_put_cmd(node, cmd);
    } else if (!strncmp(cmd, DEL_CMD, strlen(DEL_CMD))) {
        parse_del_cmd(node, cmd);
    } else if (!strncmp(cmd, MAPLE_CMD, strlen(MAPLE_CMD))) {
        parse_maple_cmd(node, cmd);
    } else if (!strncmp(cmd, JUICE_CMD, strlen(JUICE_CMD))) {
        parse_juice_cmd(node, cmd);
    }
}
