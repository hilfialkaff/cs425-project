#include "task.h"

Task::Task() :
    exe(""), is_done(true)
{

}

Task::Task(string exe, int num, job_type type, string outfile) :
    exe(exe), num(num), type(type), outfile(outfile), is_done(false)
{
    chmod((dir + exe).c_str(), S_IRUSR | S_IXUSR);

    return;
}

Task::Task(string exe, int num, job_type type, string outfile, string dir) :
    exe(exe), num(num), type(type), outfile(outfile), is_done(false), dir(dir)
{
    chmod((dir + exe).c_str(), S_IRUSR | S_IXUSR);

    return;
}

void Task::set_src_file(string src_file)
{
    this->src_files.push_back(src_file);
}

void Task::set_is_done(bool is_done)
{
    this->is_done = is_done;
}

void* _execute(void* arg)
{
    int start = ((task_exec_t*)arg)->start;
    int end = ((task_exec_t*)arg)->end;
    vector<string> src_files = ((task_exec_t*)arg)->src_files;
    string outfile = ((task_exec_t*)arg)->outfile;
    string exe = ((task_exec_t*)arg)->exe;
    string dir = ((task_exec_t*)arg)->dir;
    int num = ((task_exec_t*)arg)->num;
    int i;
    string src_file;

    string cmd = "./" + exe + " ";

    for (i = start; i < end; i++) {
        src_file = src_files.at(i);
        cmd += dir + src_file + " ";
    }

    char nums[5];
    sprintf(nums, "%d", num);
    cmd += outfile + "." + string(nums);
    // cmd += outfile;
    cout << __func__ << " cmd: " << cmd << endl;

    FILE* fp;
    fp = popen(cmd.c_str(), "w");
    if (!fp) {
        perror("popen()");
    }

    pclose(fp);

}

void Task::execute()
{
    vector<string>::iterator it;
    int i;
    int ret;

    int upto = min(num, (int)src_files.size());
    // int upto = 1;
    pthread_t th[upto];
    task_exec_t task[upto];
    task[0].end = 0;

    for (i = 0; i < upto; i++) {
        task[i].start = task[i].end;
        task[i].end = int((float(i+1) / upto) * src_files.size());
        cout << __func__ << " start: " << task[i].start << " end: " << task[i].end << endl;
        task[i].src_files = src_files;
        task[i].outfile = dir + outfile;
        task[i].exe = dir + exe;
        task[i].dir = dir;
        task[i].num = i;
        pthread_create(&th[i], NULL, _execute, (void*)&(task[i]));
        if (!ret) {
            perror("! pthread_create()");
        }
        task[i + 1].end = task[i].end;
    }

    for (i = 0; i < upto; i++) {
        ret = pthread_join(th[i], NULL);
        if (ret) {
            perror("! pthread_join()");
        }
    }

    for (i = 0; i < upto; i++) {
        char nums[1];
        sprintf(nums, "%d", i);
        cout << __func__ << " prefix: " << outfile + string(".") + string(nums) << endl;
        vector<string> ifnames = get_outfile_with_prefix(dir, outfile + string(".") + string(nums));
        vector<string>::iterator it;

        for (it = ifnames.begin(); it != ifnames.end(); it++) {
            string ifname = dir + (*it);
            string tmp = string(".") + string(nums);
            string ofname(ifname.c_str());
            ofname.erase(ofname.find(tmp.c_str()), tmp.size());

            cout << __func__ << ": concantenating " << ifname << " to: " << ofname << endl;
            ifstream in(ifname.c_str(), fstream::in);
            ofstream out(ofname.c_str(), fstream::out | fstream::app);
            string line;

            int count = 0;
            while(in.good()) {
                getline(in, line);
                out << line << endl;
                count++;
            }
            cout << __func__ << " ifname: " << ifname << " line: " << count << endl;

            in.close();
            out.close();

            int retval;
            retval = remove(ifname.c_str());
            if (retval != 0) {
                cout << __func__ << " Can't remove ifname: " << errno << endl;
            }
        }
    }
}
