#include "tester.h"

Tester tester;

void *test1(void *arg)
{
    sleep(2);
    node.putfile("test","files/test");
    sleep(2);
    node.putfile("test2","files/test2");
    sleep(2);
    node.putfile("test5","files/test5");
    sleep(2);
    Parser::parse_cmd(node, "maple maple result test test2 test5");
    sleep(5);
    Parser::parse_cmd(node, "juice juice 3 result final_result");
    sleep(5);
}

void *test2(void *arg)
{
    sleep(2);
    node.putfile("job.cpp","job.cpp");
    sleep(2);
    node.putfile("job.h","job.h");
    // sleep(2);
    // node.putfile("task.h","task.h");
    sleep(2);
    node.putfile("task.cpp","task.cpp");
    sleep(2);
    // Parser::parse_cmd(node, "maple maple result job.cpp job.h task.cpp task.h");
    Parser::parse_cmd(node, "maple maple result job.cpp job.h task.cpp");
}

void *test3(void* arg)
{
    sleep(2);
    node.putfile("gp12710.txt", "files/gp12710.txt");
    sleep(4);
    node.putfile("jbunc10.txt", "files/jbunc10.txt");
    sleep(4);
    node.putfile("mthesaur.txt", "files/mthesaur.txt");
    sleep(4);
    node.putfile("whewk12.txt", "files/whewk12.txt");
    sleep(4);
    // Parser::parse_cmd(node, "maple maple result gp12710.txt jbunc10.txt mthesaur.txt whewk12.txt");
}

void Tester::execute()
{
    if (node.get_machine_num() == string("4")) {
        pthread_t t;
        // pthread_create(&t, NULL, test1, NULL);
        pthread_create(&t, NULL, test2, NULL);
        // pthread_create(&t, NULL, test3, NULL);
    }
}
