#include "log.h"

Log logger;

Log::Log()
{
    return;
}

Log::~Log()
{
    fp.close();
    return;
}

void Log::prep_file(string num)
{
    string fname = "machine." + num + ".log";
    fp.open((BASE_DIR + fname).c_str(), fstream::out);
}

void Log::begin_benchmark()
{
#ifdef FILE_BM
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &beg);
#endif /* FILE_LOG */
}

void Log::end_benchmark(string msg)
{
#ifdef FILE_BM
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);

    fp << "Time taken for " << msg << ": " << (end.tv_sec - beg.tv_sec) * 1000000 + (end.tv_nsec - beg.tv_nsec) / 1000 << endl;
#endif /* FILE_LOG */
}

void Log::log(string msg, struct sockaddr_in* addr)
{
#ifdef DEBUG
    struct timespec t;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t);

    fp << "(" << inet_ntoa(addr->sin_addr) << ":" << ntohs(addr->sin_port) << ")@" << (t.tv_sec * 1000000) + (t.tv_nsec / 1000) << ": " << msg << endl;
#endif /* DEBUG */
}
