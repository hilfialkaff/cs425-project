#ifndef TESTER_H
#define TESTER_H

#include <pthread.h>

#include "parser.h"
#include "node.h"

class Tester {
public:
    void execute();
};

extern Tester tester;

#endif /* TESTER_H */
