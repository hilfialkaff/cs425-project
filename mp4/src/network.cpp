#include <pthread.h>

#include "node.h"
#include "utils.h"
#include "network.h"
#include "prodcons.h"

typedef struct send_msg {
    int fd;
    msg_t* msg;
} send_msg_t;

/* Wrapper function to send UDP message */
void send_udp(int type, int fd, struct sockaddr_in* dest_addr, void* payload, int len)
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*) bytes;
    int ret;
    int size;

    msg->type = type;

    /* For marshalling */
    if (len % 2) {
        msg->len = len + 1;
        if (msg->len - 1) {
            memcpy(msg->payload, payload, msg->len);
        }
    } else {
        msg->len = len;
        if (msg->len) {
            memcpy(msg->payload, payload, msg->len);
        }
    }

    size = msg_size(msg);

    // marshall(msg);
    ret = sendto(fd, (char*)msg, size, 0, (struct sockaddr*)dest_addr, sizeof(struct sockaddr));

    if (ret == -1) {
        cerr << __func__ << " fd: " << fd << " errno: " << errno << endl;
    }
}

// void enqueue_tcp(int type, int fd, void* payload, int len)
// {
//     cout << __func__ << " At" << endl;
//     thread_msg_t item;
//     item.type = type;
//     item.fd = fd;
//     item.payload = payload;
//     item.size = len;
//     boundedqueue_enqueue(bq, (void*)&item);
//     cout << __func__ << " Finish" << endl;
// }

/* Wrapper function to send TCP message */
void send_tcp(int type, int fd, void* payload, int len)
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*) bytes;
    int size;

    msg->type = type;
    msg->src = *(node.get_sockaddr());

    /* For marshalling */
    if (len % 2) {
        msg->len = len + 1;
        if (msg->len - 1) {
            memcpy(msg->payload, payload, msg->len);
        }
    } else {
        msg->len = len;
        if (msg->len) {
            memcpy(msg->payload, payload, msg->len);
        }
    }

    size = msg_size(msg);

    // marshall(msg);
    int ret = send(fd, (char*)msg, size, 0);

    if (ret == -1) {
        cerr << __func__ << " fd: " << fd << " errno: " << errno << endl;
    }
}

/* Wrapper function to receive UDP message */
int recv_udp(int fd, msg_t* msg, struct sockaddr_in* src)
{
    socklen_t size = sizeof(struct sockaddr_in);
    int ret = recvfrom(fd, (void*)msg, MAX_MSG, 0, (struct sockaddr*)src, &size);

    /* There is message to be read */
    // if (ret > 0) { 
    //     unmarshall(msg);
    // }

    return ret;
}

/* Wrapper function to receive TCP message */
bool recv_tcp(int fd, msg_t* msg)
{
    int ret = 0;
	fd_set master;

	FD_ZERO(&master);
	FD_SET(fd, &master);
    
	select(FD_SETSIZE, &master, NULL, NULL, NULL);
	if (FD_ISSET(fd, &master)) {
        ret = recv(fd, (void*)msg, sizeof(msg_t), 0);
	    // cout << __func__ << " 1st recv: " << ret << " len: " << msg->len << " type: " << msg->type << endl;
        if (ret <= 0 || !msg->len) {
            goto label;
        }

        ret = recv(fd, (void*)msg->payload, msg->len, 0);
	    // cout << __func__ << " 2nd recv: " << ret << endl;
	}

label:
    /* Client is disconnected */
    if (ret <= 0) {
        cout << "Client is disconnected" << endl;
        close(fd);
        fd = -1;
        return false;
    }

    // /* There is message to be read */
    // unmarshall(msg);

    return true;
}
