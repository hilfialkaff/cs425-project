#ifndef JOB_H
#define JOB_H

#include <vector>

#include "constants.h"
#include "contact.h"

using namespace std;

class Job
{
public:
    Job();
    Job(string exe, int num, job_type type, string outfile);

    /* Set the src files */
    void set_src_file(int fd, string src_file);

    /* Mark a client as done */
    void set_done(int cfd);
    
    /* Allocate the jobs */
    void allocate_jobs(map<unsigned int, Contact> contacts);

    /* Is this job done? */
    bool is_done();

    /* Is Task done */
    bool is_task_done(int fd);

    /* Retrieve the cfds used for the Maple/Juice job */
    vector<int> get_cfds();

    /* Get the message list to send to each node */
    map<int, void *> getSendMessageList()
    {
        return sendMessageList;
    }

    void addcfdcount(int fd);

private:
    map<int, int> cfds; /* key: fd, value: is it done? */
    map<int, void *> sendMessageList;

    /* Allocate the maple jobs */
    void allocate_maple_jobs(map<unsigned int, Contact> contacts);

    /* Allocate the juice jobs */
    void allocate_juice_jobs(map<unsigned int, Contact> contacts);

    string exe; /* Name of executable */
    int num; /* Number of maple/juice tasks */
    job_type type; /* Job type: maple or juice */
    string outfile; /* Output file */
    multimap<int, string> src_files; /* Input file */
};

#endif /* JOB_H */
