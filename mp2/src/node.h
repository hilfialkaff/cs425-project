#ifndef CLIENT_H
#define CLIENT_H

#include <netinet/in.h>
#include <string>
#include <cstring>
#include <fstream>
#include <errno.h>
#include <iostream>
#include <time.h>

#include "constants.h"
#include "network.h"
#include "contact.h"

using namespace std;

const struct timespec zero = {0, 0};

/**
 * Storing the state of the current program
 */
class Node
{
public:

    /* Constructor */
    Node();

    /* Populate the node's information */
    void bootstrap(string ip, int port, bool is_contact,
        string contact_ip, int contact_port, string num);

    /* Socket setup procedures and try to join the system (if it is not the contact point) */
    void setup();

    /* Clean up the resources associated with the connections */
    void cleanup();

    /* Parse command from user input */
    void parse_cmd(const char* cmd);

    /* Send a msg corresponding to type */
    void send(int type);

    /* Receive and handle the msg */
    bool recv();

    /* Check the status of all the contacts */
    void check_heartbeat();

    int get_fd()
    {
        return this->fd;
    }

    bool get_is_contact()
    {
        return this->is_contact;
    }

    bool get_is_connected()
    {
        return this->is_connected;
    }

    /* Logging message */
    void log(string msg, struct sockaddr_in* addr)
    {
        fp << "(" << inet_ntoa(addr->sin_addr) << ":" << ntohs(addr->sin_port) << ")" << msg << endl;
    }

    /* Destructor */
    ~Node();

private:
    bool is_contact; /* Is this the contact point? */
    bool is_connected; /* Is this node alive? */
    int fd; /* File descriptor to listen on */
    struct sockaddr_in sockaddr; /* Port/IP */
    struct sockaddr_in contact_sockaddr; /* Port/IP of contact contact */
    fstream fp; /* For logging purpose */
    vector<Contact> contacts; /* Connected contacts */
};

#endif /* CLIENT_H */
