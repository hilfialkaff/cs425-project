#ifndef NETWORK_H
#define NETWORK_H

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <errno.h>
#include <string>

#include "constants.h"

using namespace std;

/* Stores the basic structure of msg to be sent in the network */
typedef struct msg {
    uint32_t type;
    uint32_t len;
    char payload[0];
} __attribute__((packed)) msg_t;

/* Different type of msg a server can send */
enum {
    JOIN = 0x1,
    LEAVE,
    HEARTBEAT,
    NEW_MEMBER,
    MEMBER_LIST,
    CONTACT_REJOIN
};

/* Marshall a msg by converting it into network byte order */
inline void marshall(msg_t* msg)
{
    uint32_t i;
    uint16_t tmp;

    /* Fancy pointer trick since htons takes in 2 bytes
     * while our payload is a char array (1 byte each) */
    for (i = 0;i < msg->len;i += 2) {
        tmp = htons(*(uint16_t*)(msg->payload + i));
        memcpy(msg->payload + i, (void*)&tmp, sizeof(uint16_t));
    }

    msg->type = htonl(msg->type);
    msg->len = htonl(msg->len);
}

/* Unmarshall a msg by converting it into host byte order */
inline void unmarshall(msg_t* msg)
{
    uint32_t i;
    uint16_t tmp;

    msg->type = ntohl(msg->type);
    msg->len = ntohl(msg->len);

    /* Fancy pointer trick since ntohs takes in 2 bytes
     * while our payload is a char array (1 byte each) */
    for (i = 0;i < msg->len; i+=2) {
        tmp = ntohs(*(uint16_t*)(msg->payload + i));
        memcpy(msg->payload + i, (void*)&tmp, sizeof(uint16_t));
    }
}

/* Print ip & port of sockaddr_in */
inline void print_ip_port(struct sockaddr_in s)
{
#ifdef DEBUG
    cout << "ip: " << inet_ntoa(s.sin_addr) << " port: " << ntohs(s.sin_port) << endl;
#endif /* DEBUG */
}

/* Find the total length of a message */
inline int msg_size(msg_t* msg)
{
    return sizeof(msg_t) + msg->len;
}

/* Setup the sockaddr struct */
inline void setup_sockaddr(struct sockaddr_in* sockaddr, int port, string ip)
{
    memset(sockaddr, 0, sizeof(sockaddr));

    sockaddr->sin_family = AF_INET;
    sockaddr->sin_port = htons(port);
    sockaddr->sin_addr.s_addr = inet_addr(ip.c_str());
}

/* Return true if port and ip equal */
inline bool sockaddr_cmp(struct sockaddr_in* s1, struct sockaddr_in* s2)
{
    return (s1->sin_port == s2->sin_port) &&
        !strcmp(inet_ntoa(s1->sin_addr), inet_ntoa(s2->sin_addr));
}

/* Send a message via UDP */
void send_msg(int type, int fd, struct sockaddr_in* dest_addr, void* payload, int len);

/* Recv a message via UDP */
int recv_msg(int fd, msg_t* buf, struct sockaddr_in* src);

#endif /* NETWORK_H */
