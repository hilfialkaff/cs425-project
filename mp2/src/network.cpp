#include "network.h"

/* Send a message via UDP */
void send_msg(int type, int fd, struct sockaddr_in* dest_addr, void* payload, int len)
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*) bytes;
    int ret;
    int size;

    msg->type = type;

    /* For marshalling */
    if (len % 2) {
        msg->len = len + 1;
        if (msg->len - 1) {
            memcpy(msg->payload, payload, msg->len);
        }
    } else {
        msg->len = len;
        if (msg->len) {
            memcpy(msg->payload, payload, msg->len);
        }
    }


    size = msg_size(msg);

#ifdef DEBUG
    printf("Send %s:%d: ",inet_ntoa(dest_addr->sin_addr), ntohs(dest_addr->sin_port));
    for(int i = 0; i < msg_size(msg); i++) {
        printf("0x%x ", ((char*)msg)[i]);
    }
    printf("\n");
#endif /* DEBUG */

    marshall(msg);
    ret = sendto(fd, (char*)msg, size, 0, (struct sockaddr*)dest_addr, sizeof(struct sockaddr));

    if (ret == -1) {
        cerr << __func__ << " fd: " << fd << " errno: " << errno << endl;
    }
}

/* Recv a message via UDP */
int recv_msg(int fd, msg_t* msg, struct sockaddr_in* src)
{
    socklen_t size = sizeof(struct sockaddr_in);
    int ret = recvfrom(fd, (void*)msg, MAX_MSG, 0, (struct sockaddr*)src, &size);

    /* There is message to be read */
    if (ret > 0) { 
        unmarshall(msg);
    }

#if DEBUG
    printf("Recv %s:%d: ",inet_ntoa(src->sin_addr), ntohs(src->sin_port));
    if (ret > 0) {
        for(int i = 0; i < ret; i++) {
            printf("0x%x ", ((char*)msg)[i]);
        }
    }
    printf("\n");
#endif /* DEBUG */

    return ret;
}
