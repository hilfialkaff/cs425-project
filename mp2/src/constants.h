/* Max msg length to be transmitted */
#define MAX_MSG 2048 

/* File which stores the information about the contact point */
#define CONF_NAME "contact_point.conf"

/* Delimiter in conf file */
#define CONF_DELIMITER ':'

/* Base directory for log files */
#define BASE_DIR "logs/"

/* The interval in which heartbeat message is sent and checked */
#define HB_INTERVAL 3
