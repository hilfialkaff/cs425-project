#include "node.h"

/* Constructor */
Node::Node()
{
    return;
}

/* Populate the node's information */
void Node::bootstrap(string ip, int port, bool is_contact,
        string contact_ip, int contact_port, string num)
{
    setup_sockaddr(&sockaddr, port, ip);
    setup_sockaddr(&contact_sockaddr, contact_port, contact_ip);
    this->is_contact = is_contact;

    string fname = "machine." + num + ".log";
    fp.open((BASE_DIR + fname).c_str(), fstream::out);

    this->setup();
}

/* Socket setup procedures and try to join the system (if it is not the contact point) */
void Node::setup()
{
    this->fd = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0);

    if (fd < 0) {
        cerr << "! Socket: " << errno << endl;
        exit(1);
    }

    if (bind(fd, (struct sockaddr*)(&(this->sockaddr)), sizeof(struct sockaddr))) {
        cerr << "! Bind: " << errno << endl;
        exit(1);
    }

    if (!is_contact) {
        do {
            this->send(JOIN);
            sleep(1);
        } while (this->recv() <= 0);

        Contact contact(this->contact_sockaddr, zero);
        contacts.push_back(contact);
    }

    this->is_connected = true;
}

/* Clean up the resources associated with the connections */
void Node::cleanup()
{
    close(this->fd);
    this->fd = -1;
    this->is_connected = false;

    if (!this->is_contact) {
        contacts.clear();
    }
}

/* Parse command from user input */
void Node::parse_cmd(const char* cmd)
{
    if (!strcmp(cmd, "join")) {
        if (this->is_connected) {
            cerr << "Already connected" << endl;
            return;
        }

        this->setup();
        if (this->is_contact) {
            this->send(CONTACT_REJOIN);
        }
    } else if (!strcmp(cmd, "leave")) {
        if (!this->is_connected) {
            cerr << "Not yet connected" << endl;
            return;
        }
        this->send(LEAVE);
        this->cleanup();
    } else if (!strcmp(cmd, "crash")) {
        if (!this->is_connected) {
            cerr << "Not yet connected" << endl;
            return;
        }
        this->cleanup();
    }
}

/* Send a msg corresponding to type */
void Node::send(int type)
{
    vector<Contact>::iterator it;

    switch(type) {
    case HEARTBEAT:
        uint64_t ctr;
        for (it = contacts.begin(); it != contacts.end(); it++) {
            ctr = it->get_ctr();
            send_msg(type, this->fd, it->get_sockaddr(), &ctr, sizeof(uint64_t));
            it->inc_ctr();

            log("Sending heartbeat", it->get_sockaddr());
        }
        break;

    case JOIN:
        struct timespec cur;
        char tmp[100];
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cur);

        send_msg(type, this->fd, &(this->contact_sockaddr), &cur, sizeof(struct timespec));
        sprintf(tmp, "Sending JOIN msg at %ld", cur.tv_sec * 1000000 + cur.tv_nsec / 1000);
        log(tmp, &(this->contact_sockaddr));
        break;

    case LEAVE:
        for (it = contacts.begin(); it != contacts.end(); it++) {
            send_msg(type, this->fd, it->get_sockaddr(), NULL, 0);
            log("Sending LEAVE msg", it->get_sockaddr());
        }
        break;

    case NEW_MEMBER:
        for (it = contacts.begin(); it != contacts.end() - 1; it++) {
            send_msg(type, this->fd, it->get_sockaddr(), (void*)(contacts.back().get_sockaddr()), sizeof(struct sockaddr_in));

            log("Sending NEW MEMBER msg", it->get_sockaddr());
        }
        break;

    case MEMBER_LIST:
        struct sockaddr_in sockaddrs[contacts.size() - 1];
        for (it = contacts.begin(); it != contacts.end() - 1; it++) {
            sockaddrs[it - contacts.begin()] = *(it->get_sockaddr());
        }
        send_msg(type, this->fd, contacts.back().get_sockaddr(), (void*)sockaddrs, sizeof(sockaddrs));
        log("Sending MEMBER LIST msg", contacts.back().get_sockaddr());
        break;

    case CONTACT_REJOIN:
        if (!this->is_contact) {
            cout << "Non-contact shouldn't send CONTACT_REJOIN" << endl;
        }

        for (it = contacts.begin(); it != contacts.end(); it++) {
            send_msg(NEW_MEMBER, this->fd, it->get_sockaddr(), (void*)&this->sockaddr, sizeof(struct sockaddr_in));
            log("Sending NEW MEMBER msg", it->get_sockaddr());
        }
        break;

    default:
        cerr << "Invalid msg type: " << type << endl;
    }
}

/* Receive and handle the msg */
bool Node::recv()
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*)bytes;
    int ret;
    struct sockaddr_in src;
    vector<Contact>::iterator it;

    ret = recv_msg(this->fd, msg, &src);

    if (ret <= 0) {
        return false;
    }

    switch(msg->type) {
    case JOIN:
        if (!this->is_contact) {
            cerr << "Shouldn't receive JOIN" << endl;
            break;
        }

        for (it = contacts.begin(); it != contacts.end(); it++) {
            if (sockaddr_cmp(&src, it->get_sockaddr())) {
                return true;
            }
        }

        struct timespec* join_time;
        char tmp[100];

        join_time = (struct timespec*)msg->payload;
        sprintf(tmp, "Recv JOIN msg at %ld", join_time->tv_sec * 1000000 + join_time->tv_nsec / 1000);
        log(tmp, &src);

        contacts.push_back(Contact(src, *join_time));
        this->send(MEMBER_LIST);
        this->send(NEW_MEMBER);
        break;

    case LEAVE:
        for (it = contacts.begin(); it != contacts.end(); it++) {
            if (sockaddr_cmp(it->get_sockaddr(), &src)) {
                contacts.erase(it);
                break;
            }
        }

        log("Recv LEAVE msg", &src);
        break;

    case HEARTBEAT:
        for (it = contacts.begin(); it != contacts.end(); it++) {
            if (sockaddr_cmp(it->get_sockaddr(), &src)) {
                it->reset_penalty();
                break;
            }
        }
        log("Recv HEARTBEAT msg", &src);
        break;

    case NEW_MEMBER:
        if (this->is_contact) {
            cerr << "Contact shouldn't recv NEW_MEMBER" << endl;
            break;
        }

        contacts.push_back(Contact(*((struct sockaddr_in*)msg->payload), zero));
        log("Recv NEW MEMBER msg", &src);
        break;

    case MEMBER_LIST:
        if (this->is_contact) {
            cerr << "Contact shouldn't recv MEMBER_LIST" << endl;
            break;
        }

        struct sockaddr_in s;
        for(uint32_t i = 0; i < (msg->len)/sizeof(struct sockaddr_in); i++) {
            s = ((struct sockaddr_in*)msg->payload)[i];
            contacts.push_back(Contact(s, zero));
        }

        log("Recv MEMBER LIST msg", &src);
        break;

    default:
        cerr << "Invalid type: " << msg->type << endl;
    }

    return true;
}

/* Check the status of all the contacts */
void Node::check_heartbeat()
{
    vector<Contact>::iterator it;
    for (it = contacts.begin(); it != contacts.end(); it++) {
        it->inc_penalty();
        if (it->get_penalty() == MAX_PENALTY) {
            log("No heartbeat. Removing.", it->get_sockaddr());

            contacts.erase(it);
            if (contacts.empty()) {
                break;
            }
            it--;
        }
    }
}

/* Destructor */
Node::~Node()
{
    close(fd);
    contacts.clear();
    fp.close();
}
