#include <utility>
#include <iostream>
#include <fstream>
#include <string>
#include <errno.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#include "constants.h"
#include "network.h"
#include "node.h"

using namespace std;

Node node;

string ip;
int port;
string num;

/* Parse command line arguments */
void parse_args(int argc, char** argv)
{
    int opt;
    pair<string, int> ret;

    while ((opt = getopt(argc, argv, "h:p:n:")) != -1) {
        switch (opt) {
        case 'h':
            ip = optarg;
            break;
        case 'p':
            port = atoi(optarg);
            break;
        case 'n':
            num = optarg;
            break;
        default:
            cout << "Usage: " << argv[0] << " [-h server address] [-p port] [-n number]." << endl;
        }
    }
}

/* Setting up the initial connection */
void setup()
{
    ifstream fp;
    string str, contact_ip;
    int index;
    int contact_port;

    fp.open(CONF_NAME);
    if(!fp.is_open()) {
        cerr << "! Open: " << errno << endl;
        exit(1);
    }

    getline(fp, str);
    index = str.find(CONF_DELIMITER);
    contact_ip = str.substr(0, index);
    contact_port = atoi(str.substr(index + 1, str.length()).c_str());

    if (contact_ip == ip && contact_port == port) {
        node.bootstrap(contact_ip, contact_port, true, contact_ip, contact_port, num);
    } else {
        node.bootstrap(ip, port, false, contact_ip, contact_port, num);
    }

    cout << "Done setting up" << endl;
}

/* Send heartbeat message to all of its contacts */
void* send_heartbeat(void* ptr)
{
    while(1) {
        sleep(HB_INTERVAL);

        if (node.get_is_connected()) {
            node.send(HEARTBEAT);
        }
    }

    return NULL;
}

/* Detect failed nodes from the absence of heartbeat msg */
void* check_heartbeat(void* ptr)
{
    while(1) {
        sleep(HB_INTERVAL);

        if (node.get_is_connected()) {
            node.check_heartbeat();
        }
    }

    return NULL;
}

void _main()
{
    pthread_t th1, th2;
    fd_set fds;
    string cmd;
    int cfd;

    pthread_create(&th1, NULL, send_heartbeat, NULL);
    pthread_create(&th2, NULL, check_heartbeat, NULL);

    cout << "cmd> ";
    fflush(stdout);

    /* Keep an eye on the stdin (in case we receive user input)
     * the socket descriptor (in case we receive any data). */
    while(1) {
        cfd = node.get_fd();

        FD_ZERO(&fds);
        FD_SET(0, &fds);
        FD_SET(cfd, &fds);

        if(select(max(0, cfd) + 1, &fds, NULL, NULL, NULL) < 0) {
            continue;
        }

        if(FD_ISSET(0, &fds)) {
            cin >> cmd;
            node.parse_cmd(cmd.c_str());

            cout << "cmd> ";
            fflush(stdout);
        }

        if (FD_ISSET(cfd, &fds)) {
            node.recv();
        }
    }
}

int main(int argc, char** argv)
{
    parse_args(argc, argv);
    setup();
    _main();
}
