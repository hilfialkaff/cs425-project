#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <string>
#include <cstring>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <getopt.h>

#include "client.h"
#include "constants.h"

using namespace std;

void parse_args(int argc, char** argv)
{
    int opt;
    int opt_ind;
    static struct option options[] = {
            {"logname", required_argument, 0,  0 },
            {0,         0,                 0,  0 }
    };

    while ((opt = getopt_long(argc, argv, "", options, &opt_ind)) != -1) {
        switch(opt) {
            case 0:
                if (optarg) {
                    strcpy(log_name, optarg);
                }
                break;
            default:
                printf("Usage: %s --logname log\n", argv[0]);
        }
    }
}

void setup()
{
    int i = 0;
    int ret;
    int index;
    ifstream fp;
    string str;

    fp.open(SERVER_LIST);
    if(!fp.is_open()) {
        cerr << "! Open: " << errno << endl;
        exit(1);
    }

    while(getline(fp, str)) {
        index = str.find(CONF_DELIMITER);
        strcpy(server_addr[i], str.substr(0, index).c_str());
        port[i] = atoi(str.substr(index + 1, str.length()).c_str());

        i++;
    }

    fp.close();

    for (i = 0;i < MAX_SERVERS;i++) {
        servers[i].is_present = 0;
        servers[i].has_replied = 0;
        servers[i].fd = socket(AF_INET, SOCK_STREAM, 0);

        if (servers[i].fd < 0) {
            perror("! Socket");
        }

        memset(&servers[i].sockaddr, '\0', sizeof(struct sockaddr_in));

        servers[i].sockaddr.sin_family = AF_INET;
        servers[i].sockaddr.sin_port = htons(port[i]);
        inet_aton(server_addr[i], &(servers[i].sockaddr.sin_addr));

        ret = connect(servers[i].fd, (struct sockaddr *)&servers[i].sockaddr, sizeof(struct sockaddr_in));
        if (!ret) {
            servers[i].is_present = 1;
            debug_printf("%d Connected to addr: %s, port: %d\n", i, server_addr[i], port[i]);
        }
    }

    debug_printf("Finish setting up...\n");
}

void _main()
{
    fd_set fds;
    int i,ret;
    char buf[MAX_MSG];
    FILE *fp = NULL;

    printf("grep> ");
    fflush(stdout);

    while(1) {
        FD_ZERO(&fds);
        FD_SET(0, &fds);

        iter_servers_fd(i, servers) { 
            FD_SET(servers[i].fd, &fds);
        }

        select(max_fd(servers) + 1, &fds, NULL, NULL, NULL);

        /* Send msg to all servers when recv input from user */
        if(FD_ISSET(0, &fds)) {
            memset(buf, '\0', MAX_MSG);
            scanf(" %[^\n]", buf);

            if (strlen(buf)) {
                if (fp) {
                    fclose(fp);
                }

                fp = fopen(log_name, "w");

                debug_printf("Command: %s\n", buf);
	            fflush(fp);

	            /* Try to reconnect if some servers are down from the last computation */
	            for (i = 0; i < MAX_SERVERS; i++) {
		            if (!servers[i].is_present) {
  	                    servers[i].fd = socket(AF_INET, SOCK_STREAM, 0);

  	                    if (servers[i].fd < 0) {
  	                        perror("! Socket");
  	                    }

  	                    ret = connect(servers[i].fd, (struct sockaddr *)&servers[i].sockaddr, sizeof(struct sockaddr_in));
		                if (!ret) {
  	                            servers[i].is_present = 1;
  	                            debug_printf("%d Reconnected to addr: %s, port: %d\n", i, server_addr[i], port[i]);
  	                    }
  	                }
                }

                /* Send the command to each of the servers that is running */
                iter_servers_fd(i, servers) {
                    debug_printf("Sending to fd: %d\n", servers[i].fd);
                    send(servers[i].fd, buf, strlen(buf), 0);
                }

                printf("\ngrep> ");
	            fflush(stdout);
	        }
        }

        /* Check if receiving any reply from server */
        iter_servers_fd(i, servers) {
            if(FD_ISSET(servers[i].fd, &fds)) {
                memset(buf, '\0', sizeof(char) * MAX_MSG);
                int status = recv(servers[i].fd, buf, MAX_MSG, 0);

                /* Server is down */
                if (status <= 0) {
	        	    debug_printf("Server %d is not present\n", i);
	        		servers[i].is_present = 0;
	        		close(servers[i].fd);

			        continue;
		        }
		        else {
		            debug_printf("Server %d has replied\n", i);

			        fprintf(fp, "\nMachine.%d.log:\n", (i + 1));
			        fprintf(fp, "%s", buf);
			        fflush(fp);
		        }
            }
        }
    }
}

int main(int argc, char** argv)
{
    parse_args(argc, argv);
    setup();
    _main();
}
