#ifndef SERVER_H
#define SERVER_H

#include "constants.h"

/* Store each of the clients state */
typedef struct client_state {
    int is_present;
    int fd;
} client_state_t;

#define iter_clients_fd(i, clients) \
    for(i = 0;i < MAX_CLIENTS;i++) \
        if (clients[i].is_present)

int server_fd = 0;
int client_fd = 0;

char addr[100];
char server_num[10];
int port;
client_state_t clients[MAX_CLIENTS];

/* Find the max fd of the clients for select() use */
inline int max_fd(client_state_t* clients)
{
    int i;
    int max = 0;
    for (i = 0;i < MAX_CLIENTS;i++) {
        max = (max > clients[i].fd) ? max : clients[i].fd;
    }

    return max;
}

/* Parse command line arguments */
void parse_args(int argc, char** argv);

/* Setup sockets */
void setup();

/* Clean up all the opened sockets */
void cleanup();

/* Main loop */
void _main();

/* Handle a client's message */
void handle_client(int client_num);

#endif /* SERVER_H */
