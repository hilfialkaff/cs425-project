#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "server.h"
#include "constants.h"

/* Parse command line arguments */
void parse_args(int argc, char** argv)
{
    int opt;
    while ((opt = getopt(argc, argv, "h:p:n:")) != -1) {
        switch (opt) {
            case 'h':
                strcpy(addr, optarg);
                break;
            case 'p':
                port = atoi(optarg);
                break;
            case 'n':
                strcpy(server_num, optarg);
                break;
            default:
                printf("Usage: %s [-h server address] [-p port] [-n server num].\n", argv[0]);
        }
    }
}

/* Setup sockets */
void setup()
{
    int err;
    struct sockaddr_in server;

    /* Create TCP non-blocking socket */
    server_fd = socket(AF_INET, SOCK_STREAM | O_NONBLOCK, 0);
    if(server_fd < 0) {
        perror("! Socket");
    }

    memset(&server, '\0', sizeof(struct sockaddr_in));
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    inet_aton(addr, &server.sin_addr);

    /* Bind server on the appropriate address */
    err = bind(server_fd, (struct sockaddr*)&server, sizeof(server));
    if (err) {
        perror("! Bind");
    }

    /* Start listening for clients */
    listen(server_fd, MAX_CLIENTS);

    debug_printf("Listening...\n");

    memset(clients, 0, sizeof(client_state_t) * MAX_CLIENTS);
}

/* Clean up sockets */
void cleanup()
{
    int i;
    iter_clients_fd(i, clients) {
        close(clients[i].fd);
    }
    close(server_fd);
}

/* Handle the client's message */
void handle_client(int fd)
{
    char buf[MAX_MSG];
    char cmd[MAX_MSG];
    char tmp[MAX_MSG];
    int ret;
    int i;
    FILE* fp;

    memset(buf, '\0', sizeof(char) * MAX_MSG);
    memset(cmd, '\0', sizeof(char) * MAX_MSG);
    memset(tmp, '\0', sizeof(char) * MAX_MSG);

    ret = recv(fd, buf, MAX_MSG, 0);

    /* Client is disconnected */
    if (!ret) {
        for (i = 0; i < MAX_CLIENTS; i++) {
            if (clients[i].fd == fd) {
                clients[i].is_present = 0;
                close(fd);
                return;
            }
        }
    }

    debug_printf("Received: %s\n", buf);

    strcpy(cmd, "grep ");
    strcat(cmd, buf);
    strcat(cmd, " machine.");
    strcat(cmd, server_num);
    strcat(cmd, ".log");

    debug_printf("Executing cmd: %s\n", cmd);

    fp = popen(cmd, "r");
    if (!fp) {
        perror("! Popen");
    }

    memset(buf, '\0', sizeof(char) * MAX_MSG);
    while (fgets(tmp, MAX_MSG, fp)) {
        strcat(buf, tmp);
    }

    debug_printf("Result: %s\n", buf);

    send(fd, buf, strlen(buf), 0);

    if (pclose(fp)) {
        perror("! Pclose");
    }
}

/* Main loop */
void _main()
{
    fd_set fds;
    int ret;
    int i;

    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 0;

    while(1) {

        /* Register new connection if exist */
        ret = accept(server_fd, NULL, NULL);
        if (ret > 0) {
            for (i = 0; i < MAX_CLIENTS; i++) {
                if (!clients[i].is_present) {
                    clients[i].is_present = 1;
                    clients[i].fd = ret;

                    debug_printf("Recv connection: %d\n", clients[i].fd);
                    break;
                }
            }
        }

        FD_ZERO(&fds);
        iter_clients_fd(i, clients) {
            FD_SET(clients[i].fd, &fds);
        }

        select(max_fd(clients) + 1, &fds, NULL, NULL, &timeout);

        /* Find out which client has sent a command and handles them */
        iter_clients_fd(i, clients) {
            if(FD_ISSET(clients[i].fd, &fds)) {
                handle_client(clients[i].fd);
            }
        }
    }

    cleanup();
}

int main(int argc, char** argv)
{
    parse_args(argc, argv);
    setup();
    _main();
}
