#!/bin/bash

# Starting Server w/ Logging
if [ $# -ne 3 ]
then
	echo "Usage: sh run.sh <server-num> <log-and-test> <output-file-name>"
	echo "<server-num> can vary from 1 to 4"
	echo "<log-and-test> is 1 if you want to log and test"
	echo "<output-file-name> output file name"
	exit
fi

function run_test {
    if [ $2 -eq 2 -o $2 -eq 1 ]
    then
        rm -f machine.$1.log
        
        echo "MACHINE$1" > machine.$1.log

        for (( i = 1; i <= $3; i++ ))
        do
            echo "syntax error" >> machine.$1.log
        done
        
        ruby -e 'a=STDIN.readlines;10000.times do;b=[];10.times do; b << a[rand(a.size)].chomp end; puts b.join(" "); end' < /usr/share/dict/words >> machine.$1.log
    fi
}

NUM_MACHINES=4
NUM_LOOP=100
SIZE=10000

run_test $1 $2 $NUM_LOOP $SIZE

# Run each server with static IPs
if [ $1 -eq 1 ]
then
	xterm -e "./server -p 6950 -h 192.17.11.30 -n 1" &
elif [ $1 -eq 2 ]
then
	xterm -e "./server -p 6950 -h 192.17.11.33 -n 2" &
elif [ $1 -eq 3 ]
then
	xterm -e "./server -p 6950 -h 192.17.11.34 -n 3" &
elif [ $1 -eq 4 ]
then
	xterm -e "./server -p 6950 -h 192.17.11.35 -n 4" &
else
	echo "Usage: sh run.sh <server-num> <log-and-test> <output-file-name>"
	echo "<server-num> can vary from 1 to 4"
	echo "<log-and-test> is 1 if you want to log and test"
	echo "<output-file-name> output file name"
	exit
fi

sleep 5

#Testing Phase

if [ $2 -eq 2 ]
then
    rm -f tmp.pipe
    mkfifo tmp.pipe

    nohup ./client --server1 192.17.11.30 --port1 6950 --server2 192.17.11.33 --port2 6950 --server3 192.17.11.34 --port3 6950 --server4 192.17.11.35 --port4 6950 --logname $3 < tmp.pipe > /dev/null 2> /dev/null &
    sleep 2

    echo "Searching for frequent word: syntax error"
    echo "\"syntax error\"" > tmp.pipe
    sleep 2

    log_name="Logs/"$3
    num=`grep syntax $log_name | wc -l`

    result=`expr $NUM_MACHINES \* $NUM_LOOP`
    echo "num: $num, result: $result"

    if [ $num -eq $result ]
    then
        echo "Found $num results: SUCCESS"
    else
        echo "YOU FAILED"
    fi

    echo "Searching for infrequent word: MACHINE1"
    echo "MACHINE1" > tmp.pipe
    sleep 2

    log_name="Logs/"$3
    num=`grep MACHINE1 $log_name | wc -l`

    if [ $num -eq 1 ]
    then
        echo "Found $num results: SUCCESS"
    else
        echo "YOU FAILED"
    fi

    # Kill the client
    ps uax | grep "./client" | awk '{print $2}' | xargs kill
else
    ./client --server1 192.17.11.30 --port1 6950 --server2 192.17.11.33 --port2 6950 --server3 192.17.11.34 --port3 6950 --server4 192.17.11.35 --port4 6950 --logname $3
fi
