#ifndef CLIENT_H
#define CLIENT_H

#include "constants.h"

/* Store each of the servers state */
typedef struct server_state {
    int is_present;
    int has_replied;
    int fd;
    struct sockaddr_in sockaddr;
} server_state_t;

#define iter_servers_fd(i, servers) \
    for(i = 0;i < MAX_SERVERS;i++) \
        if (servers[i].is_present)

char log_name[100];
char benchmark_log[100];
int port[MAX_SERVERS];
char server_addr[MAX_SERVERS][100];
server_state_t servers[MAX_SERVERS];

/* Find the max fd of the servers for select() use */
inline int max_fd(server_state_t* servers)
{
    int i;
    int max = 0;
    for (i = 0;i < MAX_SERVERS;i++) {
        max = (max > servers[i].fd) ? max : servers[i].fd;
    }

    return max;
}

/* Check whether all the servers have replied */
inline int all_servers_replied()
{
    int i;
    int ret = 1;

    for (i = 0; i < MAX_SERVERS; i++) {
        if (servers[i].is_present && !servers[i].has_replied) {
            ret = 0;
        }
    }

    return ret;
}

#endif /* CLIENT_H */
