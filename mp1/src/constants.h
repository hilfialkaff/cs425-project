#ifndef CONSTANTS_H
#define CONSTANTS_H

#define MAX_CLIENTS 4
#define MAX_SERVERS 4
#define MAX_MSG 1024 * 1024

#define SERVER_LIST "server_list.conf" /* List of server ip/port */
#define CONF_DELIMITER ':' /* Delimiter in the server_list file */

#ifdef DEBUG
#define debug_printf(...) printf(__VA_ARGS__)
#else
#define debug_printf(...)
#endif /* DEBUG */

#endif /* CONSTANTS_H */
