import sys
import random
import string

def main():
    sz = sys.argv[1]
    w = random.choice(string.letters)

    f = open(sz + "MB", 'w')
    for i in range(int(sz)):
        l = ''.join(w for _ in xrange((10**6)))
        f.write(l)
    f.close()

if __name__ == '__main__':
    main()
