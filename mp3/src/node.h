#ifndef CLIENT_H
#define CLIENT_H

#include <netinet/in.h>
#include <string>
#include <cstring>
#include <errno.h>
#include <iostream>
#include <vector>
#include <time.h>
#include <fcntl.h>

#include "constants.h"
#include "network.h"
#include "contact.h"
#include "utils.h"
#include "log.h"

using namespace std;

typedef struct
{
    string sdfsfilename;
    string localfilename;
}FileEntry;

/**
 * Storing the state of the current program
 */
class Node
{
public:

    /* Constructor */
    Node();

    /* Populate the node's information */
    void bootstrap(string ip, int port, string num);

    /* Socket setup procedures and try to join the system (if it is not the contact point) */
    void setup();

    /* Clean up the resources associated with the connections */
    void cleanup();

    /* Send a msg corresponding to type */
    void send(int type);
    void send_fileop(int type, string sdfsfilename=string(), string localfilename=string());

    /* Receive and handle the msg */
    bool recv();

    /* Check the status of all the contacts */
    void check_heartbeat();

    unsigned int hash(const char* s, bool isnode);

    void getfile(string sdfs, string local);

    void putfile(string sdfs, string local);

    void delfile(string sdfs);

    bool fetchfile(int fd, string sdfs, string local);

    bool receivefile(int fd, string local);

    bool pushfile(int fd, string sdfs);

    bool removefile(int fd, string sdfs);

    bool verify_delete(int fd);

    bool fileexists(string sdfs, int* fd);

    void recv_replicas(int fd);

    void recv_files(int fd);

    bool insert_cfd(int fd)
    {
        cfds.push_back(fd);
    }

    void set_cfds(fd_set* fds)
    {
        vector<int>::iterator it;
        for (it = cfds.begin(); it!= cfds.end(); it++) {
            FD_SET((*it), fds);
        }
    }

    void check_cfds(fd_set* fds)
    {
        vector<int>::iterator it;
        int fd;
        int ret;

        for (it = cfds.begin(); it!= cfds.end(); it++) {
            fd = (*it);
            if (FD_ISSET(fd, fds)) {
                if (!handle_client(fd)) {
                    break;
                }
            }
        }
    }

    void remove_cfd(int cfd)
    {
        vector<int>::iterator it;
        for (it = cfds.begin(); it != cfds.end(); it++) {
            if (cfd == (*it)) {
                cfds.erase(it);
                break;
            }
        }
    }

    bool recv_op(int fd, msg_t* msg)
    {
        bool ret = recv_tcp(fd, msg);
        if (!ret) {
            remove_cfd(fd);
        }
        return ret;
    }

    int getclockwisehash()
    {
        return clkwise_node.first;
    }

    int getanticlockwisehash()
    {
        return aclkwise_node.first;
    }

    int handle_client(int cfd);

    bool copyfile(int fd, string sdfs, string local);

    bool copyreplica(int fd, string sdfs, string local);

    bool copylocally(string sdfs, string local);

    int get_fd()
    {
        return this->fd;
    }

    int get_datafd()
    {
        return this->datafd;
    }

    bool get_is_contact()
    {
        return this->is_contact;
    }

    void set_is_contact(bool is_contact)
    {
        this->is_contact = is_contact;
    }

    pair<unsigned int, Contact> get_clkwise_node()
    {
        return clkwise_node;
    }

    pair<unsigned int, Contact> get_aclkwise_node()
    {
        return aclkwise_node;
    }

    struct sockaddr_in* get_sockaddr()
    {
        return &(this->sockaddr);
    }

    struct sockaddr_in* get_contact_sockaddr()
    {
        return &(this->contact_sockaddr);
    }

    bool get_is_connected()
    {
        return this->is_connected;
    }

    map<unsigned int, Contact> get_contacts()
    {
        return this->contacts;
    }

    void setup_contact()
    {
        is_contact = true;
        contact_sockaddr = sockaddr;

        fstream fp;
        fp.open(CONF_NAME, fstream::out);
        fp << tostring(sockaddr);
        fp.close();
    }

    /* Find logical clockwise contact */
    pair<unsigned int, Contact> find_clkwise_contact()
    {
        map<unsigned int, Contact>::iterator it = contacts.find(hashid);

        if (it == contacts.end())
            cerr << __func__ << "Hash of the node does not exist" << endl;
        else
            it++;

        if (it == contacts.end())
            it = contacts.begin();

#ifdef DEBUG
        cout << " hashid: " << hashid << " clkwise: " << ntohs((*it).second.get_sockaddr()->sin_port) << endl;
#endif

        return *it;
    }

    /* Find logical anticlockwise contact */
    pair<unsigned int, Contact> find_aclkwise_contact()
    {
        map<unsigned int, Contact>::iterator it = contacts.find(hashid);
        pair<unsigned int, Contact> anti = *it;
        if (it == contacts.end())
            cerr << __func__ << "Hash of the node does not exist" << endl;
        else if (it == contacts.begin())
            anti = *(contacts.rbegin());
        else
        {
            it--;
            anti = *it;
        }

#ifdef DEBUG
        cout << " hashid: " << hashid << " aclkwise: " << ntohs((anti).second.get_sockaddr()->sin_port) << endl;
#endif

        return anti;
    }

    void print_contacts_list()
    {
        map<unsigned int, Contact>::iterator it;

        cout << "PRINT CONTACTS" << endl;
        for (it = contacts.begin(); it != contacts.end(); it++) {
            Contact c = (*it).second;
            cout << "sockaddr: " << tostring(*(c.get_sockaddr())) << " fd: " << c.get_cfd() << endl;
        }
    }


    /* Destructor */
    ~Node();

private:

    bool update_contacts()
    {
        aclkwise_node = find_aclkwise_contact();
        clkwise_node = find_clkwise_contact();
    }

    bool update_contacts_onadd();
    bool update_contacts_onrem();

    bool is_contact; /* Is this the contact point? */
    bool is_connected; /* Is this node alive? */
    int fd; /* File descriptor to listen on */
    int datafd;
    struct sockaddr_in sockaddr; /* Port/IP */
    struct sockaddr_in contact_sockaddr; /* Port/IP of contact contact */
    map<unsigned int, Contact> contacts; /* <key, connected contacts> */

    unsigned int hashid; /* Node Hash ID */
    vector<FileEntry> clockwisefilelist; /* Files with hash >= hashid and < clockwise node's hashid */
    vector<FileEntry> anticlockwisereplicafilelist; /* File replicas with hash < hashid and >= anticlockwise node's hashid*/
    vector<FileEntry> cachedfilelist;
    pair<unsigned int, Contact> clkwise_node;
    pair<unsigned int, Contact> aclkwise_node;

    vector<int> cfds;

    string machine_num;

    /* For election purposes */
    bool is_candidate;
    bool is_leader;
};

#endif /* CLIENT_H */
