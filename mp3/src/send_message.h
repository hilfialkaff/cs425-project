#include <time.h>

#include "log.h"
#include "constants.h"
#include "node.h"
#include "contact.h"

void send_join(Node* node);

void send_leave(Node* node);

void send_heartbeat(Node* node);

void send_new_member(Node* node, void* arg);

void send_member_list(Node* node, struct sockaddr_in* s);

void send_contact_rejoin(Node* node);

void send_member_die(Node* node, struct sockaddr_in* s);

void send_election_req(Node* node, void* msg);

void send_new_leader(Node* node, void* msg);
