#ifndef CONSTANTS_H
#define CONSTANTS_H

/* Max msg length to be transmitted */ 
#define MAX_MSG 2048

/* File which stores the information about the contact point */
#define CONF_NAME "contact_point.conf"

/* Delimiter in conf file */
#define CONF_DELIMITER ':'

/* Base directory for log files */
#define BASE_DIR "logs/"

/* The interval in which heartbeat message is sent and checked */
#define HB_INTERVAL 4

/* get command */
#define GET_CMD "get"

/* put command*/
#define PUT_CMD "put"

/*The delete command*/ 
#define DEL_CMD "delete"

/* Chord number of bit*/
#define NUM_BIT 3

/* Max number of clients in the sdfs */
#define MAX_CLIENTS 128

const struct timespec zero = {0, 0};

#endif /* CONSTANTS_H */
