#include <utility>
#include <iostream>
#include <fstream>
#include <string>
#include <assert.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#include "constants.h"
#include "network.h"
#include "node.h"
#include "parse_cmd.h"

using namespace std;

Node node;

string ip;
int port;
string num;

/* Parse command line arguments */
void parse_args(int argc, char** argv)
{
    int opt;
    pair<string, int> ret;

    while ((opt = getopt(argc, argv, "h:p:n:")) != -1) {
        switch (opt) {
        case 'h':
            ip = optarg;
            break;
        case 'p':
            port = atoi(optarg);
            break;
        case 'n':
            num = optarg;
            break;
        default:
            cout << "Usage: " << argv[0] << " [-h server address] [-p port] [-n number]." << endl;
        }
    }
}

/* Setting up the initial connection */
void setup()
{
    node.bootstrap(ip, port, num);
    cout << "Done setting up" << endl;
}

/* Send heartbeat message to all of its contacts */
void* send_heartbeat(void* ptr)
{
    while(1) {
        sleep(HB_INTERVAL);

        if (node.get_is_connected()) {
            node.send(HEARTBEAT);
        }
    }

    return NULL;
}

/* Detect failed nodes from the absence of heartbeat msg */
void* check_heartbeat(void* ptr)
{
    while(1) {
        sleep(HB_INTERVAL);

        if (node.get_is_connected()) {
            node.check_heartbeat();
        }
    }

    return NULL;
}

void check_new_conn(int data_fd)
{
    struct sockaddr_in s;
    size_t size = sizeof(struct sockaddr_in);
    int new_fd;

    new_fd = accept(data_fd, (struct sockaddr*)&s, (socklen_t*)&size);
    if (new_fd > 0) {
        cout << __func__ << "accept: " << tostring(s) << endl;
    
        node.insert_cfd(new_fd);
    }
}

void _main()
{
    pthread_t th1, th2;
    fd_set fds;
    string cmd;
    int cfd;
    int data_fd;
    int i;

    pthread_create(&th1, NULL, send_heartbeat, NULL);
    pthread_create(&th2, NULL, check_heartbeat, NULL);

    cout << "cmd> ";
    fflush(stdout);

    /* Keep an eye on the stdin (in case we receive user input)
     * the socket descriptor (in case we receive any data). */
    while(1) {
        data_fd = node.get_datafd();
        cfd = node.get_fd();

        check_new_conn(data_fd);

        FD_ZERO(&fds);
        FD_SET(0, &fds);

        if (node.get_is_connected()) {
            FD_SET(cfd, &fds);
            node.set_cfds(&fds);
        }

        if(select(FD_SETSIZE, &fds, NULL, NULL, NULL) < 0) {
            continue;
        }

        if(FD_ISSET(0, &fds)) {
            getline(cin,cmd);		
            parse_cmd(node, cmd.c_str());

            cout << "cmd> ";
            fflush(stdout);
        }

        if (FD_ISSET(cfd, &fds)) {
            node.recv();
        }

        node.check_cfds(&fds);
    }
}

int main(int argc, char** argv)
{
    parse_args(argc, argv);
    setup();
    _main();
}
