#ifndef LOG_H
#define LOG_H

#include <fstream>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "constants.h"

using namespace std;

class Log {

public:
    Log();

    ~Log();

    void prep_file(string num);

    void begin_benchmark();

    void end_benchmark(string msg);

    void log(string msg, struct sockaddr_in* addr);

private:
    fstream fp;
    struct timespec beg, end;
};

extern Log logger;

#endif /* LOG_H */
