#include "parse_cmd.h"

void parse_join_cmd(Node& node)
{
    if (node.get_is_connected()) {
        cerr << "Already connected" << endl;
        return;
    }
    
    node.setup();
    if (node.get_is_contact()) {
        node.send(CONTACT_REJOIN);
    }
}

void parse_leave_cmd(Node& node)
{
    if (!node.get_is_connected()) {
        cerr << "Not yet connected" << endl;
        return;
    }

    node.send(LEAVE);
    node.set_is_contact(false);
    node.cleanup();
}

void parse_crash_cmd(Node& node)
{
    if (!node.get_is_connected()) {
        cerr << "Not yet connected" << endl;
        return;
    }
    node.cleanup();
}

void parse_get_cmd(Node& node, const char *cmd)
{
    logger.begin_benchmark();
    if (!node.get_is_connected()) {
        cerr << "Not yet connected" << endl;
        return;
    }

    string command(cmd);
    size_t found = command.find_last_of(' ');            
    node.send_fileop(GET, command.substr(4,found - 4), command.substr(found + 1));
    logger.end_benchmark("GETFILE");
}

void parse_put_cmd(Node& node, const char* cmd)
{
    logger.begin_benchmark();
    if (!node.get_is_connected()) {
        cerr << "Not yet connected" << endl;
        return;
    }

    string command(cmd);
    size_t found = command.find_last_of(' ');            
    node.send_fileop(PUT, command.substr(found + 1), command.substr(4,found - 4));
    logger.end_benchmark("PUTFILE");
}

void parse_del_cmd(Node& node, const char* cmd)
{
    if (!node.get_is_connected()) {
        cerr << "Not yet connected" << endl;
        return;
    }

    string command(cmd);
    size_t found = command.find_last_of(' ');            
    node.send_fileop(DEL, command.substr(found + 1));
}

void parse_cmd(Node& node, const char* cmd)
{
    if (!strcmp(cmd, "join")) {
        parse_join_cmd(node);
    } else if (!strcmp(cmd, "leave")) {
        parse_leave_cmd(node);
    } else if (!strcmp(cmd, "crash")) {
        parse_crash_cmd(node);
    } else if (!strncmp(cmd, GET_CMD, 3)) {
        parse_get_cmd(node, cmd);
    } else if (!strncmp(cmd, PUT_CMD, 3)) {
        parse_put_cmd(node, cmd);
    } else if (!strncmp(cmd, DEL_CMD, 6)) {
        parse_del_cmd(node, cmd);
    }
}
