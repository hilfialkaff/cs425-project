#ifndef UTILS_H
#define UTILS_H

#include "network.h"
#include "constants.h"

#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

inline string get_real_fname(string num, string fname)
{
    return "machines/" + num + "/" + fname;
}

inline string tostring(struct sockaddr_in s)
{
    char id[100];

    sprintf(id, "%s:%d", inet_ntoa(s.sin_addr), ntohs(s.sin_port));
    return string(id);  
}

/* Print ip & port of sockaddr_in */
inline void print_ip_port(struct sockaddr_in s)
{
#ifdef DEBUG
    cout << "ip: " << inet_ntoa(s.sin_addr) << " port: " << ntohs(s.sin_port) << endl;
#endif /* DEBUG */
}

/* Return true if port and ip equal */
inline bool sockaddr_cmp(struct sockaddr_in* s1, struct sockaddr_in* s2)
{
    return (s1->sin_port == s2->sin_port) &&
        !strcmp(inet_ntoa(s1->sin_addr), inet_ntoa(s2->sin_addr));
}

/* Setup the sockaddr struct */
inline void setup_sockaddr(struct sockaddr_in* sockaddr, int port, string ip)
{
    memset(sockaddr, 0, sizeof(sockaddr));

    sockaddr->sin_family = AF_INET;
    sockaddr->sin_port = htons(port);
    inet_aton(ip.c_str(), &sockaddr->sin_addr);
}

inline pair<int, string> read_contact_info()
{
    ifstream fp;
    string ip;
    int port;
    string str;
    int index;

    fp.open(CONF_NAME);
    if(!fp.is_open()) {
        cerr << "! Open: " << errno << endl;
        exit(1);
    }

    getline(fp, str);
    index = str.find(CONF_DELIMITER);
    port = atoi(str.substr(index + 1, str.length()).c_str());
    ip = str.substr(0, index);

    return pair<int, string>(port, ip);
}

#endif /* UTILS_H */
