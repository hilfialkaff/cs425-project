#include "network.h"

/* Send a message via UDP */
void send_msg(int type, int fd, struct sockaddr_in* dest_addr, void* payload, int len)
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*) bytes;
    int ret;
    int size;

    msg->type = type;

    /* For marshalling */
    if (len % 2) {
        msg->len = len + 1;
        if (msg->len - 1) {
            memcpy(msg->payload, payload, msg->len);
        }
    } else {
        msg->len = len;
        if (msg->len) {
            memcpy(msg->payload, payload, msg->len);
        }
    }

    size = msg_size(msg);

#if 0
// #ifdef DEBUG
    printf("Send %s:%d: ",inet_ntoa(dest_addr->sin_addr), ntohs(dest_addr->sin_port));
    for(int i = 0; i < msg_size(msg); i++) {
        printf("0x%x ", ((char*)msg)[i]);
    }
    printf("\n");
#endif /* DEBUG */

    marshall(msg);
    ret = sendto(fd, (char*)msg, size, 0, (struct sockaddr*)dest_addr, sizeof(struct sockaddr));

    if (ret == -1) {
        cerr << __func__ << " fd: " << fd << " errno: " << errno << endl;
    }
}

void send_tcp(int type, int fd, void* payload, int len)
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*) bytes;
    int ret;
    int size;

    msg->type = type;

    /* For marshalling */
    if (len % 2) {
        msg->len = len + 1;
        if (msg->len - 1) {
            memcpy(msg->payload, payload, msg->len);
        }
    } else {
        msg->len = len;
        if (msg->len) {
            memcpy(msg->payload, payload, msg->len);
        }
    }

    size = msg_size(msg);

#ifdef DEBUG
    // printf("Send %s:%d: ",inet_ntoa(dest_addr->sin_addr), ntohs(dest_addr->sin_port));
    // for(int i = 0; i < msg_size(msg); i++) {
    //     printf("0x%x ", ((char*)msg)[i]);
    // }
    // printf("\n");
#endif /* DEBUG */

    marshall(msg);
    ret = send(fd, (char*)msg, size, 0);

    if (ret == -1) {
        cerr << __func__ << " fd: " << fd << " errno: " << errno << endl;
    }
}

/* Recv a message via UDP */
int recv_msg(int fd, msg_t* msg, struct sockaddr_in* src)
{
    socklen_t size = sizeof(struct sockaddr_in);
    int ret = recvfrom(fd, (void*)msg, MAX_MSG, 0, (struct sockaddr*)src, &size);

    /* There is message to be read */
    if (ret > 0) { 
        unmarshall(msg);
    }

#if 0
// #ifdef DEBUG
    printf("Recv %s:%d: ",inet_ntoa(src->sin_addr), ntohs(src->sin_port));
    if (ret > 0) {
        for(int i = 0; i < ret; i++) {
            printf("0x%x ", ((char*)msg)[i]);
        }
    }
    printf("\n");
#endif /* DEBUG */

    return ret;
}

bool recv_tcp(int fd, msg_t* msg)
{
    int ret = 0;
	fd_set master;

	FD_ZERO(&master);
	FD_SET(fd, &master);
    
    // cout << "Before select" << endl;
	select(FD_SETSIZE, &master, NULL, NULL, NULL);
	if (FD_ISSET(fd, &master)) {
	    // cout << "Before recv" << endl;
	   	ret = recv(fd, (void*)msg, MAX_MSG, 0);
	}
	// cout << "After select" << endl;

    /* Client is disconnected */
    if (ret <= 0) {
        cout << "Client is disconnected" << endl;
        close(fd);
        return false;
    }

    /* There is message to be read */
    unmarshall(msg);

#if DEBUG
    // printf("Recv %s:%d: ",inet_ntoa(src->sin_addr), ntohs(src->sin_port));
    // if (ret > 0) {
    //     for(int i = 0; i < ret; i++) {
    //         printf("0x%x ", ((char*)msg)[i]);
    //     }
    // }
    // printf("\n");
#endif /* DEBUG */

    return true;
}
