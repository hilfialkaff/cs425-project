#ifndef PARSE_CMD_H
#define PARSE_CMD_H

#include <cstring>
#include <string>

#include "node.h"

using namespace std;

/* Parse command from user input */
void parse_cmd(Node& node, const char* cmd);

#endif /* PARSE_H */
