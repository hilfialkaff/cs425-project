#ifndef CONTACT_H
#define CONTACT_H

#include <netinet/in.h>
#include <time.h>
#include <map>
#include <utility>
#include <cstring>

#include "constants.h"
#include "network.h"

#define MAX_PENALTY 3

/**
 * Keep track of each of the contact this member is connected to
 */
class Contact
{
public:

    /* Constructor */
    Contact();
    Contact(struct sockaddr_in addr, struct timespec time);

    bool operator==(Contact other);

    uint64_t get_ctr()
    {
        return ctr;
    }

    void inc_ctr()
    {
        ctr++;
    }

    int get_penalty()
    {
        return penalty;
    }

    void reset_penalty()
    {
        penalty = 0;
    }

    void inc_penalty()
    {
        penalty++;
    }

    struct timespec get_timestamp()
    {
        return timestamp;
    }

    struct sockaddr_in* get_sockaddr()
    {
        return &sockaddr;
    }

    void set_datafd(int datafd)
    {
        this->datafd = datafd;
    }

    int get_datafd()
    {
        return this->datafd;
    }

    void set_cfd(int cfd)
    {
        this->cfd = cfd;
    }

    int get_cfd()
    {
        return this->cfd;
    }

    void close_cfd()
    {
        close(this->cfd);
    }

    /* Destructor */
    ~Contact();

private:
    uint64_t ctr; /* Heartbeat seq # to be sent to this contact */
    struct sockaddr_in sockaddr; /* Port/IP struct */
    struct timespec timestamp; /* Time of joining */
    int datafd; /* For file operations (as server) */
    int cfd; /* For file operations (as client) */
    int penalty; /* If greater than MAX_PENALTY -> removed from contacts */
};

#endif /* CONTACT_H */
