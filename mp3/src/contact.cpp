#include "contact.h"

/* Constructor */
Contact::Contact() :
    ctr(0), penalty(0), sockaddr({0}), timestamp({0})
{
    datafd = -1;
    cfd = -1;
    return;
}

Contact::Contact(struct sockaddr_in addr, struct timespec time) :
    ctr(0), penalty(0), sockaddr(addr), timestamp(time)
{
    datafd = -1;
    cfd = -1;
    return;
}

/* Destructor */
Contact::~Contact()
{
    return;
}

bool Contact::operator==(Contact other) {
    return sockaddr_cmp(&sockaddr, other.get_sockaddr());
}
