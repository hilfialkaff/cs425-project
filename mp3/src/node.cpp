#include <unistd.h>

#include "node.h"
#include "send_message.h"
#include "math.h"

static int count = 1;


/* Constructor */
Node::Node()
    : clkwise_node(), aclkwise_node(), is_candidate(false)
{
    return;
}

/* Populate the node's information */
void Node::bootstrap(string ip, int port, string num)
{
    pair<int, string> port_ip = read_contact_info();

    if (port_ip.first == port && port_ip.second == ip) {
        is_contact = true;
    }

    setup_sockaddr(&sockaddr, port, ip);

    if (this->is_contact)
    {
        char id[100];
        struct timespec cur;
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cur);

        sprintf(id,"%s:%d", inet_ntoa(sockaddr.sin_addr), sockaddr.sin_port);
        Contact new_member = Contact(sockaddr, cur);
        hashid = hash(id, true);
		char status[100];
		sprintf(status, "Contact Server Hash: %d\n", hashid);
        logger.log(string(status), &sockaddr);

        contacts.insert(pair<unsigned int, Contact>(hashid, new_member));
    }

    this->fd = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0);
    if (fd < 0) {
        cerr << "! Socket: " << errno << endl;
        exit(1);
    }

    this->datafd = socket(AF_INET, SOCK_STREAM | O_NONBLOCK, 0);
    if(datafd < 0) {
        cerr << "! Socket: " << errno << endl;
        exit(1);
    }

    if (bind(fd, (struct sockaddr*)(&(this->sockaddr)), sizeof(struct sockaddr))) {
        cerr << "! 1 Bind: " << errno << "port: " << ntohs(this->sockaddr.sin_port) << endl;
        exit(1);
    }

    if (bind(datafd, (struct sockaddr*)(&(this->sockaddr)), sizeof(struct sockaddr))) {
        cerr << "! 2 Bind: " << errno << "port: " << ntohs(this->sockaddr.sin_port) << endl;
        exit(1);
    }

    /* Start listening for clients */
    listen(datafd, 100);

    machine_num = num;
    logger.prep_file(num);
    this->setup();
}

unsigned int Node::hash(const char* s, bool isnode)
{
	string input(s);
    long h = 0;
    while (*s)
        h += (unsigned char) *s++;
	unsigned int maxNodes = pow(2,NUM_BIT);
    unsigned int proposedid = h % maxNodes;
	char status[100];
	sprintf(status, "Hash Output for %s is %d\n", input.c_str(), proposedid);
	logger.log(string(status), &sockaddr);
    if (isnode)
    {
        bool proposedidnotunique;
        unsigned int originalid = proposedid;
        do
        {
            proposedidnotunique = false;
            for (map<unsigned int, Contact>::iterator it = contacts.begin(); it != contacts.end(); it++)
            {
                if (proposedid == (*it).first)
                    proposedidnotunique = true;
            }

            if (proposedidnotunique)
                proposedid = (proposedid + 1) % maxNodes;
        } while (proposedidnotunique && proposedid != originalid);

        if (proposedidnotunique && proposedid == originalid)
            cerr << "We have hit an upper limit on the number of ids. Should increase NUM_BIT";
    }

    return proposedid;
}

/* Socket setup procedures and try to join the system (if it is not the contact point) */
void Node::setup()
{
    pair<int, string> port_ip = read_contact_info();
    setup_sockaddr(&contact_sockaddr, port_ip.first, port_ip.second);

    if (sockaddr_cmp(&contact_sockaddr, &sockaddr)) {
        is_contact = true;
    }

    if (!is_contact) {
        do {
            this->send(JOIN);
            sleep(1);
        } while (this->recv() <= 0);
    }

    this->is_connected = true;
}

/* Clean up the resources associated with the connections */
void Node::cleanup()
{
    // close(this->fd);
    // close(this->datafd);
    // vector<int>::iterator it;
    // for (it = cfds.begin(); it != cfds.end(); it++) {
    //     close(*it);
    // }

    // this->fd = -1;
    // this->datafd = -1;
    this->is_connected = false;
    //clkwise_node.second.~Contact();
    //aclkwise_node.second.~Contact();

    if (!this->is_contact) {
        contacts.clear();
    }
}

/* Send a msg corresponding to type */
void Node::send(int type)
{
    switch(type) {
    case JOIN:
        send_join(this);
        break;
    case LEAVE:
        send_leave(this);
        break;
    case HEARTBEAT:
        send_heartbeat(this);
        break;
    case NEW_MEMBER:
        // send_new_member(this);
        break;
    case MEMBER_LIST:
        // send_member_list(this);
        break;
    case CONTACT_REJOIN:
        send_contact_rejoin(this);
        break;
    case MEMBER_DIE:
        // send_member_die(this);
        break;
    default:
        cerr << __func__ << "Invalid msg type: " << type << endl;
    }
}

void Node::send_fileop(int type, string sdfs, string local)
{
    switch(type) {
    case GET:
        getfile(sdfs, local);
        break;
    case PUT:
        putfile(sdfs, local);
        break;
    case DEL:
        delfile(sdfs);
        break;
    }
}

//Checks whether file exists which is owned by the node
bool Node::fileexists(string sdfs, int* fd)
{
    unsigned int fileid = hash(sdfs.c_str(),false);
#ifdef DEBUG
    cout << fileid << "\t" << hashid << endl;
#endif
    if ((fileid >= hashid && fileid < clkwise_node.first) || (hashid > clkwise_node.first && (fileid >= hashid || fileid < clkwise_node.first)))
    {
        *fd = -1;
		// File may already be present locally
		for (vector<FileEntry>::iterator it = clockwisefilelist.begin(); it != clockwisefilelist.end(); it++)
		    if (sdfs == it->sdfsfilename)
				return true;

        return false;
    }
    else
    {
#ifdef DEBUG
        cout << __func__ << " File in different node " << endl;
#endif
        //If file is present then it needs to be fetched from the node whose hash id is <= fileid
        map<unsigned int, Contact>::iterator it = contacts.begin();
        if ((*it).first > fileid)
            *fd = contacts.rbegin()->second.get_cfd();
        else
        {
            for (; it != contacts.end(); it++)
            {
                if ((*it).first > fileid)
                {
                    it--;
                    *fd = it->second.get_cfd();
                    break;
                }
            }
            if (it == contacts.end())
                *fd = contacts.rbegin()->second.get_cfd();
        }
#ifdef DEBUG
        cout << __func__ << " Dest Node" << *fd << endl;
#endif
        send_tcp(FILE_EXISTS, *fd, const_cast<char *>(sdfs.c_str()), sdfs.size());
        
        uint8_t bytes[MAX_MSG];
        memset(bytes, '\0', MAX_MSG);
        msg_t* msg = (msg_t*)bytes;
        struct sockaddr_in src;

        bool ret = recv_op(*fd, msg);
        if (!ret) {
            return false;
        }


		if (msg->type == FILE_MISSING)
		    return false;
		else
		    return true;
    }
}

void Node::getfile(string sdfs, string local)
{
    //Check if file exists
    int fd;
    bool exists = fileexists(sdfs, &fd);

    // File may be already cached. Check the cache list
    for (vector<FileEntry>::iterator it = cachedfilelist.begin(); it != cachedfilelist.end(); it++)
    {
        if (sdfs == it->sdfsfilename)
        {
            if (!exists)
            {
				if (fd != -1)
				{
				    string fname = get_real_fname(machine_num, it->localfilename);
		            remove(fname.c_str());
				}
				else
				{
					cerr << "File's hash should owned by node and should not be kept in cache" << endl;
				    /*FileEntry f;
					rename(it->localfilename, local);
				    f.sdfsfilename = sdfs;
				    f.localfilename = local;
				    clockwisefilelist.push_back(f);
			        cout << "File has been locally saved as" << local << endl;*/
				}
                cachedfilelist.erase(it);
            }
            else
                cout << "File is already present and is locally saved as " << it->localfilename << endl;
            return;
        }
    }

    if (!exists)
    {
        cout << "File does not exist" << endl;
        return;
    }

    if (fd == -1)
    {
        //File is present and resides locally.
        for (vector<FileEntry>::iterator it = clockwisefilelist.begin(); it != clockwisefilelist.end(); it++)
            if (sdfs == it->sdfsfilename)
            {
                local = it->localfilename;
                break;
            }

        cout << "File is already present and is locally saved as" << local << endl;
    }
    else
    {
        //File is present it needs to be fetched from the node whose hash id is <= fileid
        bool success = fetchfile(fd, sdfs, local);
        if (success) {
            cout << "File read successfully and is locally saved as " << local << endl;
            FileEntry f;
            f.sdfsfilename = sdfs;
            f.localfilename = local;
            cachedfilelist.push_back(f);
        }
        else
            cout << "File read failed for some unknown reason errno: " << errno << endl;
    }
}

bool Node::fetchfile(int fd, string sdfs, string local)
{
    send_tcp(GET, fd, const_cast<char *>(sdfs.c_str()), sdfs.size());
    return receivefile(fd, local);
}

bool Node::receivefile(int fd, string local)
{
    uint8_t bytes[MAX_MSG];
    struct sockaddr_in src;
    string fname = get_real_fname(machine_num, local);

	char status[100];
	sprintf(status, "Function: receivefile: %s\n", fname.c_str());
	logger.log(string(status), &sockaddr);

    FILE *fp = fopen(fname.c_str(),"w");

    int ret;
    msg_t* msg = (msg_t*)bytes;

    do
    {
        memset(bytes, '\0', MAX_MSG);
        bool ret= recv_op(fd, msg);
        if (!ret) {
            return false;
        }

        if (msg->type != END_OF_FILE)
		{
		    fprintf(fp,"%s",(char *)msg->payload);
	        send_tcp(DATA_RECV, fd, NULL, 0);
		}
    } while (msg->type != END_OF_FILE);

	sprintf(status, "Function: receivefile: Recv END_OF_FILE");
	logger.log(string(status), &sockaddr);

#ifdef DEBUG
    cout << __func__ << ": after end_of_file ret " << ret << " errno: " << errno << " msg: " << msg->payload << endl;
#endif

    fclose(fp);
    return true;
}

bool Node::copylocally(string srcfile, string localfile)
{
    int filefd, ret;

    char line[MAX_MSG];
    size_t len = 0;
	
	FILE *fp = fopen(localfile.c_str(), "w");

	if (!fp)
	{
		cout << "File could not be created" << endl;
		return false;
	}

	char status[100];
	sprintf(status, "Function: copylocally: Copying file %s\n", localfile.c_str());
	logger.log(string(status), &sockaddr);

    if ((filefd = open(srcfile.c_str(), O_RDONLY)) == -1)
    {
        cout << "File could not be opened for some unknown reason error: " << errno << endl;
		close(filefd);
		fclose(fp);
        return false;
    }

    memset(line, '\0', MAX_MSG);
    while(read(filefd, line, MAX_MSG - 2) > 0) {
		fprintf(fp, "%s", line);
        memset(line, '\0', MAX_MSG);
    }

    close(filefd);
	fclose(fp);
    return true;
}

void Node::putfile(string sdfs, string local)
{
    int fd;
    bool exists = fileexists(sdfs, &fd);
#ifdef DEBUG
    cout << __func__ << ": fd " << fd << endl;
#endif

    if (exists)
    {
        cout << "A file with this filename already exists. Please try something else." << endl;
        return;
    }

    bool success = true;
    if (fd == -1)
    {
		string localfile = "local_" + sdfs;
	    string fname = get_real_fname(machine_num, localfile);

		char status[100];
		sprintf(status, "Function: putfile: Filename %s\n", fname.c_str());
		logger.log(string(status), &sockaddr);

		if (copylocally(local, fname))
		{
			FileEntry f;
			f.sdfsfilename = sdfs;
			f.localfilename = localfile;
			clockwisefilelist.push_back(f);

			cout << "File is locally saved as: " << localfile << endl;

		    // Send the data to the clockwise neighbor as a replica
		    send_tcp(REPL_PUT, clkwise_node.second.get_cfd(), const_cast<char *>(sdfs.c_str()), sdfs.size());
		    sleep(1);
		    success = pushfile(clkwise_node.second.get_cfd(), fname);
		}
    }
    else
    {
		char status[100];
		sprintf(status, "Function: putfile: Sending Data %s ", sdfs.c_str());
		logger.log(string(status), &sockaddr);

        send_tcp(PUT, fd, const_cast<char *>(sdfs.c_str()), sdfs.size());
        success = pushfile(fd, local);
    }
    
    if (success) {
        cout << "File inserted successfully" << endl;
    }
}

bool Node::pushfile(int fd, string local)
{
    int filefd, ret;

    char line[MAX_MSG];
    size_t len = 0;
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*)bytes;

    cout << __func__ << ": opening file " << local << endl;

    if ((filefd = open(local.c_str(), O_RDONLY)) == -1)
    {
        cout << "File could not be opened for some unknown reason error: " << errno << endl;
		close(filefd);
        return false;
    }

    memset(line, '\0', MAX_MSG);
    while(read(filefd, line, MAX_MSG - sizeof(msg_t) - 2) > 0) {
        send_tcp(PUT, fd, line, strlen(line));
        memset(bytes, '\0', MAX_MSG);
        memset(line, '\0', MAX_MSG);
        bool ret = recv_op(fd, msg);
        if (!ret) {
            return false;
        }

        if (msg->type != DATA_RECV)
			cerr << "Last Data is not acknowledged" << endl;
    }
    
	char status[100];
	sprintf(status, "Send Finished");
	logger.log(string(status), &sockaddr);

    //cout << __func__ << ": sent END" << line << endl;
    send_tcp(END_OF_FILE, fd, NULL, 0);     

    close(filefd);
    return true;
}

void Node::delfile(string sdfs)
{
    //Check if file exists
    int fd;
    bool exists = fileexists(sdfs, &fd);

    // File may be already cached. Check the cache list
    for (vector<FileEntry>::iterator it = cachedfilelist.begin(); it != cachedfilelist.end(); it++)
    {
        if (sdfs == it->sdfsfilename)
        {
            if (!exists)
            {
				if (fd != -1)
				{
				    string fname = get_real_fname(machine_num, it->localfilename);
		            remove(fname.c_str());
		            cachedfilelist.erase(it);
	                cout << "File does not exist" << endl;
				}
				else
					cerr << "File's hash should owned by node and should not be kept in cache" << endl;
                return;
            }
        }
    }

    if (!exists)
    {
        cout << "File does not exist" << endl;
        return;
    }

    if (fd == -1)
    {
        //File is present and resides locally.
        for (vector<FileEntry>::iterator it = clockwisefilelist.begin(); it != clockwisefilelist.end(); it++)
            if (sdfs == it->sdfsfilename)
            {
				send_tcp(REPL_DEL, clkwise_node.second.get_cfd(), const_cast<char *>(sdfs.c_str()), sdfs.size());
			    string fname = get_real_fname(machine_num, it->localfilename);
                if (!verify_delete(clkwise_node.second.get_cfd()) ||  remove(fname.c_str()) != 0) {
                    cout << "Delete Failed for some unknown reason error: " << errno << endl;
                } else {
                    cout << "Successfully deleted file" << endl;
                }
                clockwisefilelist.erase(it);                

                break;
            }
    }
    else
    {
        //File is present it needs to be fetched from the node whose hash id is <= fileid
        bool success = removefile(fd, sdfs);
        if (success)
            cout << "Successfully deleted file" << endl;
        else
            cout << "Delete Failed for some unknown reason error: " << errno << endl;
    }
}

bool Node::removefile(int fd, string sdfs)
{
    send_tcp(DEL, fd, const_cast<char *>(sdfs.c_str()), sdfs.size());
    
    return verify_delete(fd);
}

bool Node::verify_delete(int fd)
{
    uint8_t bytes[MAX_MSG];
    memset(bytes, '\0', MAX_MSG);
    msg_t* msg = (msg_t*)bytes;
    bool ret;
    
    ret = recv_op(fd, msg);
    if (!ret) {
        return false;
    }

	if (msg->type != DELETE_FAILED) {
	    return true;
	}
	else {
	    return false;
	}
}

/* Receive and handle the msg */
bool Node::recv()
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*)bytes;
    int ret;
    struct sockaddr_in src;
    map<unsigned int, Contact>::iterator it;
    char id[100];

    ret = recv_msg(this->fd, msg, &src);

    if (ret <= 0) {
        return false;
    }

    switch(msg->type) {
    case JOIN:
    {
        if (!this->is_contact) {
            cerr << "Shouldn't receive JOIN" << endl;
            break;
        }

        for (it = contacts.begin(); it != contacts.end(); it++) {
            if (sockaddr_cmp(&src, it->second.get_sockaddr())) {
                return true;
            }
        }

        struct timespec* join_time;
        char tmp[100];

        join_time = (struct timespec*)msg->payload;
        sprintf(tmp, "Recv JOIN msg at %ld", join_time->tv_sec * 1000000 + join_time->tv_nsec / 1000);
        logger.log(tmp, &src);

        sprintf(id,"%s:%d", inet_ntoa(src.sin_addr), src.sin_port);
        Contact new_member = Contact(src, *join_time);
        unsigned int new_member_hash = hash(id, true);

        int fd = socket(AF_INET, SOCK_STREAM | O_NONBLOCK, 0);
        int ret;

        do {
            ret = connect(fd, (struct sockaddr*)&src, sizeof(struct sockaddr));

            if (!ret) {
				char status[100];
				sprintf(status, "Msg: JOIN: Connected to: %d\n", ntohs(src.sin_port));
				logger.log(string(status), &sockaddr);

                new_member.set_cfd(fd);
                break;
            }
        } while (ret != 0);

        new_member_msg_t arg;
        arg.addr = src;
        arg.hash = new_member_hash;

        send_new_member(this, (void*)&arg);

        sleep(1);

        contacts.insert(pair<unsigned int, Contact>(new_member_hash, new_member));
        update_contacts();
        send_member_list(this, &src);

        break;
    }

    case LEAVE:
    {
        logger.log("Recv LEAVE msg", &src);

        for (it = contacts.begin(); it != contacts.end(); it++) {
            if (aclkwise_node.second == (*it).second) {
                contacts.erase(it++);

                if (!contacts.size()) {
                    clkwise_node.second = Contact();
                    aclkwise_node.second = Contact();
                    break;
                }

                struct sockaddr_in member_s = *(aclkwise_node.second.get_sockaddr());
                update_contacts_onrem();

                send_member_die(this, &member_s);

                if (sockaddr_cmp(&member_s, &contact_sockaddr)) {
                    election_msg_t msg;
                    msg.best_attr = clockwisefilelist.size();
                    msg.node_id = hashid;
                    is_candidate = true;

                    send_election_req(this, &msg);
                }

                break;
            }
        }

        break;
    }

    case HEARTBEAT:
    {
        aclkwise_node.second.reset_penalty();

        logger.log("Recv HEARTBEAT msg", &src);
        break;
    }

    case MEMBER_LIST:
    {
        if (this->is_contact) {
            cerr << "Contact shouldn't recv MEMBER_LIST" << endl;
            break;
        }

        new_member_msg_t s;
        int ret;
        int fd;
        for(uint32_t i = 0; i < (msg->len)/sizeof(new_member_msg_t); i++) {
            s = ((new_member_msg_t*)msg->payload)[i];
            
            pair<unsigned int, Contact> c (s.hash, Contact(s.addr, zero));
            if (sockaddr_cmp(&sockaddr, &s.addr)) {
                hashid = s.hash;
            }
            else {
                fd = socket(AF_INET, SOCK_STREAM | O_NONBLOCK, 0);
                do {
                    ret = connect(fd, (struct sockaddr*)&s.addr, sizeof(struct sockaddr));

                    if (!ret) {
						char status[100];
						sprintf(status, "Msg: MEMBER_LIST: Connected to: %d\n", ntohs(src.sin_port));
						logger.log(string(status), &sockaddr);
                        c.second.set_cfd(fd);
                    }
                } while (ret != 0);
            }
            contacts.insert(c);
        }

        update_contacts_onadd();
        // update_contacts();

        logger.log("Recv MEMBER LIST msg", &src);
        break;
    }

    case MEMBER_DIE:
    {
        struct sockaddr_in* s = (struct sockaddr_in*)msg->payload;
        for (it = contacts.begin(); it != contacts.end(); it++) {
            if (sockaddr_cmp(s, it->second.get_sockaddr())) {
                contacts.erase(it);
                break;
            }
        }

        update_contacts_onrem();
        logger.log("Recv MEMBER DIE msg", &src);
        break;
    }

    case ELECTION_REQ:
    {
        election_msg_t* m = (election_msg_t*)msg->payload;

        /* This node has the best attribute */
        if (is_candidate && m->node_id == hashid) {
			char status[100];
			sprintf(status, "Msg: ELECTION_REQ: This node will be leader");
			logger.log(string(status), &sockaddr);
            new_leader_msg_t ldr_msg;
            ldr_msg.s = sockaddr;
            send_new_leader(this, &ldr_msg);

            is_candidate = false;
            is_leader = true;
            setup_contact();
        } else {
            if (clockwisefilelist.size() < m->best_attr) {
                m->best_attr = clockwisefilelist.size();
                m->node_id = hashid;
                is_candidate = true;
            }

			char status[100];
			sprintf(status, "best_attr: %d node: %d\n", m->best_attr, m->node_id);
			logger.log(string(status), &sockaddr);

            send_election_req(this, m);
        }

        logger.log("Recv ELECTION_REQ msg", &src);
        break;
    }

    case NEW_LEADER:
    {
        new_leader_msg_t* m = (new_leader_msg_t*)msg->payload;

        if (is_leader) {
            is_leader = false;
        } else {
            is_candidate = false;
            contact_sockaddr = m->s;
        }

        logger.log("Recv NEW_LEADER msg", &src);
        break;
    }

    default:
        cerr << __func__ << ": Invalid type " << msg->type << endl;
    }

    return true;
}

int Node::handle_client(int fd)
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*)bytes;
    char *sdfsfile;
    bool ret;

    memset(msg, '\0', sizeof(char) * MAX_MSG);

    ret = recv_op(fd, msg);
    if (!ret) {
        return ret;
    }


    switch(msg->type) {
    case NEW_MEMBER:
    {
        if (this->is_contact) {
            cerr << "Contact shouldn't recv NEW_MEMBER" << endl;
            break;
        }

        new_member_msg_t* m = (new_member_msg_t*)msg->payload;
        Contact new_member = Contact(m->addr, zero);

        int fd = socket(AF_INET, SOCK_STREAM | O_NONBLOCK, 0);
        int ret;

        do {
            ret = connect(fd, (struct sockaddr*)&m->addr, sizeof(struct sockaddr));

            if (!ret) {
				char status[100];
				sprintf(status, "Msg: NEW_MEMBER: Connected\n");
				// logger.log(string(status), &sockaddr);
                new_member.set_cfd(fd);
                break;
            }
        } while (ret != 0);

        contacts.insert(pair<unsigned int, Contact>(m->hash, new_member));
        print_contacts_list();

        update_contacts();

        logger.log("Recv NEW MEMBER msg for: ", &sockaddr);
        break;
    }
    case GET:
    {
        sdfsfile = (char *)msg->payload;
        string sdfs(sdfsfile), local;

        for (vector<FileEntry>::iterator it = clockwisefilelist.begin(); it != clockwisefilelist.end(); it++)
            if (sdfs == it->sdfsfilename)
            {
                local = it->localfilename;
                break;
            }

	    string fname = get_real_fname(machine_num, local);

        pushfile(fd, fname);
        logger.log("Recv GET msg for" + sdfs, &sockaddr);
        break;
    }
    case PUT:
    {
        sdfsfile = (char *)msg->payload;
        string sdfs(sdfsfile);

        string local = "local_" + sdfs;
#ifdef DEBUG
        cout << "File name:" << local << endl;
#endif
        bool success = receivefile(fd, local);
        if (success)
        {
            FileEntry f;
            f.sdfsfilename = sdfs;
            f.localfilename = local;
            clockwisefilelist.push_back(f);
		    string fname = get_real_fname(machine_num, local);
#ifdef DEBUG
			cout << "Replicating to fd: " << clkwise_node.second.get_cfd();
#endif
            send_tcp(REPL_PUT, clkwise_node.second.get_cfd(), const_cast<char *>(sdfs.c_str()), sdfs.size());
			sleep(1);
            success = pushfile(clkwise_node.second.get_cfd(), fname);
        }
        logger.log("Recv PUT msg for" + sdfs, &sockaddr);
        break;
    }
    case SPL_PUT:
    {
        // PUT without replication
        sdfsfile = (char *)msg->payload;
        string sdfs(sdfsfile);

        string local = "local_" + sdfs;
#ifdef DEBUG
        cout << "File name:" << local << endl;
#endif
        bool success = receivefile(fd, local);
        if (success)
        {
            FileEntry f;
            f.sdfsfilename = sdfs;
            f.localfilename = local;
            clockwisefilelist.push_back(f);
            cout << __func__ << ": SPL_PUT size " << clockwisefilelist.size() << endl;
        }
        logger.log("Recv SPL_PUT msg for" + sdfs, &sockaddr);
        break;
    }
    case REPL_PUT:
    {
        sdfsfile = (char *)msg->payload;
        string sdfs(sdfsfile);
		
		string local = "replica_" + sdfs;
#ifdef DEBUG
        cout << "File name:" << local << endl;
#endif
        bool success = receivefile(fd, local);
        if (success)
        {
            FileEntry f;
            f.sdfsfilename = sdfs;
            f.localfilename = local;
            anticlockwisereplicafilelist.push_back(f);
            cout << __func__ << ": REPL_PUT size " << anticlockwisereplicafilelist.size() << endl;
        }
        logger.log("Recv REPL_PUT msg for" + sdfs, &sockaddr);

        break;
    }
    case DEL:
    {
		bool success = false;
        sdfsfile = (char *)msg->payload;
        string sdfs(sdfsfile);

        vector<FileEntry>::iterator it = clockwisefilelist.begin();
        for (; it != clockwisefilelist.end(); it++)
            if (sdfs == it->sdfsfilename)
            {
			    string fname = get_real_fname(machine_num, it->localfilename);
				send_tcp(REPL_DEL, clkwise_node.second.get_cfd(), const_cast<char *>(sdfs.c_str()), sdfs.size());
				if (!verify_delete(clkwise_node.second.get_cfd()) || (remove(fname.c_str()) == -1)) {
					cout << "File Delete Failed for some unknown reason errno: " << errno << endl;
                } else {
				    cout << "File Deleted successfully" << endl;
                }
				success = true;
                clockwisefilelist.erase(it);
                send_tcp(FILE_DELETED, fd, NULL, 0);

                break;
            }
        
        if (!success && it == clockwisefilelist.end())
		{
			cout << "File is not present" << endl;
            send_tcp(DELETE_FAILED, fd, NULL, 0);
		}

        logger.log("Recv DEL msg for" + sdfs, &sockaddr);
        break;
    }
    case REPL_DEL:
    {
		bool success = false;
        sdfsfile = (char *)msg->payload;
        string sdfs(sdfsfile);

        vector<FileEntry>::iterator it = anticlockwisereplicafilelist.begin();
        for (; it != anticlockwisereplicafilelist.end(); it++)
            if (sdfs == it->sdfsfilename)
            {
			    string fname = get_real_fname(machine_num, it->localfilename);
				if (remove(fname.c_str()) != 0) {
					cout << "Replica Delete Failed for some unknown reason error: " << errno << endl;
                    send_tcp(DELETE_FAILED, fd, NULL, 0);
                    break;
                }
				success = true;
                anticlockwisereplicafilelist.erase(it);
				cout << "Replica Deleted successfully" << endl;
                send_tcp(FILE_DELETED, fd, NULL, 0);

                break;
            }
        
        if (!success && it == anticlockwisereplicafilelist.end())
		{
			cout << "Replica is not present" << endl;
            send_tcp(DELETE_FAILED, fd, NULL, 0);
		}
        logger.log("Recv REPL_DEL msg for" + sdfs, &sockaddr);
        break;
    }
    case FILE_EXISTS:
    {
        sdfsfile = (char *)msg->payload;
        string sdfs(sdfsfile);
		bool success = false;

        struct sockaddr_in dst;
        for (vector<FileEntry>::iterator it = clockwisefilelist.begin(); it != clockwisefilelist.end(); it++)
            if (sdfs == it->sdfsfilename)
            {
                logger.log("Sending File Found\n", &sockaddr);
                send_tcp(FILE_FOUND, fd, NULL, 0);
				success = true;
				break;
            }

		if (!success)
		{
		    logger.log("Sending File Missing\n", &sockaddr);
		    send_tcp(FILE_MISSING, fd, NULL, 0);
		}

        logger.log("Recv FILE_EXISTS msg for" + sdfs, &sockaddr);
	    break;
    }

    case NEW_REPL_GET:
    {
#ifdef DEBUG
        cout << "Receive NEW_REPL_GET" << endl;
        // cout << "clockwise: " << clockwisefilelist.size();
        // cout << "anticlockwise: " << anticlockwisereplicafilelist.size();
#endif
        // New anticlockwise neighbor added: Need to move some replicas from my list to its list.
        unsigned int fileid;
        vector<FileEntry>::iterator it = anticlockwisereplicafilelist.begin();
        int i = 0;
        while ((i++) < anticlockwisereplicafilelist.size())
        {
#ifdef DEBUG
            cout << "replica if sfds: " << it->sdfsfilename << " size: " << anticlockwisereplicafilelist.size() << " aclkwise id: " << aclkwise_node.first << endl; 
#endif
            fileid = hash(it->sdfsfilename.c_str(), false);
            if ((aclkwise_node.first < hashid && (fileid < aclkwise_node.first || fileid >= hashid)) || (aclkwise_node.first > hashid && (fileid < aclkwise_node.first && fileid >= hashid))) {
		        logger.log("Moving Replica" + it->sdfsfilename , &sockaddr);
                // Test for crash
                copyreplica(fd, it->sdfsfilename, it->localfilename);
			    string fname = get_real_fname(machine_num, it->localfilename);
                if (remove(fname.c_str()) != 0)
                    cout << "Delete Failed for some unknown reason errno" << errno << endl;
                anticlockwisereplicafilelist.erase(it);
                it = anticlockwisereplicafilelist.begin();
                i = 0;
            }
            else {
                it++;
            }
            usleep(100);
        }
        send_tcp(TRANS_FINISH, fd, NULL, 0);
        logger.log("Recv NEW_REPL_GET msg", &sockaddr);
        break;
    }

    case NEW_FILE_GET:
    {
        logger.log("Recv NEW_FILE_GET msg", &sockaddr);
#ifdef DEBUG
        cout << "Receive NEW_FILE_GET" << endl;
        cout << "clockwise: " << clockwisefilelist.size();
        cout << "anticlockwise: " << anticlockwisereplicafilelist.size();
#endif
        // New Clockwise Neighbor added: Need to move some file from my list to its list. Replica will still stay with the original node
        unsigned int fileid;
        vector<FileEntry>::iterator it = clockwisefilelist.begin();
        int i = 0;
        while ((i++) < clockwisefilelist.size())
        {
            fileid = hash(it->sdfsfilename.c_str(), false);
#ifdef DEBUG
            cout << "file if sfds: " << it->sdfsfilename << endl; 
#endif

            if ((clkwise_node.first > hashid && (fileid >= clkwise_node.first || fileid < hashid)) || (clkwise_node.first < hashid && fileid >= clkwise_node.first && fileid < hashid)) {
		        logger.log("Moving File" + it->sdfsfilename , &sockaddr);
                // Test for crash
                copyfile(fd, it->sdfsfilename, it->localfilename);
			    string fname = get_real_fname(machine_num, it->localfilename);
                if (remove(fname.c_str()) == -1)
                    cout << "Delete Failed for some unknown reason errno: " << errno << endl;
                clockwisefilelist.erase(it);
                it = clockwisefilelist.begin();
                i = 0;
            }
            else {
                it++;
            }
            usleep(100);
        }
        send_tcp(TRANS_FINISH, fd, NULL, 0);
        break;
    }

    default:
        cerr << __func__ << ": Invalid type " << msg->type << endl;
    }

    return 0;
}

/* Check the status of all the contacts */
void Node::check_heartbeat()
{
    map<unsigned int, Contact>::iterator it;

    if (aclkwise_node.second == Contact()) {
        return;
    }

    aclkwise_node.second.inc_penalty();
    if (aclkwise_node.second.get_penalty() == MAX_PENALTY) {
        logger.log("No heartbeat. Removing.", aclkwise_node.second.get_sockaddr());

        for (it = contacts.begin(); it != contacts.end(); it++) {
            if (aclkwise_node.second == (*it).second) {
                contacts.erase(it++);
                if (!contacts.size()) {
                    clkwise_node.second = Contact();
                    aclkwise_node.second = Contact();
                    break;
                }

                struct sockaddr_in member_s = *(aclkwise_node.second.get_sockaddr());
                update_contacts_onrem();

                send_member_die(this, &member_s);

                if (sockaddr_cmp(&member_s, &contact_sockaddr)) {
                    election_msg_t msg;
                    msg.best_attr = clockwisefilelist.size();
                    msg.node_id = hashid;
                    is_candidate = true;

                    send_election_req(this, &msg);
                }

                break;
            }
        }
    }
}

bool Node::copyfile(int dst, string sdfs, string local)
{
    send_tcp(SPL_PUT, dst, const_cast<char *>(sdfs.c_str()), sdfs.size());
    string fname = get_real_fname(machine_num, local);
    usleep(100);
    pushfile(dst, fname);
    usleep(100);
}

bool Node::copyreplica(int dst, string sdfs, string local)
{
    send_tcp(REPL_PUT, dst, const_cast<char *>(sdfs.c_str()), sdfs.size());
    string fname = get_real_fname(machine_num, local);
    usleep(100);
    pushfile(dst, fname);
    usleep(100);
}

void Node::recv_replicas(int fd)
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*)bytes;

    do {
        recv_op(fd, msg);
        if (msg->type != TRANS_FINISH) {
            string sdfs((char*)msg->payload);

            string local = "replica_" + sdfs;
#ifdef DEBUG
            cout << __func__ << ": Replica name:" << local << endl;
#endif
            bool success = receivefile(fd, local);
            if (success) {
                FileEntry f;
                f.sdfsfilename = sdfs;
                f.localfilename = local;
                anticlockwisereplicafilelist.push_back(f);
            }
        }
    } while (msg->type != TRANS_FINISH);
}

void Node::recv_files(int fd)
{
    uint8_t bytes[MAX_MSG];
    msg_t* msg = (msg_t*)bytes;

    do {
        recv_op(fd, msg);
        if (msg->type != TRANS_FINISH) {
            string sdfs((char*)msg->payload);

            string local = "local_" + sdfs;
#ifdef DEBUG
            cout << "File name:" << local << endl;
#endif

            bool success = receivefile(fd, local);
            if (success) {
                FileEntry f;
                f.sdfsfilename = sdfs;
                f.localfilename = local;
                clockwisefilelist.push_back(f);
            }
        }
    } while (msg->type != TRANS_FINISH);
}

bool Node::update_contacts_onadd()
{
    update_contacts();

    logger.begin_benchmark();
    send_tcp(NEW_REPL_GET, clkwise_node.second.get_cfd(), NULL, 0);
    recv_replicas(clkwise_node.second.get_cfd());
    usleep(500);

    send_tcp(NEW_FILE_GET, aclkwise_node.second.get_cfd(), NULL, 0);
    recv_files(aclkwise_node.second.get_cfd());
    logger.end_benchmark("Node rejoining");
}

bool Node::update_contacts_onrem()
{
    logger.begin_benchmark();

    unsigned int oldclkwisehash = clkwise_node.first;
    unsigned int oldaclkwisehash = aclkwise_node.first;
    
    update_contacts();

    //Old clockwise neighbor removed: Need to send some files from my original set to new clockwise node as replicas
    if (clkwise_node.first != oldclkwisehash)
    {
        unsigned int fileid;
        vector<FileEntry>::iterator it = clockwisefilelist.begin();
        while (it != clockwisefilelist.end())
        {
            fileid = hash(it->sdfsfilename.c_str(), false);
#ifdef DEBUG
            cout << "replica if sdfds: " << it->sdfsfilename << endl;
#endif
            if ((fileid >= hashid && fileid < oldclkwisehash) // Takes care of normal case and wrap around case when NACH > OACH
                || (hashid > oldclkwisehash && (fileid >= hashid || fileid < oldclkwisehash))) { // NACH > OACH
		        logger.log("Moving File " + it->sdfsfilename , &sockaddr);
                copyreplica(clkwise_node.second.get_cfd(), it->sdfsfilename, it->localfilename);
            }
            it++;
        }
    }

    //Old anticlockwise neighbor removed: Need to send some files from my replica set to new anticlockwise node
    if (aclkwise_node.first != oldaclkwisehash)
    {
        unsigned int fileid;
        vector<FileEntry>::iterator it = anticlockwisereplicafilelist.begin();
        while (it != anticlockwisereplicafilelist.end())
        {
#ifdef DEBUG
            cout << "file if sdfds: " << it->sdfsfilename << endl;
#endif
            fileid = hash(it->sdfsfilename.c_str(), false);
            if ((fileid < hashid && fileid >= oldaclkwisehash) // Takes care of normal case and wrap around case when NACH > OACH
                || (hashid < oldaclkwisehash && (fileid < hashid || fileid >= oldaclkwisehash))) { // NACH > OACH
		        logger.log("Moving File " + it->sdfsfilename, &sockaddr);
                copyfile(aclkwise_node.second.get_cfd(), it->sdfsfilename, it->localfilename);
            }
            it++;
        }
    }

    logger.end_benchmark("Replication upon member failure");
}

/* Destructor */
Node::~Node()
{
    close(fd);
    contacts.clear();
}
