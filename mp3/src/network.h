#ifndef NETWORK_H
#define NETWORK_H

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <errno.h>
#include <string>

#include "utils.h"
#include "constants.h"

using namespace std;

/* Stores the basic structure of msg to be sent in the network */
typedef struct msg {
    uint32_t type;
    uint32_t len;
    char payload[0];
} __attribute__((packed)) msg_t;

typedef struct new_member_msg {
    struct sockaddr_in addr;
    unsigned int hash;
} __attribute__((packed)) new_member_msg_t;

typedef struct election_msg {
    int best_attr;
    unsigned int node_id;
} __attribute__((packed)) election_msg_t;

typedef struct new_leader_msg {
    struct sockaddr_in s;
} __attribute__((packed)) new_leader_msg_t;

// TODO: Make log.log more efficient

/* Different type of messages in our system */
enum {
/********************************************************/
/* Ring management messages */
/********************************************************/
    JOIN = 0x1, /* Sent when a new node wants join */
    LEAVE, /* Sent when a node tries to leave */
    HEARTBEAT, /* Heartbeat to neighbor */
    NEW_MEMBER, /* Inform current nodes that a new node is joining */
    MEMBER_LIST, /* Inform new joining node the members in the system */
    CONTACT_REJOIN, /* When contact tries to rejoin the system */
    MEMBER_DIE, /* Sent when a member just died */
    ELECTION_REQ,
    NEW_LEADER,
/********************************************************/
/* File management messages */
/********************************************************/
    GET,
    PUT,
    SPL_PUT,
    REPL_PUT,
    DEL,
    REPL_DEL,
    FILE_MISSING,
    FILE_DELETED,
    DELETE_FAILED,
    FILE_EXISTS,
    FILE_FOUND,
    END_OF_FILE,
    TRANS_FINISH,
    NEW_REPL_GET,
    NEW_FILE_GET,
    DATA_RECV
};

/* Marshall a msg by converting it into network byte order */
inline void marshall(msg_t* msg)
{
    uint32_t i;
    uint16_t tmp;

    /* Fancy pointer trick since htons takes in 2 bytes
     * while our payload is a char array (1 byte each) */
    for (i = 0;i < msg->len;i += 2) {
        tmp = htons(*(uint16_t*)(msg->payload + i));
        memcpy(msg->payload + i, (void*)&tmp, sizeof(uint16_t));
    }

    msg->type = htonl(msg->type);
    msg->len = htonl(msg->len);
}

/* Unmarshall a msg by converting it into host byte order */
inline void unmarshall(msg_t* msg)
{
    uint32_t i;
    uint16_t tmp;

    msg->type = ntohl(msg->type);
    msg->len = ntohl(msg->len);

    /* Fancy pointer trick since ntohs takes in 2 bytes
     * while our payload is a char array (1 byte each) */
    for (i = 0;i < msg->len; i+=2) {
        tmp = ntohs(*(uint16_t*)(msg->payload + i));
        memcpy(msg->payload + i, (void*)&tmp, sizeof(uint16_t));
    }
}

/* Find the total length of a message */
inline int msg_size(msg_t* msg)
{
    return sizeof(msg_t) + msg->len;
}

/* Send a message via UDP */
void send_msg(int type, int fd, struct sockaddr_in* dest_addr, void* payload, int len);

void send_tcp(int type, int fd, void* payload, int len);

/* Recv a message via UDP */
int recv_msg(int fd, msg_t* buf, struct sockaddr_in* src);

bool recv_tcp(int fd, msg_t* msg);

#endif /* NETWORK_H */
