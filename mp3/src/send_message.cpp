#include "send_message.h"
#include "network.h"

void send_join(Node* node)
{
    struct timespec cur;
    char tmp[100];
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cur);
    
    send_msg(JOIN, node->get_fd(), node->get_contact_sockaddr(), &cur, sizeof(struct timespec));
    sprintf(tmp, "Sending JOIN msg at %ld", cur.tv_sec * 1000000 + cur.tv_nsec / 1000);
    logger.log(tmp, node->get_contact_sockaddr());
}

void send_leave(Node* node)
{
    send_msg(LEAVE, node->get_fd(), node->get_clkwise_node().second.get_sockaddr(), NULL, 0);
    logger.log("Sending LEAVE msg", node->get_clkwise_node().second.get_sockaddr());
}

void send_heartbeat(Node* node)
{
    uint64_t ctr;
    pair<unsigned int, Contact> clkwise_node = node->get_clkwise_node();
    
    if (clkwise_node.second == Contact()) {
        return;
    }
    
    ctr = clkwise_node.second.get_ctr();
    send_msg(HEARTBEAT, node->get_fd(), clkwise_node.second.get_sockaddr(), &ctr, sizeof(uint64_t));
    node->get_clkwise_node().second.inc_ctr();
    
    logger.log("Sending HEARTBEAT msg", clkwise_node.second.get_sockaddr());
}

void send_new_member(Node* node, void* arg)
{
    map<unsigned int, Contact>::iterator it;
    map<unsigned int, Contact> contacts = node->get_contacts();

    for (it = contacts.begin(); it != contacts.end(); it++) {
		if (!sockaddr_cmp(it->second.get_sockaddr(),node->get_contact_sockaddr()))
		{
		    // send_msg(NEW_MEMBER, node->get_fd(), it->second.get_sockaddr(), arg, sizeof(new_member_msg_t));
		    send_tcp(NEW_MEMBER, it->second.get_cfd(), arg, sizeof(new_member_msg_t));
		
		    logger.log("Sending NEW MEMBER msg", it->second.get_sockaddr());
		}
    }
}

void send_member_list(Node* node, struct sockaddr_in* s)
{
    map<unsigned int, Contact> contacts = node->get_contacts();
    map<unsigned int, Contact>::iterator it;
    new_member_msg_t members[contacts.size()];
    int i;

    /* TODO */
    for (i = 0, it = contacts.begin(); it != contacts.end(); i++, it++) {
        members[i].addr = *(it->second.get_sockaddr());
		members[i].hash = it->first;
    }

    send_msg(MEMBER_LIST, node->get_fd(), s, (void*)members, sizeof(members));

    logger.log("Sending MEMBER LIST msg", s);
}

void send_contact_rejoin(Node* node)
{
    map<unsigned int, Contact> contacts = node->get_contacts();
    map<unsigned int, Contact>::iterator it;

    for (it = contacts.begin(); it != contacts.end(); it++) {
        send_msg(NEW_MEMBER, node->get_fd(), it->second.get_sockaddr(), (void*)node->get_sockaddr(), sizeof(struct sockaddr_in));
        logger.log("Sending NEW MEMBER msg", it->second.get_sockaddr());
    }
}

void send_member_die(Node* node, struct sockaddr_in* s)
{
    map<unsigned int, Contact> contacts = node->get_contacts();
    map<unsigned int, Contact>::iterator it;

    for (it = contacts.begin(); it != contacts.end(); it++) {
        send_msg(MEMBER_DIE, node->get_fd(), it->second.get_sockaddr(), (void*)s, sizeof(struct sockaddr_in));
        logger.log("Sending MEMBER DIE msg", it->second.get_sockaddr());
    }
}

void send_election_req(Node* node, void* msg)
{
    Contact c = node->find_clkwise_contact().second;

    send_msg(ELECTION_REQ, node->get_fd(), c.get_sockaddr(), (election_msg_t*)msg, sizeof(election_msg_t));
    logger.log("Sending ELECTION REQ msg", c.get_sockaddr());
}

void send_new_leader(Node* node, void* msg)
{
    Contact c = node->find_clkwise_contact().second;

    send_msg(NEW_LEADER, node->get_fd(), c.get_sockaddr(), (new_leader_msg_t*)msg, sizeof(new_leader_msg_t));
    logger.log("Sending NEW LEADER msg", c.get_sockaddr());
}
