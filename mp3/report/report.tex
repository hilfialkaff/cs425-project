% TEMPLATE for Usenix papers, specifically to meet requirements of
%  USENIX '05
% originally a template for producing IEEE-format articles using LaTeX.
%   written by Matthew Ward, CS Department, Worcester Polytechnic Institute.
% adapted by David Beazley for his excellent SWIG paper in Proceedings,
%   Tcl 96
% turned into a smartass generic template by De Clarke, with thanks to
%   both the above pioneers
% use at your own risk.  Complaints to /dev/null.
% make it two column with no page numbering, default is 10 point

% Munged by Fred Douglis <douglis@research.att.com> 10/97 to separate
% the .sty file from the LaTeX source template, so that people can
% more easily include the .sty file into an existing document.  Also
% changed to more closely follow the style guidelines as represented
% by the Word sample file. 

% Note that since 2010, USENIX does not require endnotes. If you want
% foot of page notes, don't include the endnotes package in the 
% usepackage command, below.

% This version uses the latex2e styles, not the very ancient 2.09 stuff.
\documentclass[letterpaper,twocolumn,12pt]{article}
\usepackage{usenix,epsfig,endnotes}
\begin{document}

%don't want date printed
\date{}

%make title bold and 14 pt font (Latex default is non-bold, 16 pt)
\title{\Large \bf ARC: Another Replica of Chord: CS425 MP3 Report}

%for single author (just remove % characters)
\author{
{\rm Hilfi Alkaff}\\
alkaff2@illinois.edu
\and
{\rm Mainak Ghosh}\\
mghosh4@illinois.edu
} % end author

\maketitle

% Use the following at camera-ready time to suppress page numbers.
% Comment it out when you first submit the paper for review.
\thispagestyle{empty}

\section{Design}

The inspiration for ARC is Chord and we have simplified Chord in multiple levels to implement our SDFS. The design decisions we have taken thus have a lot of similarity with actual Chord. Similar to Chord, in ARC, nodes are arranged in a logical ring with each node identified by a simple identifier in the range $0$ to $N$ where $N$ is the number of nodes. The number of nodes, $N$ can scale to any large value. We will cover the design decisions in the next few sections.

\subsection{Ring Management}
A node in ARC maintains information about its two logical neighbors - clockwise and anticlockwise. The clockwise neighbor is usually the smallest hash id neighbor greater than its own hash id. Similarly an anticlockwise neighbor is the largest hashid neighbor lesser than its hash id. The first node and the last node are the only pathogenic cases where the ring wraps around.

Our protocol needs a master for nodes to join and for ring management. We do not assume a master for file query and placement. So when a new node arrives, it send a JOIN request to the master. The master replies with the existing member list and also the hash id of this new node. The new node then calculates its neighbors from the member list. The master also informs other nodes in the ring about the new node and its hash id. The other nodes then recompute their neighbors as well. We used a large portion of our MP2 code to achieve this. 

Unlike our MP2, we have gone for ring heartbeating as it is more scalable and our design has nodes arranged in one logical ring. Every node sends heartbeat message to its clockwise neighbor and the clockwise neighbor informs other nodes in the network when this node fails. This also triggers another neighbor recomputation. For master failure, we use a token ring protocol to elect a new master. When the master fails, the neighbor that is responsible to check its heartbeat will send an ELECTION message which will then trigger the ring protocol. The attribute that is being evaluated for the leader election is the number of local files that a particular node currently has. The more files a node has, the more suitable this node is as a leader. Since the additional task that a master performs is acting as a contact point when a new node joins, thus, assuming the role of a master is not expensive.

\subsection{File Handling}
The second part of the MP deals with placing files in these nodes for further operations like get, put and delete without actually involving a master. The system should also be fault tolerant using replication. Like in Chord, files are hashed to determine which node they belong to. The clockwise node is responsible for storing the replica. Some interesting scenarios could be the nodes for storing the actual file may not be present, nodes getting added to the network for which files are already present. To handle these cases and the overall design we maintain two invariants in the system: 

\begin{itemize}
\item 
A node with a hash id , $P_h$ and having a clockwise neighbor with hash id $C_h$, will contain all the files with hash id, $F_h$ such that:
\begin{center}
$F_h$ $\geq$ $P_h$ and $F_h$ $<$ $C_h$
\end{center}
\item
A node with a hash id , $P_h$ and having a anticlockwise neighbor with hash id $A_h$, will contain all replicas of files with hash id, $F_h$ such that:
\begin{center}
$F_h$ $\geq$ $A_h$ and $F_h$ $<$ $P_h$
\end{center}
\end{itemize}

Note what this invariant does is, it always ensures that a file is replicated at two nodes and thus, we have to identify all the scenarios when this invariant becomes false and correct the ring:

\begin{itemize}
\item
\textbf{Normal File Placement}: This is simple we just follow the first invariant to find the node with the largest hash id smaller or equal to the file's hash and store the file there. Replication is easy for this case.
\item
\textbf{Node Addition}: When a node $N_h$ gets added then both the invariants may not hold true. This node's anticlockwise neighbor, $A_h$ can hold some files which $N_h$ should have hold at the first place and same for replicas with its clockwise neighbor, $C_h$. So the anticlockwise neighbor detects this case and transfers files with hash id $F_h$ $\geq$ $N_h$ and $F_h$ $<$ $A_h$ from its local store to the new node's store. Similarly it happens for the replicas in $A_h$.
\item
\textbf{Node Dropping Off}: This scenario also can lead to the invariant getting falsified. The only difference here is, the data may be lost in the process but since we maintain replicas we need to restore the data to the dropped node's anticlockwise neighbor as that is now the node with largest hashid less than file's hash. A similar thing needs to be done with the replicas.
\end{itemize}

With all the nodes running this protocol, it is now very easy to route get, put and delete operations to the correct node in the network. A node also caches query operations. A cache in the ARC system stores all the data for filenames which it actually does not own, i.e the file's hash is not in the range for the node. It just stores them when someone queries it for improving future query latencies but this data is not replicated.

\section{Evaluation}

\begin{figure}[h]
  \centering
  \includegraphics[width=\linewidth]{./read_write.pdf}
  \label{fig:rw}
\end{figure}

Figure~\ref{fig:rw} shows how the get and put operations scale with the different file sizes in ARC. As expected, there is almost no difference between get and put file operations since in essence, they are performing the same task. Additionally, we can see that the time taken also scales linearly with the file sizes which is to be expected too. Also, note that the standard deviation is almost insignificant. This is because the time taken for a node to figure out the file placement does not vary much from node to node. Also the source node with the query contacts the destination node directly without using any hops. So the overall time takes should be and is almost similar.

Due to space limitation, we could only utilize 10GB of the Wikipedia corpus. With 10GB, it still follows Figure~\ref{fig:rw}. It takes an average of 203.07 seconds with a standard deviation of 0.77428 seconds to store the corpus into ARC.

The rereplication time for a 10MB file is 0.53s on average with a standard deviation of 0.18s and the bandwidth turns out to be 18MBps . Note on a failure of an ARC node we move replicas and normal files around to satisfy our invariants which accounts for the time we see here.

With our design, the average time between master failure and new master being reinstated is 78us with a standard deviation of 46.63us. The high standard deviation is due to the worst case of leader election through ring token protocol which is $O(n^{2})$ while the best case is $O(n)$. Since our design does not use use a master for file operations, file replication when a new member joins is very efficient since it does not need to consult the master (508us).

We used MP1 for debugging ARC as lot of the protocol which includes file transfers, ring management are distributed. All nodes log information on getting messages both control and data. Thus to debug any workflow which can be leader election, replication, etc checking the control messages using distributed grep helped us match our theoretical protocol with actually what was happening and thus point out and fix any inconsistencies.

\end{document}
