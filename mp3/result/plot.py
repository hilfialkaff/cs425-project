import numpy as np
import os
import matplotlib.pyplot as plt

""

r_times = {}
w_times = {}

def parse():
    for fname in os.listdir("."):
        if ".log" not in fname:
            continue

        f = open(fname, 'r')
        line = f.readline()

        label = fname[:-6]
        r_times[label] = []
        w_times[label] = []

        while line != '':
            if "GETFILE" in line:
                t = float(line.split(' ')[-1])
                r_times[label].append(t/(10**6))
            elif "PUTFILE" in line:
                t = float(line.split(' ')[-1])
                w_times[label].append(t/(10**6))
            line = f.readline()
        # print r_times
        # print w_times

def plot():
    fig = plt.figure()
    ax = fig.add_subplot(111)

    x = r_times.keys()
    y = r_times.values()
    print "read avg: ", [np.average(arr) for arr in y]
    print "read std: ", [np.std(arr) for arr in y]
    plt.errorbar(x, [np.average(arr) for arr in y], yerr=[np.std(arr) for arr in y], fmt='--o', label="Read times")

    x = w_times.keys()
    y = w_times.values()
    plt.errorbar(x, [np.average(arr) for arr in y], yerr=[np.std(arr) for arr in y], fmt='--o', label="Write times")
    print "write avg: ", [np.average(arr) for arr in y]
    print "write std: ", [np.std(arr) for arr in y]

    plt.legend()

    ax.set_xlabel("File Size (MB)")
    ax.set_ylabel("Time (s)")
    ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
       ncol=2, mode="expand", borderaxespad=0.)
    ax.set_xscale('log')
    plt.savefig("read_write.pdf", format='pdf')

if __name__ == '__main__':
    parse()
    plot()
